package com.vivo;

import com.alibaba.fastjson.JSON;
import com.vivo.dao.StockMapper;
import com.vivo.entity.StockEntity;
import com.vivo.entity.StockExample;
import com.vivo.entity.StockExt;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@SpringBootTest
@Slf4j
public class StockTest {

    @Autowired
    private StockMapper stockMapper;

    @Test
    public void tt() throws IOException {
        StockExample stockExample = new StockExample();
        StockExample.Criteria criteria = stockExample.createCriteria();
        Boolean b = false;
        if (b == true) {
            criteria.andProductCodeEqualTo("BWYLX001").andStockPatternEqualTo("A0,1,S");
            System.out.println(stockMapper.selectByExample(stockExample));
        } else {
            criteria.andProductCodeEqualTo("BWYLX001").andStockPatternEqualTo("A0,1,M12");
            System.out.println(stockMapper.selectByExample(stockExample));
        }
        System.out.println("true".equals(null));
    }

    //BWYLX002数据导入
    @Test
    public void testBWYLX002() throws IOException {
        List<String> shebaoList = new ArrayList();
        shebaoList.add("0");
        shebaoList.add("1");
        List<String> jiaofeiList = new ArrayList();
        jiaofeiList.add("M12");
        jiaofeiList.add("S");
        //第一组价格，首投的基本价original_price
        BufferedReader firstYouNian = new BufferedReader(new FileReader("D:\\BWYLX002首投费率表\\1.年交有社保.txt"));
        BufferedReader firstWuNian = new BufferedReader(new FileReader("D:\\BWYLX002首投费率表\\2.年交无社保.txt"));
        BufferedReader firstYouYue = new BufferedReader(new FileReader("D:\\BWYLX002首投费率表\\3.月交有社保.txt"));
        BufferedReader firstWuYue = new BufferedReader(new FileReader("D:\\BWYLX002首投费率表\\4.月交无社保.txt"));
        //第二组价格，续保的基本价renew_price
        BufferedReader renewYouNian = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\百万医疗\\1.年交有社保.txt"));
        BufferedReader renewWuNian = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\百万医疗\\2.年交无社保.txt"));
        BufferedReader renewYouYue = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\百万医疗\\3.月交有社保.txt"));
        BufferedReader renewWuYue = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\百万医疗\\4.月交无社保.txt"));
        //第二组价格，续保的体检保费renewPhysicalCheckPremium
        BufferedReader tjYouNian = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\体检\\1.年交有社保.txt"));
        BufferedReader tjWuNian = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\体检\\2.年交无社保.txt"));
        BufferedReader tjYouYue = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\体检\\3.月交有社保.txt"));
        BufferedReader tjWuYue = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\体检\\4.月交无社保.txt"));
        //第二组价格，续保的质子重离子治疗保费renewPHITPremium
        BufferedReader pHITYouNian = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\PHIT\\1.年交有社保.txt"));
        BufferedReader pHITWuNian = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\PHIT\\2.年交无社保.txt"));
        BufferedReader pHITYouYue = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\PHIT\\3.月交有社保.txt"));
        BufferedReader pHITWuYue = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\PHIT\\4.月交无社保.txt"));
        //第二组价格，续保的特药保费renewSpecMedicinePremium
        BufferedReader tyYouNian = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\附加特药\\1.年交有社保.txt"));
        BufferedReader tyWuNian = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\附加特药\\2.年交无社保.txt"));
        BufferedReader tyYouYue = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\附加特药\\3.月交有社保.txt"));
        BufferedReader tyWuYue = new BufferedReader(new FileReader("D:\\BWYLX002续保费率表\\附加特药\\4.月交无社保.txt"));
        for (Integer age = 0; age <= 99; age++) {
            for (String shebao : shebaoList) {
                for (String jiaofei : jiaofeiList) {
                    StockEntity record = new StockEntity();
                    StockExt stockExt = new StockExt();
                    record.setProductCode("BWYLX002");
                    record.setStockPattern("A" + age + "," + shebao + "," + jiaofei);
                    record.setStockQuota("3000000.00");
                    record.setEffectiveTime("D1");
                    record.setDuration("Y1");
                    record.setHesitationPeriod("D15");
                    record.setWaitingPeriod("D30");
                    record.setGracePeriod("D30");
                    record.setValid(1);
                    record.setVersion(1L);
                    record.setIsDeleted(0);
                    //配置业务代码
                    if (jiaofei.equals("M12")) {
                        record.setBusinessCode("1013B00R02");
                    } else if (jiaofei.equals("S")) {
                        record.setBusinessCode("1013B00R01");
                    }
                    if (shebao.equals("0")) {
                        if (jiaofei.equals("M12")) {/*无社保月交*/
                            if (age <= 65) {
                                BigDecimal firstPrice = BigDecimal.valueOf(2.00);
                                record.setFirstPrice(firstPrice);
                                BigDecimal originalPrice = BigDecimal.valueOf(new Double(firstWuYue.readLine()));
                                record.setOriginalPrice(originalPrice);
                                record.setSaleAds("首月￥" + String.format("%.2f", firstPrice) + "," + "次月起（共11月）￥" + String.format("%.2f", originalPrice) + "/月");
                                stockExt.setFirstYearPremium(firstPrice.add(originalPrice.multiply(BigDecimal.valueOf(11))));
                            }
                            BigDecimal renewPrice = BigDecimal.valueOf(new Double(renewWuYue.readLine()));
                            record.setRenewPrice(renewPrice);
                            BigDecimal renewPhysicalCheckPremium = BigDecimal.valueOf(new Double(tjWuYue.readLine()));
                            stockExt.setRenewPhysicalCheckPremium(renewPhysicalCheckPremium);
                            BigDecimal renewPHITPremium = BigDecimal.valueOf(new Double(pHITWuYue.readLine()));
                            stockExt.setRenewPHITPremium(renewPHITPremium);
                            BigDecimal renewSpecMedicinePremium = BigDecimal.valueOf(new Double(tyWuYue.readLine()));
                            stockExt.setRenewSpecMedicinePremium(renewSpecMedicinePremium);
                        } else if (jiaofei.equals("S")) {/*无社保年交*/
                            if (age <= 65) {
                                BigDecimal originalPrice = BigDecimal.valueOf(new Double(firstWuNian.readLine()));
                                record.setFirstPrice(originalPrice);
                                record.setOriginalPrice(originalPrice);
                                record.setSaleAds("￥" + String.format("%.2f", originalPrice) + "/年");
                                stockExt.setFirstYearPremium(originalPrice);
                            }
                            BigDecimal renewPrice = BigDecimal.valueOf(new Double(renewWuNian.readLine()));
                            record.setRenewPrice(renewPrice);
                            BigDecimal renewPhysicalCheckPremium = BigDecimal.valueOf(new Double(tjWuNian.readLine()));
                            stockExt.setRenewPhysicalCheckPremium(renewPhysicalCheckPremium);
                            BigDecimal renewPHITPremium = BigDecimal.valueOf(new Double(pHITWuNian.readLine()));
                            stockExt.setRenewPHITPremium(renewPHITPremium);
                            BigDecimal renewSpecMedicinePremium = BigDecimal.valueOf(new Double(tyWuNian.readLine()));
                            stockExt.setRenewSpecMedicinePremium(renewSpecMedicinePremium);
                        }
                    } else if (shebao.equals("1")) {
                        if (jiaofei.equals("M12")) {/*有社保月交*/
                            if (age <= 65) {
                                BigDecimal firstPrice = BigDecimal.valueOf(2.00);
                                record.setFirstPrice(firstPrice);
                                BigDecimal originalPrice = BigDecimal.valueOf(new Double(firstYouYue.readLine()));
                                record.setOriginalPrice(originalPrice);
                                record.setSaleAds("首月￥" + String.format("%.2f", firstPrice) + "," + "次月起（共11月）￥" + String.format("%.2f", originalPrice) + "/月");
                                stockExt.setFirstYearPremium(firstPrice.add(originalPrice.multiply(BigDecimal.valueOf(11))));
                            }
                            BigDecimal renewPrice = BigDecimal.valueOf(new Double(renewYouYue.readLine()));
                            record.setRenewPrice(renewPrice);
                            BigDecimal renewPhysicalCheckPremium = BigDecimal.valueOf(new Double(tjYouYue.readLine()));
                            stockExt.setRenewPhysicalCheckPremium(renewPhysicalCheckPremium);
                            BigDecimal renewPHITPremium = BigDecimal.valueOf(new Double(pHITYouYue.readLine()));
                            stockExt.setRenewPHITPremium(renewPHITPremium);
                            BigDecimal renewSpecMedicinePremium = BigDecimal.valueOf(new Double(tyYouYue.readLine()));
                            stockExt.setRenewSpecMedicinePremium(renewSpecMedicinePremium);
                        } else if (jiaofei.equals("S")) {/*有社保年交*/
                            if (age <= 65) {
                                BigDecimal originalPrice = BigDecimal.valueOf(new Double(firstYouNian.readLine()));
                                record.setFirstPrice(originalPrice);
                                record.setOriginalPrice(originalPrice);
                                record.setSaleAds("￥" + String.format("%.2f", originalPrice) + "/年");
                                stockExt.setFirstYearPremium(originalPrice);
                            }
                            BigDecimal renewPrice = BigDecimal.valueOf(new Double(renewYouNian.readLine()));
                            record.setRenewPrice(renewPrice);
                            BigDecimal renewPhysicalCheckPremium = BigDecimal.valueOf(new Double(tjYouNian.readLine()));
                            stockExt.setRenewPhysicalCheckPremium(renewPhysicalCheckPremium);
                            BigDecimal renewPHITPremium = BigDecimal.valueOf(new Double(pHITYouNian.readLine()));
                            stockExt.setRenewPHITPremium(renewPHITPremium);
                            BigDecimal renewSpecMedicinePremium = BigDecimal.valueOf(new Double(tyYouNian.readLine()));
                            stockExt.setRenewSpecMedicinePremium(renewSpecMedicinePremium);
                        }
                    }
                    record.setAddition(JSON.toJSONString(stockExt));
                    stockMapper.insertSelective(record);
                }
            }
        }
    }

    //BWYLX001数据导入
    @Test
    public void testBWYLX001() throws IOException {
        List<String> shebaoList = new ArrayList();
        shebaoList.add("0");
        shebaoList.add("1");
        List<String> jiaofeiList = new ArrayList();
        jiaofeiList.add("M12");
        jiaofeiList.add("S");
        //第一组价格，首投的基本价original_price
        BufferedReader firstYouNian = new BufferedReader(new FileReader("D:\\BWYLX001首投费率表\\1.年交有社保.txt"));
        BufferedReader firstWuNian = new BufferedReader(new FileReader("D:\\BWYLX001首投费率表\\2.年交无社保.txt"));
        BufferedReader firstYouYue = new BufferedReader(new FileReader("D:\\BWYLX001首投费率表\\3.月交有社保.txt"));
        BufferedReader firstWuYue = new BufferedReader(new FileReader("D:\\BWYLX001首投费率表\\4.月交无社保.txt"));
        //第二组价格，续保的基本价renew_price
        BufferedReader renewYouNian = new BufferedReader(new FileReader("D:\\BWYLX001续保费率表\\百万医疗\\1.年交有社保.txt"));
        BufferedReader renewWuNian = new BufferedReader(new FileReader("D:\\BWYLX001续保费率表\\百万医疗\\2.年交无社保.txt"));
        BufferedReader renewYouYue = new BufferedReader(new FileReader("D:\\BWYLX001续保费率表\\百万医疗\\3.月交有社保.txt"));
        BufferedReader renewWuYue = new BufferedReader(new FileReader("D:\\BWYLX001续保费率表\\百万医疗\\4.月交无社保.txt"));
        //第二组价格，续保的特药保费renewSpecMedicinePremium
        BufferedReader tyYouNian = new BufferedReader(new FileReader("D:\\BWYLX001续保费率表\\附加特药\\1.年交有社保.txt"));
        BufferedReader tyWuNian = new BufferedReader(new FileReader("D:\\BWYLX001续保费率表\\附加特药\\2.年交无社保.txt"));
        BufferedReader tyYouYue = new BufferedReader(new FileReader("D:\\BWYLX001续保费率表\\附加特药\\3.月交有社保.txt"));
        BufferedReader tyWuYue = new BufferedReader(new FileReader("D:\\BWYLX001续保费率表\\附加特药\\4.月交无社保.txt"));
        for (Integer age = 0; age <= 99; age++) {
            for (String shebao : shebaoList) {
                for (String jiaofei : jiaofeiList) {
                    StockEntity record = new StockEntity();
                    StockExt stockExt = new StockExt();
                    record.setProductCode("BWYLX001");
                    record.setStockPattern("A" + age + "," + shebao + "," + jiaofei);
                    record.setStockQuota("3000000.00");
                    record.setEffectiveTime("D1");
                    record.setDuration("Y1");
                    record.setHesitationPeriod("D15");
                    record.setWaitingPeriod("D30");
                    record.setGracePeriod("D30");
                    record.setValid(1);
                    record.setVersion(1L);
                    record.setIsDeleted(0);
                    //配置业务代码
                    if (jiaofei.equals("M12")) {
                        record.setBusinessCode("1013B00R02");
                    } else if (jiaofei.equals("S")) {
                        record.setBusinessCode("1013B00R01");
                    }
                    if (shebao.equals("0")) {
                        if (jiaofei.equals("M12")) {/*无社保月交*/
                            if (age <= 65) {
                                BigDecimal firstPrice = BigDecimal.valueOf(2.00);
                                record.setFirstPrice(firstPrice);
                                BigDecimal originalPrice = BigDecimal.valueOf(new Double(firstWuYue.readLine()));
                                record.setOriginalPrice(originalPrice);
                                record.setSaleAds("首月￥" + String.format("%.2f", firstPrice) + "," + "次月起（共11月）￥" + String.format("%.2f", originalPrice) + "/月");
                                stockExt.setFirstYearPremium(firstPrice.add(originalPrice.multiply(BigDecimal.valueOf(11))));
                            }
                            BigDecimal renewPrice = BigDecimal.valueOf(new Double(renewWuYue.readLine()));
                            record.setRenewPrice(renewPrice);
                            BigDecimal renewSpecMedicinePremium = BigDecimal.valueOf(new Double(tyWuYue.readLine()));
                            stockExt.setRenewSpecMedicinePremium(renewSpecMedicinePremium);
                        } else if (jiaofei.equals("S")) {/*无社保年交*/
                            if (age <= 65) {
                                BigDecimal originalPrice = BigDecimal.valueOf(new Double(firstWuNian.readLine()));
                                record.setFirstPrice(originalPrice);
                                record.setOriginalPrice(originalPrice);
                                record.setSaleAds("￥" + String.format("%.2f", originalPrice) + "/年");
                                stockExt.setFirstYearPremium(originalPrice);
                            }
                            BigDecimal renewPrice = BigDecimal.valueOf(new Double(renewWuNian.readLine()));
                            record.setRenewPrice(renewPrice);
                            BigDecimal renewSpecMedicinePremium = BigDecimal.valueOf(new Double(tyWuNian.readLine()));
                            stockExt.setRenewSpecMedicinePremium(renewSpecMedicinePremium);
                        }
                    } else if (shebao.equals("1")) {
                        if (jiaofei.equals("M12")) {/*有社保月交*/
                            if (age <= 65) {
                                BigDecimal firstPrice = BigDecimal.valueOf(2.00);
                                record.setFirstPrice(firstPrice);
                                BigDecimal originalPrice = BigDecimal.valueOf(new Double(firstYouYue.readLine()));
                                record.setOriginalPrice(originalPrice);
                                record.setSaleAds("首月￥" + String.format("%.2f", firstPrice) + "," + "次月起（共11月）￥" + String.format("%.2f", originalPrice) + "/月");
                                stockExt.setFirstYearPremium(firstPrice.add(originalPrice.multiply(BigDecimal.valueOf(11))));
                            }
                            BigDecimal renewPrice = BigDecimal.valueOf(new Double(renewYouYue.readLine()));
                            record.setRenewPrice(renewPrice);
                            BigDecimal renewSpecMedicinePremium = BigDecimal.valueOf(new Double(tyYouYue.readLine()));
                            stockExt.setRenewSpecMedicinePremium(renewSpecMedicinePremium);
                        } else if (jiaofei.equals("S")) {/*有社保年交*/
                            if (age <= 65) {
                                BigDecimal originalPrice = BigDecimal.valueOf(new Double(firstYouNian.readLine()));
                                record.setFirstPrice(originalPrice);
                                record.setOriginalPrice(originalPrice);
                                record.setSaleAds("￥" + String.format("%.2f", originalPrice) + "/年");
                                stockExt.setFirstYearPremium(originalPrice);
                            }
                            BigDecimal renewPrice = BigDecimal.valueOf(new Double(renewYouNian.readLine()));
                            record.setRenewPrice(renewPrice);
                            BigDecimal renewSpecMedicinePremium = BigDecimal.valueOf(new Double(tyYouNian.readLine()));
                            stockExt.setRenewSpecMedicinePremium(renewSpecMedicinePremium);
                        }
                    }
                    record.setAddition(JSON.toJSONString(stockExt));
                    stockMapper.insertSelective(record);
                }
            }
        }
    }

    //重疾赠险数据导入
    @Test
    public void testZJZX001() {
        for (Integer age = 18; age <= 50; age++) {
            StockEntity record = new StockEntity();
            StockExt stockExt = new StockExt();
            record.setProductCode("ZJZX001");
            record.setStockPattern("A" + age);
            record.setFirstPrice(BigDecimal.valueOf(0.00));
            record.setOriginalPrice(BigDecimal.valueOf(0.00));
            record.setSaleAds("免费");
            record.setBusinessCode("1029A05001");
            if (age <= 40) {
                record.setStockQuota("1000.00-20000.00");
            } else if (age >= 41) {
                record.setStockQuota("1000.00-10000.00");
            }
            record.setValid(1);
            record.setEffectiveTime("D1");
            record.setDuration("Y1");
            record.setHesitationPeriod("D0");
            record.setWaitingPeriod("D90");
            record.setGracePeriod("D0");
            record.setVersion(1L);
            record.setIsDeleted(0);
            stockExt.setFirstYearPremium(BigDecimal.valueOf(0));
            record.setAddition(JSON.toJSONString(stockExt));
            stockMapper.insertSelective(record);
        }
    }

    @Test
    public void dataImport() throws IOException {
        String filePath = "D:\\123.xls";
        Integer sheetNum = 1;
        File file = new File(filePath);
        String fileName = file.getName();
        FileInputStream fileInputStream = new FileInputStream(file);
        Workbook workbook = null;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            workbook = new XSSFWorkbook(fileInputStream);
        } else {
            workbook = new HSSFWorkbook(fileInputStream);
        }
        Sheet sheet = workbook.getSheetAt(sheetNum);
        System.out.println(sheet.getRow(0).getCell(0).getStringCellValue());
    }

    //手机险-保值换新数据导入
    @Test
    public void testSJBZHX() {
        StockExample stockExample = new StockExample();
        stockExample.createCriteria().andProductCodeEqualTo("SJHXB001").andBusinessCodeEqualTo("PL0700999");
        stockExample.setOrderByClause("id asc");
        List<StockEntity> stockEntityList = stockMapper.selectByExample(stockExample);
        stockEntityList.stream().forEach(item -> {
            StockEntity record = new StockEntity();
            record.setProductCode("SJBZHX001");
            record.setBusinessCode(item.getBusinessCode());
            record.setStockPattern(item.getStockPattern());
            record.setOriginalPrice(item.getOriginalPrice());
            if (null != item.getStockQuota()) {
                record.setStockQuota(String.format("%.2f", Double.valueOf(item.getStockQuota()) / 2));
            }
            record.setValid(1);
            record.setEffectiveTime(item.getEffectiveTime());
            record.setDuration(item.getDuration());
            record.setHesitationPeriod(item.getHesitationPeriod());
            record.setWaitingPeriod(item.getWaitingPeriod());
            record.setGracePeriod(item.getGracePeriod());
            record.setIsDeleted(0);
            record.setVersion(1L);
            stockMapper.insertSelective(record);
        });
    }
}
