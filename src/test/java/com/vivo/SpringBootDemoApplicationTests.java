package com.vivo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.vivo.dao.CategoryMapper;
import com.vivo.entity.Category;
import com.vivo.entity.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@Slf4j
class SpringBootDemoApplicationTests {

    @Autowired
    DataSource dataSource;

    @Test
    void contextLoads() throws SQLException {
        System.out.println(dataSource.getClass());
        System.out.println(dataSource.getConnection());
    }

    @Autowired
    private CategoryMapper categoryMapper;

    @Test
    public void testCategory() {
        System.out.println(JSON.toJSONString(categoryMapper.getCategory(0, 0),
                SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue));
    }

    @Test
    public void test99() {
        //String s1=null;
        //System.out.println(s1.split(",").length);
        System.out.println("".split(",").length);
        System.out.println("1".split(",").length);
        System.out.println("1,2".split(",").length);
        log.warn("异常：{}", "123");

    }

    @Test
    public void test100() {
        List<Object> objectList = new ArrayList<>();
        objectList.add(new Category("1", "2", "3", 4, null));
        objectList.add(new Person(1, "潘志雄", "男", 2));
        System.out.println(JSON.toJSON(objectList));
    }
}
