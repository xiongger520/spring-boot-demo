package com.vivo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.vivo.dao.AttrMapper;
import com.vivo.dao.UserTagEntityMapper;
import com.vivo.dto.RiskControlTagDTO;
import com.vivo.entity.UserTagEntity;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@SpringBootTest
public class AttrTest {

    @Resource
    private AttrMapper attrMapper;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Resource
    private UserTagEntityMapper userTagEntityMapper;

    @Test
    public void test() throws ParseException, IOException {

        FileInputStream inputStream = new FileInputStream("D://log.txt");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String str = null;
        while ((str = bufferedReader.readLine()) != null) {
            JSONObject jsonObject = JSON.parseObject(str);
            System.out.println("正在处理:" + str);
            UserTagEntity userTagEntity = JSON.parseObject(jsonObject.getJSONObject("bizData").getString("objAfter").substring(16).replaceAll("\\\\", ""), UserTagEntity.class);
            String tagEnum = "[{\"code\":1,\"desc\":\"电信诈骗\",\"subRiskControlTagList\":[{\"code\":11,\"desc\":\"假冒公检法\"},{\"code\":12,\"desc\":\"电商诈骗\"},{\"code\":13,\"desc\":\"刷单诈骗\"},{\"code\":14,\"desc\":\"投资诈骗\"}]},{\"code\":2,\"desc\":\"贷款诈骗\",\"subRiskControlTagList\":[{\"code\":21,\"desc\":\"中介欺诈\"},{\"code\":22,\"desc\":\"身份冒用\"}]},{\"code\":3,\"desc\":\"协商还款\",\"subRiskControlTagList\":[{\"code\":31,\"desc\":\"减免全部息费\"},{\"code\":32,\"desc\":\"减免罚息\"},{\"code\":33,\"desc\":\"延期还款\"},{\"code\":34,\"desc\":\"停息挂账\"}]},{\"code\":4,\"desc\":\"催收投诉\",\"subRiskControlTagList\":[{\"code\":41,\"desc\":\"催收人员态度\"},{\"code\":42,\"desc\":\"质疑催收流程\"},{\"code\":43,\"desc\":\"非借款人停催\"},{\"code\":44,\"desc\":\"紧急联系人停催\"}]},{\"code\":5,\"desc\":\"其他风控\",\"subRiskControlTagList\":[{\"code\":51,\"desc\":\"其他\"}]}]";
            List<RiskControlTagDTO> riskControlTagDTOList = JSON.parseArray(tagEnum, RiskControlTagDTO.class);
            RiskControlTagDTO firstTag = riskControlTagDTOList.stream().filter(item -> item.getCode().equals(userTagEntity.getFirstTagCode())).findFirst().orElse(null);
            userTagEntity.setFirstTagDesc(firstTag.getDesc());
            RiskControlTagDTO secondTag = firstTag.getSubRiskControlTagList().stream().
                    filter(item -> item.getCode().equals(userTagEntity.getSecondTagCode())).findFirst().orElse(null);
            userTagEntity.setSecondTagDesc(secondTag.getDesc());
            userTagEntity.setSubmitterNo(jsonObject.getJSONObject("operator").getString("userId"));
            userTagEntity.setSubmitter(jsonObject.getJSONObject("operator").getString("userName"));
            userTagEntity.setVersion(1L);
            userTagEntity.setIsDeleted(0);
            userTagEntity.setCreateTime(sdf.parse(jsonObject.getJSONObject("optInfo").getString("time")));
            userTagEntity.setUpdateTime(sdf.parse(jsonObject.getJSONObject("optInfo").getString("time")));
            userTagEntityMapper.insertSelective(userTagEntity);
        }
    }
}
