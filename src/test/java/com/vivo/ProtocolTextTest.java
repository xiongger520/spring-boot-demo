//package com.vivo;
//
//import com.vivo.mongodb.ProtocolText;
//import com.vivo.mongodb.ProtocolTextRepository;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.ArrayList;
//import java.util.List;
//
//
//@SpringBootTest
//public class ProtocolTextTest {
//    @Autowired
//    private ProtocolTextRepository protocolTextRepository;
//
//    @Test
//    public void test1() {
//        protocolTextRepository.save(new ProtocolText(1000L, "PABX001", "平安保险条款", "豁免条款", "PABX", "平安保险", "abcd"));
//        protocolTextRepository.save(new ProtocolText(2000L, "ZABX001", "众安保险条款", "职业分类表", "ZABX", "众安保险", "qwertyui"));
//        protocolTextRepository.save(new ProtocolText(3000L, "TPYBX001", "太平洋保险条款", "风险告知书", "TPYBX", "太平洋保险", "987654321"));
//    }
//
//    @Test
//    public void test2() {
//        System.out.println(protocolTextRepository.findByTextCode("PABX001"));
//        System.out.println(protocolTextRepository.findByTextNameLikeAndSupplierNameLike("平安", "平安"));
//    }
//
//    @Test
//    public void test88() {
//        List<String> list1 = null;
//        List<String> list2 = new ArrayList<String>();
//        System.out.println(list1);
//        System.out.println(list2);
//    }
//
//    @Test
//    public void test99() {
//        String string1 = "[1,2]";
//        System.out.println(string1.substring(1, string1.length() - 1));
//    }
//
//
//}
