package com.vivo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.vivo.dao.CategoryMapper;
import com.vivo.dao.CustomerMapper;
import com.vivo.entity.BigDealEntity;
import com.vivo.entity.Category;
import com.vivo.entity.SalesLimit;
import com.vivo.service.Client;
import com.vivo.service.PriceContext;
import com.vivo.service.SevenDiscount;
import com.vivo.service.VarService;
import com.vivo.utils.BeanInfoUtil;
import com.vivo.utils.DtoUtils;
import com.vivo.utils.RC4Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@SpringBootTest
@Slf4j
public class CategoryTest {
    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private VarService varService;

    @Autowired
    private CustomerMapper customerMapper;

    @Test
    public void test110() {
        System.out.println(JSON.toJSONString(customerMapper.swapDisplayOrder("fp_attr", 1, 2),
                SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue));
    }

    @Test
    public void test11() {
        System.out.println(JSON.toJSONString(categoryMapper.getCategory(2, 0),
                SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue));
    }

    @Test
    public void test66() {
        List<String> stringList = new ArrayList<>();
        stringList.add("1");
        stringList.add("2");
        stringList.add("4");
        stringList.add("3");

        stringList.stream().forEachOrdered((String i) -> {
            System.out.println(i);
        });
    }

    @Test
    public void test666() throws InterruptedException {
        new Client().request();
    }

    @Test
    public void test777() {
        System.out.println(new PriceContext(new SevenDiscount()).getDiscountPrice(100.00));
    }

    @Test
    public void test888() {
        Date date = new Date();
        System.out.println(date);
    }

    @Test
    public void test999() {
        BigDecimal totalAmount = new BigDecimal("10");
        BigDecimal currentAmount = new BigDecimal("10");
        BigDecimal nextAmount = currentAmount.add(new BigDecimal("2"));
        System.out.println((totalAmount.min(nextAmount).subtract(currentAmount)));
    }

    @Test
    public void test1000() {
        //Pattern p = Pattern.compile("A(120|((1[0-1]|\\d)?\\d))");
        //Matcher m = p.matcher("A18");
        //System.out.println(m.find());   //匹配aaa2223
        //System.out.println(m.group());   //返回0 返回第一组匹配到的子字符串在字符串中的索引号
        ////System.out.println(m.group());   //返回3 返回第一组匹配到的子字符串的最后一个字符在字符串中的索引位置.
        ////System.out.println(m.end(1));   //返回3 返回第一组匹配到的子字符串的最后一个字符在字符串中的索引位置.
        ////System.out.println(m.end(2));   //返回7
        ////System.out.println(m.group(1));   //返回aaa,返回第一组匹配到的子字符串
        ////System.out.println(m.group(2));   //返回2223,返回第二组匹配到的子字符串
        List<SalesLimit> salesLimit = new ArrayList<>();
        salesLimit.add(new SalesLimit("A0-A60", -1));
        salesLimit.add(new SalesLimit("A61-A65", 2000));
        System.out.println(JSON.toJSONString(salesLimit));
    }

    //获取值为null的字段名，并将值为空白字符（""和"  "）的字段值置为null
    public String[] getNullPropertyNames(Object source) {
        BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();
        List<String> emptyNames = new ArrayList<>();
        for (PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            } else if (srcValue instanceof String && StringUtils.isBlank((String) srcValue)) {
                src.setPropertyValue(pd.getName(), null);
            }
        }
        return emptyNames.toArray(new String[emptyNames.size()]);
    }

    @Test
    public void ttr() throws UnsupportedEncodingException {
        SalesLimit salesLimit = new SalesLimit("", null);
        System.out.println(salesLimit);
        System.out.println(getNullPropertyNames(salesLimit).length);
        System.out.println(salesLimit);
        System.out.println(JSON.parseArray(null, SalesLimit.class));
        //BeanWrapper src = new BeanWrapperImpl(salesLimit);
        //PropertyDescriptor[] pds = src.getPropertyDescriptors();
        //List<String> emptyNames = new ArrayList<>();
        //for (PropertyDescriptor pd : pds) {
        //    Object srcValue = src.getPropertyValue(pd.getName());
        //    if (srcValue == null) {
        //        emptyNames.add(pd.getName());
        //    } else if (srcValue instanceof String && StringUtils.isBlank((String) srcValue)) {
        //        src.setPropertyValue(pd.getName(), null);
        //    }
        //}
        //System.out.println(emptyNames.size());
        //System.out.println(salesLimit);
        //System.out.println(Arrays.asList(1, 2, 3, 4, 5, 6, 7).contains(null));
        //System.out.println(StringUtils.length(null));
        //System.out.println(StringUtils.length(""));
        //System.out.println(StringUtils.length("  "));
        //System.out.println(JSONObject.parseArray("   "));
        //String str1 = "hello";
        //String str2 = "你好abc";
        //
        //System.out.println("utf-8编码下'hello'所占的字节数:" + str1.getBytes("utf-8mb4").length);
        //System.out.println("gbk编码下'hello'所占的字节数:" + str1.getBytes("gbk").length);
        //
        //System.out.println("utf-8编码下'你好abc'所占的字节数:" + str2.getBytes("utf-8").length);
        //System.out.println("gbk编码下你好'你好abc'所占的字节数:" + str2.getBytes("gbk").length);
        System.out.println(StringUtils.length(null));
    }

    @Test
    public void jiami() {
        String key = "open20160501";
        String miwen = RC4Util.encryRC4String("123456789", key);
        System.out.println(miwen);
        String mingwen = RC4Util.decryRC4(miwen, key);
        System.out.println(mingwen);
    }

    @Test
    public void haha() throws Exception {
        Category category = new Category();
        category.setCategoryName("123");
        System.out.println(BeanInfoUtil.getProperty(category, "categoryName") instanceof List);
        System.out.println(BeanInfoUtil.getProperty(category, "categoryName") instanceof String);
        BeanInfoUtil.setProperty(category, "categoryName", "haha");
        System.out.println(BeanInfoUtil.getProperty(category, "categoryName"));
    }

    @Test
    public void tyty() throws Exception {
        //Category record = new Category();
        //Category dto=new Category();
        //dto.setCategoryId("1");
        //dto.setCategoryParentId("");
        //dto.setCategoryName(" ");
        //DtoUtils.createCopy(dto,record);
        //System.out.println(record.toString());

        Category record = new Category("1", "2", "hh", null, null);
        Category dto = new Category();
        dto.setCategoryId("111");
        dto.setCategoryParentId(null);
        dto.setCategoryName(" ");
        DtoUtils.modifyCopy(dto, record);
        System.out.println(record.toString());

    }

    @Test
    public void erer() {
        //Map<String, Object> map = new HashMap<>();
        //map.put("id", 1);
        //System.out.println(customerMapper.selectId(map));
        //map.put("is_deleted", 1);
        //System.out.println(customerMapper.selectId(map));
        //System.out.println(JSON.toJSONString(map));
        //System.out.println(JSON.toJSONString(""));
        //System.out.println(JSON.toJSONString(null));

        //BxEntity bxEntity=new BxEntity();
        //StockEntity stockEntity=new StockEntity();
        //stockEntity.setId(1L);
        //BeanUtils.copyProperties(stockEntity, bxEntity);
        //System.out.println(bxEntity.toString());

        Integer volume = 50000;
        String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        CloseableHttpResponse resp = null;
        try {
            HttpGet hget = new HttpGet(String.format("https://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_Bill.GetBillList?symbol=%s&num=1&page=1&sort=ticktime&asc=0&volume=%d&amount=0&type=0&day=%s", "sh600793", volume, today));
            hget.setHeader("referer", String.format("https://vip.stock.finance.sina.com.cn/quotes_service/view/cn_bill.php?symbol=%s", "sh600793"));
            //hget.setHeader("Cookie", "MONEY-FINANCE-SINA-COM-CN-WEB5=");
            //超时时间500ms
            hget.setConfig(RequestConfig.custom().setConnectTimeout(400).build());
            CloseableHttpClient httpClient = HttpClients.createDefault();
            resp = httpClient.execute(hget);
        } catch (IOException e1) {
        }
        String result = null;
        try {
            result = EntityUtils.toString(resp.getEntity());
        } catch (IOException e2) {
        }
        List<BigDealEntity> bigDealEntityList = JSON.parseArray(result, BigDealEntity.class);
        System.out.println(bigDealEntityList);
    }

    @Test
    public void ter() {
        //MaService maService=new MaService();
        //System.out.println(maService.calMa("sh515790"));
        //System.out.println(maService.calMa("sh512690"));
        //System.out.println(maService.calMa("sh600519"));
        //MacdService macdService =new MacdService();
        //System.out.println(macdService.calMacd("sh600519",60));
        //VolumeService volumeService=new VolumeService();
        //System.out.println(volumeService.calLastRealVolume("sh512690"));
        //System.out.println(volumeService.calLastRealVolume("sz399001"));
        //System.out.println(volumeService.calLastRealVolume("sz399006"));
        //System.out.println(volumeService.calLastRealVolume("sz399001"));
        //System.out.println(volumeService.calLastRealVolume("sz399006"));


    }
}
