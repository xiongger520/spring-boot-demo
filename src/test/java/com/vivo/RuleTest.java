package com.vivo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

@SpringBootTest
public class RuleTest {
    @Test
    public void test() {
        String id1 = "450122199402080014";
        String id2 = "110101199003076691";
        String id3 = "11010119900307889X";
        String id4 = "110101199003077774";
        String id5 = "11010120000229975X";
        String id6 = "110101200002294916";
        String id7 = "110101200002294094";
        String regex = "^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$";
        System.out.println(id1.matches(regex));
        System.out.println(id2.matches(regex));
        System.out.println(id3.matches(regex));
        System.out.println(id4.matches(regex));
        System.out.println(id5.matches(regex));
        System.out.println(id6.matches(regex));
        System.out.println(id7.matches(regex));
    }

    //根据生效时间和持续月间计算保障月间
    @Test
    public void calInsurancePeriod() {
        String effectiveTime = "D1";
        String duration = "M1";
        if (effectiveTime.charAt(0) != 'M') System.out.println("hhhh");
        LocalDateTime localDateTime = LocalDateTime.now().plusDays(Integer.valueOf(effectiveTime.substring(1)));
        localDateTime = localDateTime.withHour(0).withMinute(0).withSecond(0).withNano(0);
        String startDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDateTime);
        if (duration.equals("M1") && localDateTime.toLocalDate().isLeapYear() && localDateTime.getMonth() == Month.JANUARY && (localDateTime.getDayOfMonth() == 30 || localDateTime.getDayOfMonth() == 31)) {
            localDateTime = localDateTime.plusMonths(1).withHour(23).withMinute(59).withSecond(59).withNano(0);
        } else {
            switch (duration.charAt(0)) {
                case 'D': {
                    localDateTime = localDateTime.plusDays(Integer.valueOf(duration.substring(1)));
                    break;
                }
                case 'M': {
                    localDateTime = localDateTime.plusMonths(Integer.valueOf(duration.substring(1)));
                    break;
                }
                case 'Y': {
                    localDateTime = localDateTime.plusYears(Integer.valueOf(duration.substring(1)));
                    break;
                }
            }
            localDateTime = localDateTime.minusSeconds(1);
        }
        String endDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDateTime);
        System.out.println(startDateTime + "------>" + endDateTime);
    }
}
