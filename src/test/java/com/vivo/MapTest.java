package com.vivo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class MapTest {

    @Test
    public void test() {
        Map<String, Map<String, Long>> map = new HashMap<>();
        for (Integer i = 0; i < 5; i++) {
            Map<String, Long> tmp = new HashMap<>();
            tmp.put("panzhixiong", 60L);
            map.put(i.toString(), tmp);
        }
        //System.out.println(map.get("panzhixiong"));
        //System.out.println(JSON.toJSONString(map, SerializerFeature.PrettyFormat));
        Double num = 1234.99;
        System.out.println((int) (Math.ceil(num / 1000) * 1000));
        num = 999.00;
        System.out.println((int) (Math.ceil(num / 1000) * 1000));
        num = 2000.00;
        System.out.println((int) (Math.ceil(num / 1000) * 1000));
        num = 3456.00;
        System.out.println((int) (Math.ceil(num / 1000) * 1000));
    }
}
