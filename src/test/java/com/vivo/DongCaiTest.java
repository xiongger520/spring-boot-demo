package com.vivo;

import com.vivo.bx.DongCaiParse;
import com.vivo.dao.BxEntityMapper;
import com.vivo.entity.BxEntity;
import com.vivo.entity.BxEntityExample;
import com.vivo.entity.DongCaiEntity;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;


@SpringBootTest
public class DongCaiTest {

    private String baoxiaoUrl = "http://data.eastmoney.com/hsgtcg/BK0474.html";
    private String yiyaoUrl = "http://data.eastmoney.com/hsgtcg/BK0465.html";
    private String bandaotiUrl = "http://data.eastmoney.com/hsgtcg/BK0917.html";

    private List<String> urlList = Arrays.asList("1", "2");

    @Autowired
    private DongCaiParse dongcaiParse;

    @Autowired
    private BxEntityMapper bxEntityMapper;

    @Test
    public void test1() {

        DongCaiEntity dongCaiEntity1 = dongcaiParse.queryRecentData(baoxiaoUrl);
        //DongCaiEntity dongCaiEntity2 = dongcaiParse.queryRecentData(yiyaoUrl);
        //DongCaiEntity dongCaiEntity3 = dongcaiParse.queryRecentData(yiyaoUrl);

        BxEntityExample bxEntityExample = new BxEntityExample();
        bxEntityExample.createCriteria().andHyCodeEqualTo(dongCaiEntity1.getHYCode());
        bxEntityExample.limit(1);
        bxEntityExample.setOrderByClause("id desc");
        List<BxEntity> bxEntityList = bxEntityMapper.selectByExample(bxEntityExample);
        BxEntity rs = bxEntityList.get(0);
        if (DateUtils.isSameDay(rs.getDataDate(), dongCaiEntity1.getHdDate())) {
            System.out.println("不用更新");
        } else {
            bxEntityMapper.insertSelective(new BxEntity(null, dongCaiEntity1.getHYCode(), dongCaiEntity1.getHYName(), dongCaiEntity1.getHdDate(), dongCaiEntity1.getShareSZ_ZC() / 10e8, dongCaiEntity1.getShareSZ_GZ() / 10e8, null));
            System.out.println("更新完成");
        }

    }
}
