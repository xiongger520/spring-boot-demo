package com.vivo;

import com.alibaba.fastjson.JSON;
import com.vivo.entity.HawkingTestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;


@SpringBootTest
public class HawkingTest {

    @Test
    public void test66() {
        String tmp = "[\n" +
                "    {\n" +
                "    \"testName\":\"产品详情页头图\",\n" +
                "    \"testCode\":\"detail_head_picture\",\n" +
                "    \"paramName\":\"headPictureNumber\",\n" +
                "    \"fieldName\":[\"headPicture\"],\n" +
                "    \"status\":\"1\"\n" +
                "    },\n" +
                "    {\n" +
                "    \"testName\":\"产品详情页特色图\",\n" +
                "    \"testCode\":\"detail_product_feature\",\n" +
                "    \"paramName\":\"productFeaturePictureNumber\",\n" +
                "    \"fieldName\":[\"field1\",\"field2\"],\n" +
                "    \"status\":\"1\"\n" +
                "    }\n" +
                "]";
        List<HawkingTestConfig> hawkingTestConfigList = JSON.parseArray(tmp, HawkingTestConfig.class);
        if (!CollectionUtils.isEmpty(hawkingTestConfigList)) {
            List<String> testCodeList = new ArrayList<>();
            for (HawkingTestConfig i : hawkingTestConfigList) {
                if (1 == i.getStatus()) {
                    testCodeList.add(i.getTestCode());
                }
            }

        }
    }
}
