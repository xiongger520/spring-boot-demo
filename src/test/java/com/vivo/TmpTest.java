package com.vivo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.vivo.dao.TmpMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@SpringBootTest
public class TmpTest {

    @Resource
    private TmpMapper tmpMapper;

    @Test
    public void test() throws IOException {
        File file = new File("D:\\stock2.json");
        FileWriter writer = new FileWriter(file);
        writer.write(JSON.toJSONString(tmpMapper.selectAllStocksByProductCode("BWYLX001"), SerializerFeature.PrettyFormat));
        writer.flush();
        writer.close();
    }
}
