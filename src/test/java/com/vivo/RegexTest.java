package com.vivo;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SpringBootTest
public class RegexTest {

    @Test
    public void test11() {
        String s = "<p><strong>1、</strong>被保险人目前或过往未患有下列疾病：良、恶性肿瘤（含白血病），2级或以上高血压（收缩压大于160mmHg，舒张压大于100mmHg），冠心病，心肌梗死，脑梗死，脑出血，[风湿性心脏病|风湿性心脏病简称风心病，是指由于风湿热活动，累及心脏瓣膜而造成的心脏瓣膜病变。表现为二尖瓣、三尖瓣、主动脉瓣中有一个或几个瓣膜狭窄和（或）关闭不全。临床上狭窄或关闭不全常同时存在，但常以一种为主。患病初月常常无明显症状，后月则表现为心慌气短、乏力、咳嗽、下肢水肿、咳粉红色泡沫痰等心功能失代偿的表现。]，[心功能不全二级以上|心功能不全是由于各种原因造成心肌的收缩功能下降，使心脏前向性排血减少，造成血液淤滞在体循环或肺循环产生的症状。纽约心脏学会（NYHA）根据诱发心力衰竭症状（疲乏、心悸、气短或心绞痛）所需的活动量将心功能分为l-4级。心功能不全二级为轻微体力活动即有明显症状休息后稍减轻，体力活动大受限制。]，肾功能不全，肝炎（肝炎病毒携带者），肝硬化，[重型再障性贫血|重型再障性贫血是指因骨髓造血功能慢性持续性衰竭导致的贫血、中性粒细胞减少及血小板减少。须满足下列全部条件：（1）骨髓穿刺检查或骨髓活检结果支持诊断；（2）外周血象须具备以下三项条件：①中性粒细胞绝对值&le;0.5&times;109/L；②网织红细胞&le;1%；③血小板绝对值&le;20&times;109/L。]，[系统性红斑狼疮|系统性红斑狼疮是由多种因素引起，累及多系统的自身免疫性疾病。其特点是生成自身抗体对抗多种自身抗原，多见于育龄妇女。]，类风湿性关节炎，糖尿病，帕金森氏病，癫痫，精神病，[先天性疾病|先天性疾病就是一出生就有的病。母亲在怀孕月间接触环境有害因素，如农药、有机溶剂、重金属等化学品，或过量暴露在各种射线下，或服用某些药物，或染上某些病菌，甚至一些习惯爱好，如桑拿（蒸汽浴）和饮食癖好，都可能引起胎儿先天异常，但不属于遗传疾病。]，[法定传染病|法定传染病是指我国传染病防治法中所规定的传染病。甲类：鼠疫、霍乱。乙类：传染性非典型肺炎、艾滋病、甲型H1N1流感、病毒性肝炎、脊髓灰质炎、人感染高致病性禽流感、麻疹、流行性出血热、狂犬病、流行性乙型脑炎、登革热、炭疽、细菌性和阿米巴性痢疾、肺结核、伤寒和副伤寒、流行性脑脊髓膜炎、百日咳、白喉、新生儿破伤风、猩红热、布鲁氏菌病、淋病、梅毒、钩端螺旋体病、血吸虫病、疟疾。丙类：流行性感冒、流行性腮腺炎、风疹、急性出血性结膜炎、麻风病、流行性和地方性斑疹伤寒、黑热病、包虫病、丝虫病、手足口病，除霍乱、细菌性和阿米巴性痢疾、伤寒和副伤寒以外的感染性腹泻病。]（包含甲类及乙类），慢性阻塞性肺病、瘫痪；<br />\n" +
                "<strong>2、</strong>被保险人过去1年内未发现健康检查异常（如血液、超声、影像检查、内镜、病理检查等）；<br />\n" +
                "<strong>3、</strong>被保险人过去1年内不存在下列症状：反复头痛、晕厥、胸痛、气急、[紫绀|紫绀是指血液中去氧血红蛋白增多使皮肤和粘膜呈青紫色改变的一种表现。这种改变常发生在皮肤较薄，色素较少和毛细血管较丰富的部位，如：唇、指（趾）、甲床等。]、持续反复发热、抽搐、不明原因皮下出血点、咯血、反复呕吐、进食梗噎感或吞咽困难、呕血、浮肿、腹痛、黄疸、便血、血尿、[蛋白尿|蛋白尿是指由于肾小球滤过膜的滤过作用和肾小管的重吸收作用，健康人尿中蛋白质（多指分子量较小的蛋白质）的含量很少（每日排出量小于150mg），蛋白质定性检查时，呈阴性反应。当尿中蛋白质含量增加，普通尿常规检查即可测出，称蛋白尿。如果尿蛋白含量&ge;3.5g/24h，则称为大量蛋白尿。]、肿块、不明原因的消瘦（体重减轻5公斤以上）；&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;<br />\n" +
                "<strong>4、</strong>被保险人过去2年内没有过住院或被要求进一步检查、手术或住院治疗；<br />\n" +
                "<strong>5、</strong>2周岁以下被保险人出生时体重不低于2.5公斤，不存在早产、窒息、发育迟缓、脑瘫情况。</p>\n" +
                "\n" +
                "<p>以下情况可作为例外事项，仍符合投保条件：<br />\n" +
                "<strong>（1）针对以上第2条健康检查异常，如满足以下情况，则为例外事项，可进行投保：</strong><br />\n" +
                "&nbsp;&nbsp; &nbsp;（a）甲状腺异常：甲状腺囊肿、甲状腺结节伴甲状腺功能检查正常并经核素扫描证实为功能性结节的；轻度脂肪肝：腹部B超提示为轻/中度脂肪肝且肝功能正常；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（b）血脂偏高：未经药物控制的情况下,甘油三脂＜4mmol/L且总胆固醇&lt;7mmol/L；高/低血压：收缩压90mmHg~160mmHg、舒张压60mmHg~100mmHg；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（c）鼻炎、咽炎、慢性扁桃体炎、腺样体肥大、慢性浅表性胃炎；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（d）病毒性感冒引起的血常规检查异常。<br />\n" +
                "<strong>（2）针对以上第4条住院病史，如满足以下情况，则为例外事项，可进行投保：</strong><br />\n" +
                "&nbsp;&nbsp; &nbsp;（a）分娩；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（b）急性呼吸系统疾病；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（c）急性胃肠炎、急性阑尾炎；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（d）胆囊结石已进行胆囊切除手术；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（e）胆囊结石已进行胆囊切除手术；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（f）胆囊息肉已进行胆囊切除手术且病理结果为良性；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（g）意外受伤引起的软组织损伤；<br />\n" +
                "&nbsp;&nbsp; &nbsp;（h）意外住院在5天以内且已痊愈，并无后遗症或器官缺损。</p>\n";

        Matcher m = Pattern.compile("\\[[^\\]]+\\|[^\\]]+\\]").matcher(s);
        while (m.find()) {
            System.out.println(m.group());
            System.out.println(m.start() + "--->" + m.end());
        }
    }

    @Test
    public void test33() {
        String s = "var hq_str_s_sz000009=\"中国宝安,7.20,-0.02,-0.28,220727,15961\";\n" +
                "var hq_str_s_sz002459=\"晶澳科技,32.83,0.83,2.59,83951,27406\";";
        Matcher m = Pattern.compile("\\d{6}=\\\".+\\\"").matcher(s);
        while (m.find()) {
            String mes1 = m.group();
            System.out.println(mes1);
            String mes2 = mes1.substring(0, 6);
            System.out.println(mes1.split(",")[0]);
            System.out.println(mes1.split(",")[2]);
        }
    }


    @Test
    public void test5555() {
        String s = "1";
        System.out.println((!StringUtils.isBlank(s) && !Pattern.compile("^\\w+$").matcher(s).find()));
        s = "-1";
        System.out.println((!StringUtils.isBlank(s) && !Pattern.compile("^\\w+$").matcher(s).find()));
        s = "";
        System.out.println((!StringUtils.isBlank(s) && !Pattern.compile("^\\w+$").matcher(s).find()));
    }
}
