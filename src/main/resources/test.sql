/*
 Navicat Premium Data Transfer

 Source Server         : 1.localhost
 Source Server Type    : MySQL
 Source Server Version : 50625
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50625
 File Encoding         : 65001

 Date: 01/07/2020 18:00:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_card
-- ----------------------------
DROP TABLE IF EXISTS `tb_card`;
CREATE TABLE `tb_card`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `card_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证号',
  `version` bigint(20) DEFAULT NULL COMMENT '版本号（乐观锁）',
  `is_deleted` int(11) DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_card_code`(`card_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '身份证表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_card
-- ----------------------------
INSERT INTO `tb_card` VALUES (1, '110101199003072674', 1, 0);
INSERT INTO `tb_card` VALUES (2, '110101199003077977', 1, 0);
INSERT INTO `tb_card` VALUES (3, '110101199003075234', 1, 0);

-- ----------------------------
-- Table structure for tb_check_in
-- ----------------------------
DROP TABLE IF EXISTS `tb_check_in`;
CREATE TABLE `tb_check_in`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `open_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'vivo账号id',
  `member_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '钱包账号id',
  `biz_type` int(11) DEFAULT NULL COMMENT '业务类型：1-自营贷款，2-引流业务，3-保险业务',
  `biz_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '业务编号',
  `serial_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '业务流水号',
  `last_date` date DEFAULT NULL COMMENT '上一次签到日月',
  `con_days` int(11) DEFAULT NULL COMMENT '连续签到天数',
  `version` bigint(20) DEFAULT NULL COMMENT '版本（乐观锁）',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除：0-不删除，1-删除',
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_biz_code_serial`(`biz_code`, `serial_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '签到表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_check_in
-- ----------------------------
INSERT INTO `tb_check_in` VALUES (1, '1', NULL, 3, 'ZJZX001', '1001', '2020-07-01', 7, 6, 0, '2020-06-30 21:45:19', '2020-07-01 11:00:21');

-- ----------------------------
-- Table structure for tb_check_in_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_check_in_log`;
CREATE TABLE `tb_check_in_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `open_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'vivo账号id',
  `member_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '钱包账号id',
  `biz_type` int(11) DEFAULT NULL COMMENT '业务类型：1-自营贷款，2-引流业务，3-保险业务',
  `biz_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '业务编号',
  `serial_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '业务流水号',
  `check_in_time` datetime(0) DEFAULT NULL COMMENT '签到时间',
  `version` bigint(20) DEFAULT NULL COMMENT '版本（乐观锁）',
  `is_deleted` int(4) DEFAULT NULL COMMENT '是否删除：0-不删除，1-删除',
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_biz_code_serial`(`biz_code`, `serial_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '签到记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_check_in_log
-- ----------------------------
INSERT INTO `tb_check_in_log` VALUES (1, '1', NULL, 3, 'ZJZX001', '1001', '2020-06-29 21:45:19', 1, 0, '2020-06-30 21:45:19', '2020-06-30 21:46:40');
INSERT INTO `tb_check_in_log` VALUES (2, '1', NULL, 3, 'ZJZX001', '1001', '2020-06-30 21:46:51', 1, 0, '2020-06-30 21:46:50', '2020-06-30 21:46:50');
INSERT INTO `tb_check_in_log` VALUES (3, '1', NULL, 3, 'ZJZX001', '1001', '2020-07-01 10:01:06', 1, 0, '2020-07-01 10:01:05', '2020-07-01 10:01:05');
INSERT INTO `tb_check_in_log` VALUES (4, '1', NULL, 3, 'ZJZX001', '1001', '2020-07-01 10:55:41', 1, 0, '2020-07-01 10:55:40', '2020-07-01 10:55:40');
INSERT INTO `tb_check_in_log` VALUES (5, '1', NULL, 3, 'ZJZX001', '1001', '2020-07-01 11:00:21', 1, 0, '2020-07-01 11:00:21', '2020-07-01 11:00:21');

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `order_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单编号',
  `user_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT '用户表id',
  `version` bigint(20) DEFAULT NULL COMMENT '版本号（乐观锁）',
  `is_deleted` int(11) DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_order_code`(`order_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_order
-- ----------------------------
INSERT INTO `tb_order` VALUES (1, 'D001', 1, 1, 0);
INSERT INTO `tb_order` VALUES (2, 'D002', 1, 1, 0);
INSERT INTO `tb_order` VALUES (3, 'D003', 1, 1, 0);
INSERT INTO `tb_order` VALUES (4, 'D004', 3, 1, 0);
INSERT INTO `tb_order` VALUES (5, 'D005', 2, 1, 0);
INSERT INTO `tb_order` VALUES (6, 'D006', 2, 1, 0);

-- ----------------------------
-- Table structure for tb_order_product_rel
-- ----------------------------
DROP TABLE IF EXISTS `tb_order_product_rel`;
CREATE TABLE `tb_order_product_rel`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单表id',
  `product_id` bigint(20) DEFAULT NULL COMMENT '产品表id',
  `version` bigint(20) DEFAULT NULL COMMENT '版本号（乐观锁）',
  `is_deleted` int(11) DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单与产品关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_order_product_rel
-- ----------------------------
INSERT INTO `tb_order_product_rel` VALUES (1, 1, 1, 1, 0);
INSERT INTO `tb_order_product_rel` VALUES (2, 1, 2, 1, 0);
INSERT INTO `tb_order_product_rel` VALUES (3, 1, 3, 1, 0);
INSERT INTO `tb_order_product_rel` VALUES (4, 2, 2, 1, 0);
INSERT INTO `tb_order_product_rel` VALUES (5, 2, 1, 1, 0);
INSERT INTO `tb_order_product_rel` VALUES (6, 3, 3, 1, 0);

-- ----------------------------
-- Table structure for tb_person
-- ----------------------------
DROP TABLE IF EXISTS `tb_person`;
CREATE TABLE `tb_person`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `age` int(10) UNSIGNED DEFAULT NULL COMMENT '年龄',
  `sex` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '性别',
  `card_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT '身份证表id',
  `version` bigint(20) DEFAULT NULL COMMENT '版本号（乐观锁）',
  `is_deleted` int(11) NOT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_code_id`(`card_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '人员表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_person
-- ----------------------------
INSERT INTO `tb_person` VALUES (1, '张三', 22, '男', 1, 1, 0);
INSERT INTO `tb_person` VALUES (2, '李四', 23, '女', 2, 1, 0);
INSERT INTO `tb_person` VALUES (3, '王五', 24, '男', 3, 1, 0);

-- ----------------------------
-- Table structure for tb_product
-- ----------------------------
DROP TABLE IF EXISTS `tb_product`;
CREATE TABLE `tb_product`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '产品名称',
  `price` decimal(10, 2) DEFAULT NULL COMMENT '价格',
  `version` bigint(255) DEFAULT NULL COMMENT '版本号（乐观锁）',
  `is_deleted` int(11) DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_product
-- ----------------------------
INSERT INTO `tb_product` VALUES (1, '书本1', 1.00, 1, 0);
INSERT INTO `tb_product` VALUES (2, '书本2', 2.00, 1, 0);
INSERT INTO `tb_product` VALUES (3, '书本3', 3.00, 1, 0);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '自增主键',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地址',
  `version` bigint(20) DEFAULT NULL COMMENT '版本号（乐观锁）',
  `is_deleted` int(11) DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, '张三', '地址1', 1, 0);
INSERT INTO `tb_user` VALUES (2, '李四', '地址2', 1, 0);
INSERT INTO `tb_user` VALUES (3, '王五', '地址3', 1, 0);

SET FOREIGN_KEY_CHECKS = 1;
