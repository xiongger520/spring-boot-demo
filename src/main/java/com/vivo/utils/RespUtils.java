package com.vivo.utils;

import com.vivo.dto.ProductCenterRespDTO;
import com.vivo.enums.ExceptionEnum;

//相应格式化工具类
public class RespUtils {

    public static <T> ProductCenterRespDTO<T> ok() {
        return make(ExceptionEnum.SUCCESS.getCode(), ExceptionEnum.SUCCESS.getMsg(), null);
    }

    public static <T> ProductCenterRespDTO<T> ok(T t) {
        return make(ExceptionEnum.SUCCESS.getCode(), ExceptionEnum.SUCCESS.getMsg(), t);
    }

    public static <T> ProductCenterRespDTO<T> error() {
        return make(ExceptionEnum.INTERNAL_SERVICE_ERROR.getCode(), ExceptionEnum.INTERNAL_SERVICE_ERROR.getMsg(), null);
    }

    public static <T> ProductCenterRespDTO<T> make(String code, String msg, T t) {
        ProductCenterRespDTO<T> resp = new ProductCenterRespDTO<>();
        resp.setCode(code);
        resp.setMsg(msg);
        resp.setData(t == null ? null : t);
        return resp;
    }
}