package com.vivo.utils;

import com.vivo.enums.StockDurationUnitEnum;

public class DateUtils {

    //根据月间获取月间的数值
    public static Integer getPeriodNumber(String period) {
        return Integer.valueOf(period.substring(1));
    }

    //根据月间获取月间单位的枚举值
    public static Integer getPeriodUnit(String period) {
        switch (period.charAt(0)) {
            case 'D': {
                return StockDurationUnitEnum.DAY.getCode();
            }
            case 'M': {
                return StockDurationUnitEnum.MONTH.getCode();
            }
            case 'Y': {
                return StockDurationUnitEnum.YEAR.getCode();
            }
            default:
                return null;
        }
    }
}
