package com.vivo.utils;

import com.vivo.entity.Category;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

public class BeanInfoUtil {

    // 设置bean的某个属性值
    public static void setProperty(Category category, String categoryName, String value) throws Exception {
        // 获取bean的某个属性的描述符
        PropertyDescriptor propDesc = new PropertyDescriptor(categoryName, Category.class);
        // 获得用于写入属性值的方法
        Method methodSetUserName = propDesc.getWriteMethod();
        // 写入属性值
        methodSetUserName.invoke(category, value);
    }

    // 获取bean的某个属性值
    public static Object getProperty(Category category, String categoryName) throws Exception {
        // 获取Bean的某个属性的描述符
        PropertyDescriptor proDescriptor = new PropertyDescriptor(categoryName, Category.class);
        // 获得用于读取属性值的方法
        Method methodGetUserName = proDescriptor.getReadMethod();
        // 读取属性值
        Object objUserName = methodGetUserName.invoke(category);
        return objUserName;
    }
}
