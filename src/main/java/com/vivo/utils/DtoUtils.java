package com.vivo.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;

/**
 * Dto工具类
 *
 * @version:1.0.0
 */
public class DtoUtils {

    //创建时复制方法，可以忽略空白字符字段
    public static void createCopy(Object source, Object target) {
        BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();
        List<String> blankFieldNameList = new ArrayList<>();
        for (PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null || (srcValue instanceof String && StringUtils.isBlank((String) srcValue))) {
                blankFieldNameList.add(pd.getName());
            }
        }
        BeanUtils.copyProperties(source, target, blankFieldNameList.toArray(new String[blankFieldNameList.size()]));
    }

    //修改时复制方法，通过null以外的空白字符可以抹去原值
    public static void modifyCopy(Object source, Object target) {
        BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();
        List<String> blankFieldNameList = new ArrayList<>();
        for (PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                blankFieldNameList.add(pd.getName());
            } else if (srcValue instanceof String && StringUtils.isBlank((String) srcValue)) {
                src.setPropertyValue(pd.getName(), null);
            }
        }
        BeanUtils.copyProperties(source, target, blankFieldNameList.toArray(new String[blankFieldNameList.size()]));
    }
}