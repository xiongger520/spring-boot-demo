//package com.vivo;
//
//import org.apache.catalina.Context;
//import org.apache.catalina.Wrapper;
//import org.springframework.boot.web.embedded.tomcat.TomcatContextCustomizer;
//import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
//import org.springframework.boot.web.server.WebServerFactoryCustomizer;
//import org.springframework.stereotype.Component;
//
//import java.io.File;
//
//@Component
//public class CustomTomcat implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {
//
//    public String sourcePath = "/data/finance-product/FTP/Axure/";
//
//    @Override
//    public void customize(TomcatServletWebServerFactory factory) {
//        factory.addContextCustomizers(new TomcatContextCustomizer() {
//            @Override
//            public void customize(Context context) {
//                File file = new File(sourcePath);
//                if (!file.exists()) {
//                    file.mkdirs();
//                }
//                context.setDocBase(file.getAbsolutePath());
//                Wrapper defServlet = (Wrapper) context.findChild("default");
//                defServlet.addInitParameter("listings", "true");
//                defServlet.addInitParameter("readOnly", "false");
//                defServlet.addMapping("/*");
//            }
//        });
//    }
//}
