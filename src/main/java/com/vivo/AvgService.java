package com.vivo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import lombok.Data;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

//分时服务
@Data
public class AvgService {

    private CloseableHttpClient httpClient = HttpClients.createDefault();
    private Map<String, HttpGet> map = new HashMap<>();
    //东财代码、已经统计的索引值、累计值
    private Map<String, MutablePair<Integer, Integer>> cache = new HashMap<>();
    //执行器
    private ExecutorService executorService = Executors.newFixedThreadPool(8);

    public List<Double> calAvgPercentConcurrent(List<String> list) {
        List<Double> result = new ArrayList<>();
        List<FutureTask<Double>> futureTaskList = new ArrayList<>();
        for (String eastCode : list) {
            FutureTask<Double> futureTask = new FutureTask<>(new Callable<Double>() {
                @Override
                public Double call() throws Exception {
                    return calAvgPercent(eastCode);
                }
            });
            futureTaskList.add(futureTask);
            executorService.submit(futureTask);
        }
        try {
            for (FutureTask<Double> futureTask : futureTaskList) {
                result.add(futureTask.get());
            }
        } catch (Exception e) {
        }
        return result;
    }

    //计算均价线上比例
    public Double calAvgPercent(String eastCode) {
        Double percent = 0.0;
        try {
            HttpGet httpGet = map.get(eastCode);
            if (null == httpGet) {
                httpGet = new HttpGet(String.format("http://push2.eastmoney.com/api/qt/stock/trends2/get?fields1=f1&fields2=f53,f58&&iscr=0&secid=%s", eastCode));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(300).build());
                map.put(eastCode, httpGet);
            }
            String result = EntityUtils.toString(httpClient.execute(httpGet).getEntity());
            JSONArray jsonArray = JSON.parseObject(result).getJSONObject("data").getJSONArray("trends");
            Integer idx = 0;
            Integer count = 0;
            Integer size = jsonArray.size();
            MutablePair<Integer, Integer> mutablePair = cache.get(eastCode);
            //从缓存中读取已经统计的索引值、累计值
            if (null != mutablePair) {
                idx = mutablePair.getLeft();
                count = mutablePair.getRight();
            }
            for (; idx + 1 < size; idx++) {
                String[] arr = String.valueOf(jsonArray.get(idx + 1)).split(",");
                if (Double.valueOf(arr[1]) >= Double.valueOf(arr[2])) {
                    count++;
                }
            }
            //更新缓存的值
            if (null != mutablePair) {
                mutablePair.setLeft(idx);
                mutablePair.setRight(count);
            } else {
                cache.put(eastCode, MutablePair.of(idx, count));
            }
            percent = count * 1.0 / size * 100;
        } catch (Exception e) {
            System.out.println("cal " + eastCode.substring(2) + " avgPercent error!");
        }
        return percent;
    }
}
