package com.vivo.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpression {
    public static void main(String[] args) {
        //除了字母数字下划线之外的字符为非法字符
        Pattern pattern = Pattern.compile("[^a-zA-Z0-9-]");
        //指定设置非法字符
        //String str = "123@abc     #D     EF";
        String str = "TPYBX456-123";
        Matcher matcher = pattern.matcher(str);
        StringBuffer buffer = new StringBuffer();
        //如果找到非法字符
        while (matcher.find()) {
            //如果里面包含非法字符如冒号双引号等，那么就把他们消去，并把非法字符前面的字符放到缓冲区
            matcher.appendReplacement(buffer, "");
        }
        //将剩余的合法部分添加到缓冲区
        matcher.appendTail(buffer);
        System.out.println("您的输入为: " + str);
        System.out.println("合法的输出为: " + buffer.toString());
    }
}
