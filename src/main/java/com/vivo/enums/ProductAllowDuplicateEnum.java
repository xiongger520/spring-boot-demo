package com.vivo.enums;

//产品允许是否重复投保枚举
public enum ProductAllowDuplicateEnum {
    YES(1, "允许"), NO(2, "不允许");;
    private int code;
    private String desc;

    ProductAllowDuplicateEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
