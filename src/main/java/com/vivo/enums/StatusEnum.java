package com.vivo.enums;

//状态枚举
public enum StatusEnum {
    ENABLE(1, "启用"), DISABLE(0, "禁用");
    private int code;
    private String desc;

    StatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
