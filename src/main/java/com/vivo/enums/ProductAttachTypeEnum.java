package com.vivo.enums;

//产品附件类型枚举
public enum ProductAttachTypeEnum {
    PRODUCT_FEATURE(1, "产品特色"), FAQ(2, "常见问题"), CLAIM_EXPLANATION(3, "理赔流程");
    private int code;
    private String desc;

    ProductAttachTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
