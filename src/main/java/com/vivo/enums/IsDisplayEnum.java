package com.vivo.enums;

//是否显示枚举
public enum IsDisplayEnum {
    YES(1, "显示"), NO(0, "不显示");
    private Integer code;
    private String desc;

    IsDisplayEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
