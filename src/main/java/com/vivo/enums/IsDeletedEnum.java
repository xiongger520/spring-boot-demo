package com.vivo.enums;

//是否删除枚举
public enum IsDeletedEnum {
    YES(1, "已删除"), NO(0, "未删除");
    private Integer code;
    private String desc;

    IsDeletedEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
