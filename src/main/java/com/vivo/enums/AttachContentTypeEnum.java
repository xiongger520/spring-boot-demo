package com.vivo.enums;

//附件内容类型枚举
public enum AttachContentTypeEnum {
    LINK(1, "网址链接"), TEXT(2, "（富）文本");
    private int code;
    private String desc;

    AttachContentTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
