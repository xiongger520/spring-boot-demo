package com.vivo.enums;

//产品分类类型枚举
public enum ProductCategoryTypeEnum {
    INSURANCE(1, "保险业务"), INSTALLMENT_CONSUMPTION(2, "消费分月"), LOAN(3, "贷款");;
    private int code;
    private String desc;

    ProductCategoryTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
