package com.vivo.enums;

//库存持续月间单位枚举
public enum StockDurationUnitEnum {
    DAY(1, "天"), MONTH(2, "月"), YEAR(3, "年"), LIFETIME(4, "终身");;
    private int code;
    private String desc;

    StockDurationUnitEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
