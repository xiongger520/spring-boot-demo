package com.vivo.enums;

public enum ExceptionEnum {
    SUCCESS("000000", "调用成功"),
    INTERNAL_SERVICE_ERROR("500", "内部服务异常"),
    PRODUCT_INVALID("601", "产品已下架"),
    PARAMETER_ERROR("00001", "参数错误"),
    ;
    private String code;
    private String msg;

    ExceptionEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
