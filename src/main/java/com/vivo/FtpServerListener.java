package com.vivo;

import lombok.extern.slf4j.Slf4j;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.impl.DefaultFtpServer;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.WritePermission;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class FtpServerListener implements ServletContextListener {

    public String ftpPath = "/data/finance-product/FTP/";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            FtpServerFactory factory = new FtpServerFactory();
            ListenerFactory listenerFactory = new ListenerFactory();
            listenerFactory.setPort(21);
            factory.addListener("default", listenerFactory.createListener());

            FtpServer server = factory.createServer();
            BaseUser user = new BaseUser();
            user.setName("anonymous");
            File file = new File(ftpPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            user.setHomeDirectory(file.getAbsolutePath());
            //赋予写权限
            List<Authority> authorities = new ArrayList<Authority>();
            authorities.add(new WritePermission());
            user.setAuthorities(authorities);
            factory.getUserManager().save(user);
            sce.getServletContext().setAttribute("default", server);
            server.start();
            log.info("FTP服务启动!");
        } catch (FtpException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        DefaultFtpServer server = (DefaultFtpServer) sce.getServletContext().getAttribute("FTPSERVER_CONTEXT_NAME");
        if (server != null) {
            server.stop();
            log.info("FTP服务关闭!");
        }
    }
}
