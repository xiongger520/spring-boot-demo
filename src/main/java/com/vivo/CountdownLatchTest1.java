package com.vivo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CountdownLatchTest1 {

    public static void main(String[] args) throws InterruptedException {
        Integer threadSize = 5;
        Integer dataSize = 240;
        Integer per = dataSize / threadSize;
        ExecutorService service = Executors.newFixedThreadPool(threadSize);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CountDownLatch countDownLatch = new CountDownLatch(threadSize);
        String[] datas = new String[dataSize];
        JSONArray[] results = new JSONArray[dataSize];
        for (int i = 0; i < dataSize; i++) {
            datas[i] = "https://hq.techgp.cn/rjhy-gmg-quote/api/1/stock/quote/v1/historyv1?endtime=1637324451&inst=002459&limit=-240&market=SZ&servicetype=KLINEB&starttime=0&resptype=JSON&period=MIN1";
        }
        long t1 = System.currentTimeMillis();
        for (int i = 0; i < threadSize; i++) {
            Integer start = i * per;
            Integer end = (i + 1) < threadSize ? (i + 1) * per : dataSize;
            service.execute(new WorkFun(countDownLatch, httpClient, datas, start, end, results));
        }
        countDownLatch.await();
        System.out.println("-------------->");
        for (int i = 0; i < dataSize; i++) {
            System.out.println(results[i].size());
        }
        System.out.println("cost:" + (System.currentTimeMillis() - t1));
        service.shutdown();
    }
}

class WorkFun implements Runnable {

    private CountDownLatch countDownLatch;
    private CloseableHttpClient httpClient;
    private String[] datas = null;
    private int start;
    private int end;
    private JSONArray[] results = null;
    private HttpGet httpGet;

    public WorkFun(CountDownLatch countDownLatch, CloseableHttpClient httpClient, String[] datas, int start, int end, JSONArray[] results) {
        this.countDownLatch = countDownLatch;
        this.httpClient = httpClient;
        this.datas = datas;
        this.start = start;
        this.end = end;
        this.results = results;
        httpGet = new HttpGet();
        httpGet.setConfig(RequestConfig.custom().setConnectTimeout(500).build());
    }

    @Override
    public void run() {
        for (Integer i = start; i < end; i++) {
            httpGet.setURI(URI.create(datas[i]));
            JSONArray jsonArray = null;
            try {
                jsonArray = JSON.parseObject(EntityUtils.toString(httpClient.execute(httpGet).getEntity())).getJSONArray("KlineData");
            } catch (IOException e) {
                e.printStackTrace();
            }
            results[i] = jsonArray;
        }
        countDownLatch.countDown();
    }
}
