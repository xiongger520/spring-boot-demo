package com.vivo.bx;

import com.vivo.entity.DongCaiEntity;

public interface DongCaiParse {
    public DongCaiEntity queryRecentData(String url);

    public void saveDAta(DongCaiEntity dongCaiEntity);
}
