package com.vivo.bx.impl;


import com.alibaba.fastjson.JSONObject;
import com.vivo.bx.DongCaiParse;
import com.vivo.entity.DongCaiEntity;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class DongCaiParseImpl implements DongCaiParse {

    @Override
    public DongCaiEntity queryRecentData(String url) {
        String htmlResult = SendHttpRequest(url, "GET", null);
        //删除空白字符
        htmlResult = Pattern.compile("\\s").matcher(htmlResult).replaceAll("");
        Matcher m = Pattern.compile("chart1\\:\\{\\\"data\\\"\\:\\[\\{[\\\"\\w\\:\\,\\u4e00-\\u9fa5\\-\\.]*\\}").matcher(htmlResult);
        String rs = null;
        if (m.find()) {
            rs = m.group();
            return JSONObject.parseObject(rs.substring(16), DongCaiEntity.class);
        } else {
            return null;
        }
    }

    @Override
    public void saveDAta(DongCaiEntity dongCaiEntity) {

    }

    //发送http请求  requestUrl为请求地址  requestMethod请求方式，值为"GET"或"POST"
    private String SendHttpRequest(String requestUrl, String requestMethod, String outputStr) {
        StringBuffer buffer = null;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod(requestMethod);
            conn.connect();
            //往服务器端写内容 也就是发起http请求需要带的参数
            if (null != outputStr) {
                OutputStream os = conn.getOutputStream();
                os.write(outputStr.getBytes("utf-8"));
                os.close();
            }
            //读取服务器端返回的内容
            InputStream is = conn.getInputStream();
            InputStreamReader isr = new InputStreamReader(is, "gbk");
            BufferedReader br = new BufferedReader(isr);
            buffer = new StringBuffer();
            String line = null;
            while ((line = br.readLine()) != null) {
                buffer.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }
}
