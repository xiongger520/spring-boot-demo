package com.vivo.controller;

import com.vivo.entity.Person;
//import com.vivo.rabbitmq.MessageProvider;
//import com.vivo.rabbitmq.RabbitMQConfig;
import com.vivo.redis.FindPerson;
import com.vivo.service.PersonService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@Slf4j
@Api(tags = {"用户操作接口"})
public class PersonController {

    @Autowired
    PersonService personService;

    @Autowired
    private FindPerson findPerson;

    @RequestMapping("/getAllPerson")
    @ApiOperation(value = "获取所有人的信息", notes = "提示接口使用者注意事项", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页数", dataType = "int", allowableValues = "1,2", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页信息条数", dataType = "int", allowableValues = "2,3", required = true)
    })
    public List<Person> getAllPerson(@ApiParam(name = "page", required = true) Integer page, @ApiParam(name = "pageSize", required = true) Integer pageSize) {
        System.out.println("正在访问/getAllPerson-------");
        return personService.getAllPerson(page, pageSize);
    }

    @RequestMapping("/addPerson")
    @ApiOperation(value = "增加一个人", notes = "提示接口使用者注意事项", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户编号", dataType = "int", allowableValues = "1,2,3", required = true),
            @ApiImplicitParam(name = "name", value = "姓名", dataType = "String", allowableValues = "潘志雄", required = true),
            @ApiImplicitParam(name = "sex", value = "性别", dataType = "String", allowableValues = "男,女", required = true),
            @ApiImplicitParam(name = "age", value = "年龄", dataType = "int", allowableValues = "22,23,24,25,26", required = true)
    })
    public void addPerson(Person person) {
        System.out.println(person);
    }

    @RequestMapping("/getPerson")
    @ApiIgnore
    public Person getPerson(Integer id) {
        return findPerson.findPersonById(id);
    }

    @RequestMapping("/deletePerson")
    @ApiIgnore
    public void deletePerson(Integer id) {
        findPerson.deletePerson(id);
    }

//    @Autowired
//    MessageProvider messageProvider;
//
//    @RequestMapping("/rmq/direct")
//    @ApiIgnore
//    public String direct(String routingkey, String msg) {
//        messageProvider.sendMessage(RabbitMQConfig.DIRECT_EXCHANGE, routingkey, msg);
//        return "ok!";
//    }
//
//    @RequestMapping("/rmq/fanout")
//    @ApiIgnore
//    public String fanout(String msg) {
//        messageProvider.sendMessage(RabbitMQConfig.FANOUT_EXCHANGE, "", msg);
//        return "ok!";
//    }
//
//    @RequestMapping("/rmq/topic")
//    @ApiIgnore
//    public String topic(String routingkey, String msg) {
//        messageProvider.sendMessage(RabbitMQConfig.TOPIC_EXCHANGE, routingkey, msg);
//        return "ok!";
//    }
//
//    @RequestMapping("/rmq/delay")
//    @ApiIgnore
//    public String delay(String routingkey, String msg) {
//        messageProvider.sendMessage(RabbitMQConfig.DELAY_EXCHANGE, routingkey, msg);
//        return "ok!";
//    }


}