package com.vivo.controller;

import com.vivo.MyAnnotation;
import com.vivo.entity.Person;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import springfox.documentation.annotations.ApiIgnore;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

@RestController
@ApiIgnore
public class SimpleController {

    @RequestMapping("/hello")
    @MyAnnotation(value = "panzhixiong")
    public String hello() {
        return "Hello World!";
    }

    //字符串
    @RequestMapping("/jsonEmptyString")
    public String jsonEmptyString() {
        //JackSon返回空白
        //FastJSON返回""
        return new String();
    }

    @RequestMapping("/jsonNullString")
    public String jsonNullString() {
        //JackSon返回空白
        //FastJSON返回空白
        return null;
    }

    //List
    @RequestMapping("/jsonList")
    public List<Person> jsonList() {
        List<Person> list = new ArrayList<>();
        list.add(new Person(1, "潘志雄", "男", 25));
        //JackSon正常返回
        //FastJSON正常返回
        return list;
    }

    @RequestMapping("/jsonNullList")
    public List<Person> jsonNullList() {
        //JackSon返回空白
        //FastJSON返回空白
        return null;
    }

    @RequestMapping("/jsonEmptyList")
    public List<Person> jsonEmptyList() {
        List<Person> personList = new ArrayList<>();
        //JackSon返回[]
        //FastJSON返回[]
        return personList;
    }

    //Map
    @RequestMapping("/jsonMap")
    public Map<String, String> jsonMap() {
        Map<String, String> stringPersonMap = new HashMap<>();
        stringPersonMap.put("panzhixiong", "潘志雄");
        stringPersonMap.put("pzx", null);
        //JackSon正常返回{……}，pzx的value返回null(不是字符串)
        //FastJSON正常返回{……}，pzx的value返回null(不是字符串)
        return stringPersonMap;
    }

    @RequestMapping("/jsonEmptyMap")
    public Map<String, Person> jsonEmptyMap() {
        Map<String, Person> stringPersonMap = new HashMap<>();
        //JackSon返回{}
        //FastJSON返回{}
        return stringPersonMap;
    }

    @RequestMapping("/jsonNullMap")
    public Map<String, Person> jsonNullMap() {
        //JackSon返回空白
        //FastJSON返回空白
        return null;
    }

    //Date
    @RequestMapping("/jsonDate")
    public Date jsonDate() {
        //JackSon返回1578714789139
        //FastJSON返回"2020-01-11 12:00:12"
        return new Date();
    }

    @PostMapping("/springUpload")
    public String springUpload(HttpServletRequest request) throws IllegalStateException, IOException {
        long startTime = System.currentTimeMillis();
        //将当前上下文初始化给  CommonsMutipartResolver （多部分解析器）
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        //检查form中是否有enctype="multipart/form-data"
        if (multipartResolver.isMultipart(request)) {
            //将request变成多部分request
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            //获取multiRequest 中所有的文件名
            Iterator iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                //一次遍历所有文件
                MultipartFile file = multiRequest.getFile(iter.next().toString());
                if (file != null) {
                    String path = "D:\\springUpload\\" + UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
                    //上传
                    file.transferTo(new File(path));
                }
            }
        }
        long endTime = System.currentTimeMillis();
        return "Spring方法的运行时间：" + (endTime - startTime) + "ms";
    }

    //使用该方法不用包含jar包：commons-fileupload和commons-io
    @PostMapping("/fileUpload2")
    public String fileUpload2(@RequestParam("file") MultipartFile file) throws IOException {
        long startTime = System.currentTimeMillis();
        String path = "D:\\fileUpload2\\" + UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
        String originalFilename = file.getOriginalFilename();
        String suffixName = originalFilename.substring(originalFilename.lastIndexOf("."));
        if (".png".equals(suffixName)) {
            BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
            System.out.println("图片宽度为：" + bufferedImage.getWidth());
        }
        File newFile = new File(path);
        //通过CommonsMultipartFile的方法直接写文件（注意这个时候）
        file.transferTo(newFile);
        long endTime = System.currentTimeMillis();
        return "采用file.Transto的运行时间：" + (endTime - startTime) + "ms";
    }
}