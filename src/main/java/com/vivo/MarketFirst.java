package com.vivo;

import com.vivo.dto.MyStock;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.imageio.ImageIO;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MarketFirst {

    private static CloseableHttpClient realHttpClient = HttpClients.createDefault();

    public static void main(String[] args) throws IOException {
        List<MyStock> myStockList = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader("D:\\01\\123.txt"));
        String line = null;
        while (StringUtils.isNotBlank(line = bufferedReader.readLine())) {
            MyStock myStock = new MyStock();
            String[] arr = line.split(",");
            myStock.setCode(arr[0]);
            myStock.setName(arr[1]);
            myStockList.add(myStock);
        }
        Boolean isGray = false;
        try {
            Integer idx = 0;
            for (MyStock i : myStockList) {
                String eastCode = null;
                if (StringUtils.startsWith(i.getCode(), "6")) {
                    eastCode = "1." + i.getCode();
                } else {
                    eastCode = "0." + i.getCode();
                }
                HttpGet httpGet = new HttpGet(String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL", eastCode));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(500).build());
                File file = new File(String.format("D:\\01\\today\\%03d-%s-%s.png", ++idx, i.getCode(), i.getName().replace("*", "星")));
                if (isGray) {
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(EntityUtils.toByteArray(realHttpClient.execute(httpGet).getEntity()));
                    BufferedImage originalImage = ImageIO.read(byteArrayInputStream);
                    BufferedImage grayImage = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
                    ColorConvertOp colorConvertOp = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
                    colorConvertOp.filter(originalImage, grayImage);
                    ImageIO.write(grayImage, "png", file);
                } else {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(EntityUtils.toByteArray(realHttpClient.execute(httpGet).getEntity()));
                    fileOutputStream.close();
                }
            }
            System.out.println(String.format("共计%d个", myStockList.size()));
        } catch (Exception e) {
        }
    }
}
