package com.vivo;

import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

public class JiuCai {

    private static CloseableHttpClient realHttpClient = HttpClients.createDefault();

    private static void queryJiuCai(String name) {
        try {
            HttpPost httpPost = new HttpPost("https://app.jiucaigongshe.com/jystock-app/api/v2/article/search");
            httpPost.addHeader("authority", "app.jiucaigongshe.com");
            httpPost.addHeader("accept", "application/json, text/plain, */*");
            httpPost.addHeader("accept-language", "zh-CN,zh;q=0.9");
            httpPost.addHeader("content-type", "application/json");
            httpPost.addHeader("cookie", "UM_distinctid=180a7163b26618-0a12e4d2b44ca7-6b3e555b-1fa400-180a7163b28204; Hm_lvt_2d6d056d37910563cdaa290ee2981080=1654942341; Hm_lpvt_2d6d056d37910563cdaa290ee2981080=1654958026");
            httpPost.addHeader("origin", "https://www.jiucaigongshe.com");
            httpPost.addHeader("platform", "3");
            httpPost.addHeader("referer", "https://www.jiucaigongshe.com/");
            httpPost.addHeader("sec-ch-ua", "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"102\", \"Google Chrome\";v=\"102\"");
            httpPost.addHeader("sec-ch-ua-mobile", "?0");
            httpPost.addHeader("sec-ch-ua-platform", "\"Windows\"");
            httpPost.addHeader("sec-fetch-dest", "empty");
            httpPost.addHeader("sec-fetch-mode", "cors");
            httpPost.addHeader("sec-fetch-site", "same-site");
            httpPost.addHeader("timestamp", "1654959947157");
            httpPost.addHeader("token", "2710886e059e7054f8bd7a4f637f65d3");
            httpPost.addHeader("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36");
            List<NameValuePair> nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("back_garden", "0"));
            nameValuePairList.add(new BasicNameValuePair("keyword", name));
            nameValuePairList.add(new BasicNameValuePair("order", "1"));
            nameValuePairList.add(new BasicNameValuePair("limit", "15"));
            nameValuePairList.add(new BasicNameValuePair("start", "1"));
            nameValuePairList.add(new BasicNameValuePair("type", "5"));
            UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
            httpPost.setEntity(urlEncodedFormEntity);
            httpPost.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
            CloseableHttpResponse response = realHttpClient.execute(httpPost);
            String result = EntityUtils.toString(response.getEntity());
//            JSONArray jsonArray = JSON.parseObject(result).getJSONObject("answer").getJSONArray("components").getJSONObject(0).getJSONObject("data").getJSONArray("datas");
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String name = "永兴材料";
        queryJiuCai(name);

    }
}
