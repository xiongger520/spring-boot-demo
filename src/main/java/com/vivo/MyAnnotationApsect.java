package com.vivo;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAnnotationApsect {

    @Pointcut(value = "@annotation(com.vivo.MyAnnotation)")
    private void pointCut() {
    }

    //获取注解类属性
    @Around(value = "pointCut() && @annotation(myAnnotation)")
    public Object around(ProceedingJoinPoint point, MyAnnotation myAnnotation) {
        String value = myAnnotation.value();
        System.out.println("value:" + value);
        try {
            return point.proceed(); //执行程序
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return throwable.getMessage();
        }
    }
}
