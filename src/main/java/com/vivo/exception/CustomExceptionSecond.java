package com.vivo.exception;

import lombok.Data;

//自定义异常类
@Data
public class CustomExceptionSecond extends RuntimeException {
    private long code;
    private String msg;

    public CustomExceptionSecond(String msg) {
        super(msg);
        this.msg = msg;
    }

    public CustomExceptionSecond(Long code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}
