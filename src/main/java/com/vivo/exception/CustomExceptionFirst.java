package com.vivo.exception;

import lombok.Data;

//自定义异常类
@Data
public class CustomExceptionFirst extends RuntimeException {
    private long code;
    private String msg;

    public CustomExceptionFirst(String msg) {
        super(msg);
        this.msg = msg;
    }

    public CustomExceptionFirst(Long code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}
