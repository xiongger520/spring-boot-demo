package com.vivo.dao;

import com.vivo.entity.CheckInLog;
import com.vivo.entity.CheckInLogExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Mybatis Generator 2020/06/30
 */
@Repository
public interface CheckInLogMapper {
    /**
     * @mbg.generated 2020-06-30
     */
    long countByExample(CheckInLogExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    int deleteByExample(CheckInLogExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    int deleteByPrimaryKey(Map<String, Object> paramMap);

    /**
     * @mbg.generated 2020-06-30
     */
    int insert(CheckInLog record);

    /**
     * @mbg.generated 2020-06-30
     */
    int insertSelective(CheckInLog record);

    /**
     * @mbg.generated 2020-06-30
     */
    List<CheckInLog> selectByExample(CheckInLogExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    CheckInLog selectByPrimaryKey(Long id);

    /**
     * @mbg.generated 2020-06-30
     */
    int updateByExampleSelective(@Param("record") CheckInLog record, @Param("example") CheckInLogExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    int updateByExample(@Param("record") CheckInLog record, @Param("example") CheckInLogExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    int updateByPrimaryKeySelective(CheckInLog record);

    /**
     * @mbg.generated 2020-06-30
     */
    int updateByPrimaryKey(CheckInLog record);

    int batchInsert(@Param("list") List<CheckInLog> list);

    Map<String, Object> sumByExample(CheckInLogExample example);
}