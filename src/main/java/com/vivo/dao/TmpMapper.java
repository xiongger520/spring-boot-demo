package com.vivo.dao;

import com.vivo.entity.Stock;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TmpMapper {
    //根据产品code获取所有库存
    List<Stock> selectAllStocksByProductCode(String productCode);
}