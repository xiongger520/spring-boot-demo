package com.vivo.dao;

import com.vivo.entity.GroupEntity;

import java.util.List;

public interface AttrMapper {
    public List<GroupEntity> queryGroupsByProductId(Integer productId);
}
