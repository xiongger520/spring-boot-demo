package com.vivo.dao;

import com.vivo.entity.CheckInEntity;
import com.vivo.entity.CheckInExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Mybatis Generator 2020/06/30
 */
@Repository
public interface CheckInMapper {
    /**
     * @mbg.generated 2020-06-30
     */
    long countByExample(CheckInExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    int deleteByExample(CheckInExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    int deleteByPrimaryKey(Map<String, Object> paramMap);

    /**
     * @mbg.generated 2020-06-30
     */
    int insert(CheckInEntity record);

    /**
     * @mbg.generated 2020-06-30
     */
    int insertSelective(CheckInEntity record);

    /**
     * @mbg.generated 2020-06-30
     */
    List<CheckInEntity> selectByExample(CheckInExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    CheckInEntity selectByPrimaryKey(Long id);

    /**
     * @mbg.generated 2020-06-30
     */
    int updateByExampleSelective(@Param("record") CheckInEntity record, @Param("example") CheckInExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    int updateByExample(@Param("record") CheckInEntity record, @Param("example") CheckInExample example);

    /**
     * @mbg.generated 2020-06-30
     */
    int updateByPrimaryKeySelective(CheckInEntity record);

    /**
     * @mbg.generated 2020-06-30
     */
    int updateByPrimaryKey(CheckInEntity record);

    int batchInsert(@Param("list") List<CheckInEntity> list);

    Map<String, Object> sumByExample(CheckInExample example);
}