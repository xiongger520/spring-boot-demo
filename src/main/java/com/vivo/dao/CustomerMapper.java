package com.vivo.dao;

import com.vivo.entity.Customer;
import com.vivo.entity.CustomerExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Mybatis Generator 2020/04/21
 */
@Repository
public interface CustomerMapper {
    /**
     * @mbg.generated 2020-04-21
     */
    long countByExample(CustomerExample example);

    /**
     * @mbg.generated 2020-04-21
     */
    int deleteByExample(CustomerExample example);

    /**
     * @mbg.generated 2020-04-21
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated 2020-04-21
     */
    int insert(Customer record);

    /**
     * @mbg.generated 2020-04-21
     */
    int insertSelective(Customer record);

    /**
     * @mbg.generated 2020-04-21
     */
    List<Customer> selectByExampleWithBLOBs(CustomerExample example);

    /**
     * @mbg.generated 2020-04-21
     */
    List<Customer> selectByExample(CustomerExample example);

    /**
     * @mbg.generated 2020-04-21
     */
    Customer selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated 2020-04-21
     */
    int updateByExampleSelective(@Param("record") Customer record, @Param("example") CustomerExample example);

    /**
     * @mbg.generated 2020-04-21
     */
    int updateByExampleWithBLOBs(@Param("record") Customer record, @Param("example") CustomerExample example);

    /**
     * @mbg.generated 2020-04-21
     */
    int updateByExample(@Param("record") Customer record, @Param("example") CustomerExample example);

    /**
     * @mbg.generated 2020-04-21
     */
    int updateByPrimaryKeySelective(Customer record);

    /**
     * @mbg.generated 2020-04-21
     */
    int updateByPrimaryKeyWithBLOBs(Customer record);

    /**
     * @mbg.generated 2020-04-21
     */
    int updateByPrimaryKey(Customer record);

    int batchInsert(@Param("list") List<Customer> list);

    java.util.Map<String, Object> sumByExample(CustomerExample example);

    int swapDisplayOrder(@Param("tableName") String tableName, @Param("id1") Integer id1, @Param("id2") Integer id2);

    Long selectId(Map<String, Object> map);
}