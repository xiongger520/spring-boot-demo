package com.vivo.dao;

import com.vivo.entity.StockEntity;
import com.vivo.entity.StockExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Mybatis Generator 2020/06/02
 */
@Repository
public interface StockMapper {
    /**
     *
     * @mbg.generated 2020-06-02
     */
    long countByExample(StockExample example);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int deleteByExample(StockExample example);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int deleteByPrimaryKey(Map<String, Object> paramMap);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int insert(StockEntity record);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int insertSelective(StockEntity record);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    List<StockEntity> selectByExampleWithBLOBs(StockExample example);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    List<StockEntity> selectByExample(StockExample example);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    StockEntity selectByPrimaryKey(Long id);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int updateByExampleSelective(@Param("record") StockEntity record, @Param("example") StockExample example);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int updateByExampleWithBLOBs(@Param("record") StockEntity record, @Param("example") StockExample example);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int updateByExample(@Param("record") StockEntity record, @Param("example") StockExample example);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int updateByPrimaryKeySelective(StockEntity record);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int updateByPrimaryKeyWithBLOBs(StockEntity record);

    /**
     *
     * @mbg.generated 2020-06-02
     */
    int updateByPrimaryKey(StockEntity record);

    int batchInsert(@Param("list") List<StockEntity> list);

    Map<String, Object> sumByExample(StockExample example);
}