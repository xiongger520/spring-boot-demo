package com.vivo.dao;

import com.vivo.entity.Category;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryMapper {
    public List<Category> getCategory(@Param("categoryId") Integer categoryId, @Param("isDeleted") Integer isDeleted);
}

