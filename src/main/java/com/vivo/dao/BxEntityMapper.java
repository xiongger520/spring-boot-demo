package com.vivo.dao;

import com.vivo.entity.BxEntity;
import com.vivo.entity.BxEntityExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Mybatis Generator 2020/09/14
 */
@Repository
public interface BxEntityMapper {
    /**
     * @mbg.generated 2020-09-14
     */
    long countByExample(BxEntityExample example);

    /**
     * @mbg.generated 2020-09-14
     */
    int deleteByExample(BxEntityExample example);

    /**
     * @mbg.generated 2020-09-14
     */
    int deleteByPrimaryKey(Long id);

    /**
     * @mbg.generated 2020-09-14
     */
    int insert(BxEntity record);

    /**
     * @mbg.generated 2020-09-14
     */
    int insertSelective(BxEntity record);

    /**
     * @mbg.generated 2020-09-14
     */
    List<BxEntity> selectByExample(BxEntityExample example);

    /**
     * @mbg.generated 2020-09-14
     */
    BxEntity selectByPrimaryKey(Long id);

    /**
     * @mbg.generated 2020-09-14
     */
    int updateByExampleSelective(@Param("record") BxEntity record, @Param("example") BxEntityExample example);

    /**
     * @mbg.generated 2020-09-14
     */
    int updateByExample(@Param("record") BxEntity record, @Param("example") BxEntityExample example);

    /**
     * @mbg.generated 2020-09-14
     */
    int updateByPrimaryKeySelective(BxEntity record);

    /**
     * @mbg.generated 2020-09-14
     */
    int updateByPrimaryKey(BxEntity record);

    int batchInsert(@Param("list") List<BxEntity> list);

    java.util.Map<String, Object> sumByExample(BxEntityExample example);
}