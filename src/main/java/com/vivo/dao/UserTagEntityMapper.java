package com.vivo.dao;

import com.vivo.entity.UserTagEntity;
import com.vivo.entity.UserTagEntityExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Mybatis Generator 2022/01/29
 */
@Repository
public interface UserTagEntityMapper {
    /**
     * @mbg.generated 2022-01-29
     */
    long countByExample(UserTagEntityExample example);

    /**
     * @mbg.generated 2022-01-29
     */
    int deleteByExample(UserTagEntityExample example);

    /**
     * @mbg.generated 2022-01-29
     */
    int deleteByPrimaryKey(Map<String, Object> paramMap);

    /**
     * @mbg.generated 2022-01-29
     */
    int insert(UserTagEntity record);

    /**
     * @mbg.generated 2022-01-29
     */
    int insertSelective(UserTagEntity record);

    /**
     * @mbg.generated 2022-01-29
     */
    List<UserTagEntity> selectByExampleWithBLOBs(UserTagEntityExample example);

    /**
     * @mbg.generated 2022-01-29
     */
    List<UserTagEntity> selectByExample(UserTagEntityExample example);

    /**
     * @mbg.generated 2022-01-29
     */
    UserTagEntity selectByPrimaryKey(Long id);

    /**
     * @mbg.generated 2022-01-29
     */
    int updateByExampleSelective(@Param("record") UserTagEntity record, @Param("example") UserTagEntityExample example);

    /**
     * @mbg.generated 2022-01-29
     */
    int updateByExampleWithBLOBs(@Param("record") UserTagEntity record, @Param("example") UserTagEntityExample example);

    /**
     * @mbg.generated 2022-01-29
     */
    int updateByExample(@Param("record") UserTagEntity record, @Param("example") UserTagEntityExample example);

    /**
     * @mbg.generated 2022-01-29
     */
    int updateByPrimaryKeySelective(UserTagEntity record);

    /**
     * @mbg.generated 2022-01-29
     */
    int updateByPrimaryKeyWithBLOBs(UserTagEntity record);

    /**
     * @mbg.generated 2022-01-29
     */
    int updateByPrimaryKey(UserTagEntity record);

    int batchInsert(@Param("list") List<UserTagEntity> list);

    Map<String, Object> sumByExample(UserTagEntityExample example);
}