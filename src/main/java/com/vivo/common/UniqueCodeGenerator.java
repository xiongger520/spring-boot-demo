package com.vivo.common;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.Random;

public class UniqueCodeGenerator {
    //中文字符串得到全拼字符串（大写）
    public static String getPingYin(String src) {
        char[] t1;
        t1 = src.toCharArray();
        String[] t2;
        HanyuPinyinOutputFormat t3 = new HanyuPinyinOutputFormat();
        t3.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        t3.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        t3.setVCharType(HanyuPinyinVCharType.WITH_V);
        String t4 = "";
        int t0 = t1.length;
        try {
            for (int i = 0; i < t0; i++) {
                // 判断是否为汉字字符
                if (java.lang.Character.toString(t1[i]).matches("[\\u4E00-\\u9FA5]+")) {
                    t2 = PinyinHelper.toHanyuPinyinStringArray(t1[i], t3);
                    t4 += t2[0];
                } else {
                    t4 += java.lang.Character.toString(t1[i]);
                }
            }
            return t4.toUpperCase();
        } catch (BadHanyuPinyinOutputFormatCombination e1) {
            e1.printStackTrace();
        }
        return t4.toUpperCase();
    }

    //中文字符串得到拼音首字母字符串（大写）
    public static String getPinYinHeadChar(String str) {
        String convert = "";
        for (int j = 0; j < str.length(); j++) {
            char word = str.charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert += pinyinArray[0].charAt(0);
            } else {
                convert += word;
            }
        }
        return convert.toUpperCase();
    }

    //根据时间戳和随机数获取唯一key
    public static synchronized Integer getUniqueMoKey() {
        Random random = new Random();
        Integer number = random.nextInt(90000000) + 10000000;
        return (int) (System.currentTimeMillis() + number) % 100000000;
    }

    //获取唯一模块编号
    public static synchronized Integer getUniqueModuleCode() {
        return getUniqueMoKey();
    }

    //获取保险公司编号
    public static synchronized String getSupplierCode(String supplierName) {
        return getPinYinHeadChar(supplierName);
    }

    //获取唯一产品编号
    public static synchronized String getProductCode(String supplierName, String productName) {
        return getPinYinHeadChar(supplierName) + "-" + getPinYinHeadChar(productName) + "-" + getUniqueMoKey() % 10000;
    }

    //获取唯一方案编号
    public static synchronized String getStockCode(String supplierName, String productName, String stockName) {
        return getPinYinHeadChar(supplierName) + "-" + getPinYinHeadChar(productName) + "-" + getPinYinHeadChar(stockName) + "-" + getUniqueMoKey() % 10000;
    }

    //获取唯一条款编号
    public static synchronized String getClauseCode(String supplierName, String clauseName) {
        return getPinYinHeadChar(supplierName) + "-" + getPinYinHeadChar(clauseName) + "-" + getUniqueMoKey() % 10000;
    }

    //获取唯一条款编号
    public static synchronized String getCategoryCode(String categoryName) {
        return getPinYinHeadChar(categoryName) + "-" + getUniqueMoKey() % 10000;
    }

    public static void main(String[] args) {
        System.out.println(getProductCode("众安保险", "资金安全险"));
        System.out.println(getProductCode("众安保险", "资金安全险"));
        System.out.println(getProductCode("众安保险", "资金安全险"));
        System.out.println(getProductCode("众安保险", "资金安全险"));
        System.out.println(getStockCode("众安保险", "资金安全险", "基础版"));
        System.out.println(getStockCode("众安保险", "资金安全险", "基础版"));
        System.out.println(getStockCode("众安保险", "资金安全险", "基础版"));
        System.out.println(getStockCode("众安保险", "资金安全险", "基础版"));
        System.out.println(getClauseCode("众安保险", "保险条款"));
        System.out.println(getClauseCode("众安保险", "保险条款"));
        System.out.println(getClauseCode("众安保险", "保险条款"));
        System.out.println(getClauseCode("众安保险", "保险条款"));
        System.out.println(getCategoryCode("财产保险"));
        System.out.println(getCategoryCode("财产保险"));
        System.out.println(getCategoryCode("财产保险"));
        System.out.println(getCategoryCode("财产保险"));
    }
}
