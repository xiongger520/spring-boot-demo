package com.vivo.common;


public enum Color {

    RED(1, "红色"), BLUE(2, "蓝色");
    public Integer code;//索引值
    public String desc;//中文名

    Color(Integer code, String name) {
        this.code = code;
        this.desc = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return desc;
    }
}
