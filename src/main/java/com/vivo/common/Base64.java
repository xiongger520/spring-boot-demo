package com.vivo.common;

import org.springframework.util.Base64Utils;

public class Base64 {
    //二进制文件转base64编码的字符串
    public static String Encode(byte[] srcBytes) {
        return Base64Utils.encodeToString(srcBytes);

    }

    //base64编码的字符串转二进制文件
    public static byte[] Decode(String srcString) {
        return Base64Utils.decodeFromString(srcString);
    }
}