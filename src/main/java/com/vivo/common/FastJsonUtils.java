package com.vivo.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.JSONLibDataFormatSerializer;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.List;

/**
 * fastjson工具类
 *
 * @version:1.0.0
 */
public class FastJsonUtils {

    private static final SerializeConfig config;

    static {
        config = new SerializeConfig();
        config.put(java.util.Date.class, new JSONLibDataFormatSerializer()); // 使用和json-lib兼容的日月输出格式
        config.put(java.sql.Date.class, new JSONLibDataFormatSerializer()); // 使用和json-lib兼容的日月输出格式
    }

    private static final SerializerFeature[] features = {
            // 防止循环引用
            SerializerFeature.DisableCircularReferenceDetect,
            // null字符串转""字符串返回，尽量不要用
            // SerializerFeature.WriteNullStringAsEmpty,
            // null列表转[]返回，尽量不要用
            //SerializerFeature.WriteNullListAsEmpty,
            //输出value为null的map键值对
            SerializerFeature.WriteMapNullValue,
            //格式化时间Date
            SerializerFeature.WriteDateUseDateFormat
    };

    /**
     * Java对象转为JSON字符串
     *
     * @param object
     * @return JSON字符串
     */
    public static String ObjectToJSONString(Object object) {
        return JSON.toJSONString(object, config, features);
    }

    /**
     * JSON字符串转为Java对象
     *
     * @param string
     * @param clazz
     * @return Java对象
     */
    public static <T> T JSONStringToObject(String string, Class<T> clazz) {
        return JSON.parseObject(string, clazz);
    }

    /**
     * JSON字符串转为Java对象数组
     *
     * @param string
     * @param clazz
     * @return Java对象数组
     */
    public static <T> Object[] JSONStringToObjectsArray(String string, Class<T> clazz) {
        return JSON.parseArray(string, clazz).toArray();
    }

    /**
     * JSON字符串转为Java对象列表
     *
     * @param string
     * @param clazz
     * @return Java对象列表
     */
    public static <T> List<T> JSONStringToObjectsList(String string, Class<T> clazz) {
        return JSON.parseArray(string, clazz);
    }
}