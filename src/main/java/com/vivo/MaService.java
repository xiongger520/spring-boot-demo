package com.vivo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

//均线服务
public class MaService {

    private CloseableHttpClient httpClient = HttpClients.createDefault();
    private Map<String, HttpGet> map = new HashMap<>();
    private Map<String, double[]> cache = new HashMap<>();

    //计算日均线
    public Map calMa(String eastCode) {
        Map<String, Double> result = new HashMap<>();
        try {
            HttpGet httpGet = map.get(eastCode);
            if (null == httpGet) {
                httpGet = new HttpGet(String.format("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f53&klt=101&fqt=1&secid=%s&beg=0&end=20500000", eastCode));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(500).build());
                map.put(eastCode, httpGet);
            }
            JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(httpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("klines");
            Integer size = jsonArray.size();
            //实时价格
            Double realPrice = Double.valueOf(jsonArray.get(size - 1).toString());
            //尝试从缓存中读取已累计值
            double[] array = cache.get(eastCode);
            //分别存放前4、9、19、29、59、119日的累计值
            if (null == array) {
                array = new double[6];
                double sum = 0.0;
                for (Integer idx = size - 2, count = 0; idx >= 0 && count <= 119; idx--) {
                    sum = sum + Double.valueOf(jsonArray.get(idx).toString());
                    count++;
                    switch (count) {
                        case 4: {
                            array[0] = sum;
                            break;
                        }
                        case 9: {
                            array[1] = sum;
                            break;
                        }
                        case 19: {
                            array[2] = sum;
                            break;
                        }
                        case 29: {
                            array[3] = sum;
                            break;
                        }
                        case 59: {
                            array[4] = sum;
                            break;
                        }
                        case 119: {
                            array[5] = sum;
                            break;
                        }
                    }
                }
                cache.put(eastCode, array);
            }
            if (array[0] != 0.0) {
                result.put("MA5", (array[0] + realPrice) / 5);
            }
            if (array[1] != 0.0) {
                result.put("MA10", (array[1] + realPrice) / 10);
            }
            if (array[2] != 0.0) {
                result.put("MA20", (array[2] + realPrice) / 20);
            }
            if (array[3] != 0.0) {
                result.put("MA30", (array[3] + realPrice) / 30);
            }
            if (array[4] != 0.0) {
                result.put("MA60", (array[4] + realPrice) / 60);
            }
            if (array[5] != 0.0) {
                result.put("MA120", (array[5] + realPrice) / 120);
            }
        } catch (Exception e) {
            System.out.println(eastCode + "日均线计算失败!");
            e.printStackTrace();
        }
        return result;
    }
}
