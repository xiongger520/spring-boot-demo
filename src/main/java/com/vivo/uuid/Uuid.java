package com.vivo.uuid;

import com.vivo.dto.MaDto;
import org.apache.dubbo.common.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Uuid {
    public static void main(String[] args) {
        List<MaDto> maDtoList = new ArrayList<>();
        maDtoList.add(new MaDto("1", 1.0));
        maDtoList.add(new MaDto("2", 2.0));
        maDtoList.add(new MaDto("3", 3.0));
        List<MaDto> ml = maDtoList.stream().filter(item -> item.getMaName().equals("2")).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(ml)) {
            System.out.println(maDtoList.indexOf(ml.get(0)));
        }
        //VolService volService = new VolService();
        //while (true) {
        //    long t1 = System.currentTimeMillis();
        //    System.out.println(volService.calPreviousVol(239, "sz159870"));
        //    System.out.println("耗时:" + (System.currentTimeMillis() - t1));
        //}

    }
}
