package com.vivo.uuid;


/**
 * 参考文档地址：https://blog.csdn.net/qq_42878176/article/details/128201390
 */

/**
 * 各个状态行为的抽象，可以是接口，也可是抽象类
 */
abstract class Status {

    void next(WorkFlow workFlow) {

    }

    void back(WorkFlow workFlow) {

    }
}

/**
 * 开始态
 */
class StartStatus extends Status {

    @Override
    public void next(WorkFlow workFlow) {
        System.out.println("保存开始环节的信息....");
        workFlow.changeStatus(new Task1Status());
    }

    @Override
    public void back(WorkFlow workFlow) {
        System.out.println("已经是第一步了....");
    }
}

/**
 * 任务1态
 */
class Task1Status extends Status {

    @Override
    public void next(WorkFlow workFlow) {
        System.out.println("保存Task1环节的信息....");
        workFlow.changeStatus(new EndStatus());
    }

    @Override
    public void back(WorkFlow workFlow) {
        System.out.println("重置,返回开始环节....");
        workFlow.changeStatus(new StartStatus());
    }
}

/**
 * 结束态
 */
class EndStatus extends Status {

    @Override
    public void next(WorkFlow workFlow) {
        System.out.println("结单....");
    }

    @Override
    public void back(WorkFlow workFlow) {
        System.out.println("重置，返回Task1环节....");
        workFlow.changeStatus(new Task1Status());
    }
}


/**
 * 工作流，可以理解为上下文，维护状态+执行具体步骤
 */
public class WorkFlow {

    private Status status;

    public WorkFlow(Status status) {
        this.status = status;
    }

    public void changeStatus(Status status) {
        System.out.println(this.status.getClass().getName() + "结束，下一步环节:" + status.getClass().getName());
        this.status = status;
    }

    public void pass() {
        System.out.println("下一步");
        this.status.next(this);
    }

    public void rejext() {
        System.out.println("驳回");
        this.status.back(this);
    }

    // 运行
    public static void main(String[] args) {
        WorkFlow workFlow = new WorkFlow(new StartStatus());
        workFlow.pass();
        workFlow.rejext();
        workFlow.pass();
        workFlow.pass();
        workFlow.pass();
    }
}
