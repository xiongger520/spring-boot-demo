package com.vivo.uuid;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class FileDispose {
    public static void main(String[] args) throws IOException {
        //使用缓冲流的前提是要有一个基本流
        //字符流
//        FileWriter fileWriter = new FileWriter(new File("D:\\01\\temp.txt"));
//        fileWriter.write("123");
//        fileWriter.close();
//        FileReader fileReader = new FileReader(new File("D:\\01\\temp.txt"));
//        //一次只能读取一个字符
//        System.out.println((char) fileReader.read());
//        fileReader.close();
//        BufferedReader bufferedReader = new BufferedReader(new FileReader("D:\\01\\temp.txt"));
////        一次可以读取一行
//        System.out.println(bufferedReader.readLine());
//        bufferedReader.close();
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("D:\\01\\temp.txt"));
//        bufferedWriter.write("123");
//        bufferedWriter.close();
        //字节流
//        FileInputStream fileInputStream = new FileInputStream("D:\\01\\temp.txt");
//        //一次只能读取一个字节
//        System.out.println((char) fileInputStream.read());
//        fileInputStream.close();
//        FileOutputStream fileOutputStream = new FileOutputStream("D:\\01\\temp.txt");
//        //一次只能写入一个字节，写入'5'
//        fileOutputStream.write(49 + 4);
//        fileOutputStream.close();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream("D:\\01\\temp.txt"));
        byte[] bytes = new byte[10];
        bufferedInputStream.read(bytes);
        System.out.println((char) bytes[0]);
    }
}
