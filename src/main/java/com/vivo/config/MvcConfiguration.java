//package com.vivo.config;
//
//import java.nio.charset.Charset;
//import java.util.ArrayList;
//import java.util.List;
//
//import com.alibaba.fastjson.serializer.SerializerFeature;
//import com.alibaba.fastjson.support.config.FastJsonConfig;
//import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Configuration
//@EnableWebMvc//自定义mvc配置
//public class MvcConfiguration implements WebMvcConfigurer {
//    //使用FastJSON作为消息转换器
//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        FastJsonHttpMessageConverter fastJsonHttpMessageConverter = new FastJsonHttpMessageConverter();
//        List<MediaType> supportedMediaTypes = new ArrayList<>();
//        //目前仅支持json类型
//        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
//        fastJsonHttpMessageConverter.setSupportedMediaTypes(supportedMediaTypes);
//        //字符集
//        fastJsonHttpMessageConverter.setDefaultCharset(Charset.forName("utf-8"));
//        FastJsonConfig fastJsonConfig = new FastJsonConfig();
//        fastJsonConfig.setSerializerFeatures(
//                // 防止循环引用
//                SerializerFeature.DisableCircularReferenceDetect,
//                // null字符串转""字符串返回，尽量不要用
//                // SerializerFeature.WriteNullStringAsEmpty,
//                // null列表转[]返回，尽量不要用
//                //SerializerFeature.WriteNullListAsEmpty,
//                //输出value为null的map键值对
//                SerializerFeature.WriteMapNullValue,
//                //格式化时间Date
//                SerializerFeature.WriteDateUseDateFormat
//        );
//        fastJsonHttpMessageConverter.setFastJsonConfig(fastJsonConfig);
//        converters.add(fastJsonHttpMessageConverter);
//    }
//
//    //配置静态资源处理
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/", "classpath:/template/");
//    }
//}