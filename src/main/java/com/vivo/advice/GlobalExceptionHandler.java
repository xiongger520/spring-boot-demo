package com.vivo.advice;

import com.vivo.exception.CustomExceptionFirst;
import com.vivo.exception.CustomExceptionSecond;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import sun.rmi.runtime.Log;

import java.util.HashMap;
import java.util.Map;

//Controller层全局异常捕捉处理，相当于Controller+切面通知advice+ResponseBody
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(CustomExceptionFirst.class)
    public Map errorHandlerFirst(CustomExceptionFirst e) {
        System.out.println();
        Map map = new HashMap();
        map.put("code", e.getCode());
        map.put("msg", e.getMsg());
        map.put("data", null);
        log.info("发生CustomExceptionFirst异常------->");
        return map;
    }

    @ExceptionHandler(CustomExceptionSecond.class)
    public Map errorHandlerSecond(CustomExceptionSecond e) {
        Map map = new HashMap();
        map.put("code", e.getCode());
        map.put("msg", e.getMsg());
        map.put("data", null);
        log.info("发生CustomExceptionSecond异常------->");
        return map;
    }

    @ExceptionHandler(Exception.class)
    public Map globalErrorHandler(Exception e) {
        Map map = new HashMap();
        map.put("code", "400");
        map.put("msg", e.getMessage());
        map.put("data", null);
        log.info("发生其他异常------->");
        return map;
    }
}