//package com.vivo.advice;
//
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//@Slf4j
//public class TimeSpendAspect {
//
//    //编写表达式一定要小心谨慎，防止写错！！！
//    @Around("execution(public * com.vivo.controller..*(..))")
//    public Object around(ProceedingJoinPoint joinPoint) {
//        String methodName = joinPoint.getSignature().getName();
//        try {
//            long startTimeMillis = System.currentTimeMillis();
//            Object result = joinPoint.proceed();
//            long execTimeMillis = System.currentTimeMillis() - startTimeMillis;
//            log.info(" 接口名: /" + methodName + " , 接口执行时间:" + execTimeMillis + "ms");
//            return result;
//        } catch (Throwable t) {
//            log.warn("接口: " + methodName + "执行错误,详细信息:" + t.getMessage());
//            return null;
//        }
//    }
//}