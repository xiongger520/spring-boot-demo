package com.vivo.advice;

import com.vivo.exception.CustomExceptionFirst;
import com.vivo.exception.CustomExceptionSecond;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExceptionController {
    @RequestMapping("/CustomExceptionFirst")
    public String CustomExceptionFirst() throws CustomExceptionFirst {
        throw new CustomExceptionFirst(400L, "发生了自定义异常1");
    }

    @RequestMapping("/CustomExceptionSecond")
    public String CustomExceptionSecond() throws CustomExceptionSecond {
        throw new CustomExceptionSecond(500L, "发生了自定义异常2");
    }
}
