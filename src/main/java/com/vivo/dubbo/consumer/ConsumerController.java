package com.vivo.dubbo.consumer;

import com.vivo.dubbo.provider.HelloService;
import com.vivo.entity.BxEntity;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumerController {
    @Reference
    private HelloService helloService;

    @RequestMapping("/sayHello")
    public String sayHello(BxEntity bxEntity) {
        return helloService.sayHello(bxEntity);
    }
}