package com.vivo.dubbo.provider.impl;

import com.vivo.exception.CustomExceptionFirst;
import com.vivo.exception.CustomExceptionSecond;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

@Activate
@Slf4j
public class ExceptionFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        Result result = null;
        try {
            log.info("dubbo过滤器：{}接口调用前------------->", invocation.getMethodName());
            result = invoker.invoke(invocation);
            log.info("dubbo过滤器：{}接口调用后<-------------", invocation.getMethodName());
            if (result.hasException()) {
                throw result.getException();
            }
        } catch (Throwable e) {
            if (e instanceof CustomExceptionFirst) {
                log.info("处理异常1");
            } else if (e instanceof CustomExceptionSecond) {
                log.info("处理异常2");
            } else {
                log.info("未知异常");
            }
        }
        return result;
    }
}