package com.vivo.dubbo.provider.impl;

import com.vivo.dubbo.provider.HelloService;
import com.vivo.entity.BxEntity;
import org.apache.dubbo.config.annotation.Service;

import javax.validation.Valid;

@Service(validation = "true")
public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello(@Valid BxEntity bxEntity) {
        System.out.println(bxEntity);
        return "Hello World!";
    }
}