package com.vivo.dubbo.provider;

import com.vivo.entity.BxEntity;

public interface HelloService {
    public String sayHello(BxEntity bxEntity);
}
