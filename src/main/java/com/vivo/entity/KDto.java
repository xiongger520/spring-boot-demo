package com.vivo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class KDto implements Serializable {
    private static final long serialVersionUID = 6673293517719474089L;
    private LocalDateTime day;
    private String open;
    private String high;
    private String low;
    private String close;
    private String volume;

    public KDto() {
    }

    public KDto(LocalDateTime day, String open, String high, String low, String close, String volume) {
        this.day = day;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public LocalDateTime getDay() {
        return day;
    }

    public void setDay(LocalDateTime day) {
        this.day = day;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }
}
