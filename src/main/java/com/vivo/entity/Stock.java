package com.vivo.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Stock implements Serializable {
    /**
     * 库存样式，用于价格和数量匹配，通过销售规格选项的business_code依次拼接而成，用英文逗号,分割。例如，用户的选择为：60周岁、男、30万保额、缴费月限10年，则为：A60,1,30W,Y10
     */
    @JSONField(ordinal = 1)
    private String stockPattern;

    /**
     * 首月价
     */
    @JSONField(ordinal = 2)
    private BigDecimal firstPrice;

    /**
     * 销售广告，例如：首月￥3/月，次月￥25/月
     */
    @JSONField(ordinal = 3)
    private String saleAds;
}