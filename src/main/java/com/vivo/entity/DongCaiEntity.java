package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DongCaiEntity implements Serializable {

    private String HYCode;

    private String ORIGINALCODE;

    private String HYName;

    private Date HdDate;

    private Double ShareSZ_ZC;

    private Double ShareHold_ZC_Chg_BK;

    private Double ShareHold_ZC_Chg_GZ;

    private Double ShareSZ_GZ;

    private String DateType;
}