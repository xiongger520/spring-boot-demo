package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductExt {
    //保额
    private String quota;
    //费率描述
    private String premiumDesc;
    //初试广告信息
    private String initialAds;
}
