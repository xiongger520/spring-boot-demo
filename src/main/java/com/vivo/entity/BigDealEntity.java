package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Time;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BigDealEntity implements Serializable {
    private String symbol;
    private String name;
    private Time ticktime;
    private String price;
    private String volume;
    private String prev_price;
    private String kind;
}
