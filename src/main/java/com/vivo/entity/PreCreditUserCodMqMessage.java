package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PreCreditUserCodMqMessage {
    /**
     * openId
     */
    private String openId;

    /**
     * memberNo
     */
    private String memberNo;

    /**
     * 借据的结清时间，目前只有结清挽留场景才会使用这个字段
     */
    private String dueBillNo;

    /**
     * 业务发生时间，前端推送埋点数据时间
     * 对于101-结清挽留场景，这个代表借据的结清时间（精确到日期）
     */
    private Date bisnessTime;

    /**
     * 详见CodSceneEnum
     */
    private String scene;
    /**
     * imei
     */
    private String imei;

}
