package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockExt {
    //续保体检保费
    private BigDecimal renewPhysicalCheckPremium;
    //续保质子重离子治疗保费
    private BigDecimal renewPHITPremium;
    //续保特药保费
    private BigDecimal renewSpecMedicinePremium;
    //首投年保费
    private BigDecimal firstYearPremium;
}
