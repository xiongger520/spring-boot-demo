package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Mybatis Generator 2020/06/30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CheckInLog implements Serializable {
    /**
     * tb_check_in_log
     */
    private static final long serialVersionUID = 1L;
    /**
     * 自增主键
     */
    private Long id;
    /**
     * vivo账号id
     */
    private String openId;
    /**
     * 钱包账号id
     */
    private String memberNo;
    /**
     * 业务类型：1-自营贷款，2-引流业务，3-保险业务
     */
    private Integer bizType;
    /**
     * 业务编号
     */
    private String bizCode;
    /**
     * 业务流水号
     */
    private String serialNo;
    /**
     * 签到时间
     */
    private Date checkInTime;
    /**
     * 版本（乐观锁）
     */
    private Long version;
    /**
     * 是否删除：0-不删除，1-删除
     */
    private Integer isDeleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
}