package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HawkingTestConfig implements Serializable {
    //实验名称
    private String testName;
    //实验编号
    private String testCode;
    //参数名
    private String paramName;
    //字段名列表
    private List<String> fieldNameList;
    //状态
    private Integer status;
}