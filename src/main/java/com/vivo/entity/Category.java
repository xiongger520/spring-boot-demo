package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {

    private String categoryId;
    private String categoryParentId;
    private String categoryName;
    private Integer isDeleted;
    private List<Category> subList;

}
