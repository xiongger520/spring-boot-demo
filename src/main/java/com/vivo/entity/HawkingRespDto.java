package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HawkingRespDto implements Serializable {
    private static final long serialVersionUID = -9057199312564968675L;
    //实验编号
    private String testCode;
    //实验id，霍金系统自动生成
    private Long testId;
    //实验方案id，霍金系统自动生成
    private Long testPlanId;
    //响应参数列表
    private List<HawkingParam> paramList;
}