package com.vivo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Mybatis Generator 2022/01/29
 */
@Data
@NoArgsConstructor
public class UserTagEntity implements Serializable {
    /**
     * fw_user_tag
     */
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private Long id;
    /**
     * openId
     */
    private String openId;
    /**
     * 钱包会员号
     */
    private String memberNo;
    /**
     * 一级标签编号
     */
    private Integer firstTagCode;
    /**
     * 一级标签描述
     */
    private String firstTagDesc;
    /**
     * 二级标签编号
     */
    private Integer secondTagCode;
    /**
     * 二级标签描述
     */
    private String secondTagDesc;
    /**
     * 提交人工号
     */
    private String submitterNo;
    /**
     * 提交人姓名
     */
    private String submitter;
    /**
     * 版本号
     */
    private Long version;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 是否删除，0-未删除，1-已删除
     */
    private Integer isDeleted;
    /**
     * 原因
     */
    private String reason;
}