package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class KeyEntity {

    private Integer id;

    private String keyName;

    private String keyDesc;

    private List<ValueEntity> valueEntityList;
}
