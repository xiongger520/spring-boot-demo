package com.vivo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by Mybatis Generator 2020/04/21
 */
@Data
@NoArgsConstructor
public class Customer implements Serializable {
    /**
     * t_customer
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    private Integer id;
    /**
     *
     */
    private String username;
    /**
     *
     */
    private String jobs;
    /**
     *
     */
    private String phone;
    /**
     *
     */
    private String cusDesc;
}