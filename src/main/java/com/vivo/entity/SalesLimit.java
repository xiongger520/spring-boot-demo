package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalesLimit implements Serializable {
    private static final long serialVersionUID = 5781233184409559972L;
    //年龄段，必须类似"A0-A120"格式
    private String ageRange;
    //产品剩余数量，取值100表示最多可卖100件，0表示已经售罄，-1表示可以不限量售卖
    private Integer remainAmount;
}