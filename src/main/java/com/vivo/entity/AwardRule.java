package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AwardRule implements Serializable {
    //最小年龄
    private Integer minAge;
    //最大年龄
    private String maxAge;
    //规则
    private String rule;
}