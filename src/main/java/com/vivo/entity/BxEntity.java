package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Mybatis Generator 2020/09/14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BxEntity implements Serializable {
    /**
     * etf_bx
     */
    private static final long serialVersionUID = 1L;
    /**
     * 自增主键
     */
    private Long id;
    /**
     * 行业编号
     */
    @NotBlank(message = "行业编号不能为空")
    private String hyCode;
    /**
     * 行业名称
     */
    @Size(max = 3, message = "行业名称不能超过")
    private String hyName;
    /**
     * 数据时间
     */
    private Date dataDate;
    /**
     * 北向资金增持数额，单位：亿元
     */
    private Double bxAddQuota;
    /**
     * 北向资金总持数额，单位：亿元
     */
    private Double bxTotalQuota;
    /**
     * 创建时间
     */
    private Date createTime;
}