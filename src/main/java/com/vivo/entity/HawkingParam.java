package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HawkingParam implements Serializable {
    private static final long serialVersionUID = -145343297836975925L;
    //参数名
    private String paramName;
    //参数值
    private String paramValue;

}