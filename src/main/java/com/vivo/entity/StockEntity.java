package com.vivo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Mybatis Generator 2020/06/02
 */
@Data
@NoArgsConstructor
public class StockEntity implements Serializable {
    /**
     * fp_stock
     */
    private static final long serialVersionUID = 1L;
    /**
     * 自增id
     */
    private Long id;
    /**
     * 产品code
     */
    private String productCode;
    /**
     * 库存名称
     */
    private String stockName;
    /**
     * 库存副名称
     */
    private String stockSubname;
    /**
     * 业务编号，传给外部业务方系统
     */
    private String businessCode;
    /**
     * 库存样式，用于价格和数量匹配，通过销售规格选项的business_code依次拼接而成，用英文逗号,分割。例如，用户的选择为：60周岁、男、30万保额、缴费月限10年，则为：A60,1,30W,Y10
     */
    private String stockPattern;
    /**
     * 首月价
     */
    private BigDecimal firstPrice;
    /**
     * 原始价
     */
    private BigDecimal originalPrice;
    /**
     * 销售广告，例如：首月￥3/月，次月￥25/月
     */
    private String saleAds;
    /**
     * 续保价格
     */
    private BigDecimal renewPrice;
    /**
     * 附加价格
     */
    private BigDecimal extraPrice;
    /**
     * 库存数量
     */
    private Integer stockAmount;
    /**
     * 库存提示，一般是错误提示，例如：该库存已经售罄
     */
    private String stockTip;
    /**
     * 库存额度，例如：保障额度。保障额度为2万元，则存储为20000.00；保障额度为1-2万元，则存储为10000.00-20000.00，单位默认为（人民币）元
     */
    private String stockQuota;
    /**
     * 是否有效：0-无效，1-有效
     */
    private Integer valid;
    /**
     * 生效时间，大部分产品/方案均为次日0时起生效，即T+1日生效，该值一般取D1，用户自定义生效时间的产品/方案不用设置该字段
     */
    private String effectiveTime;
    /**
     * 持续月间，约定规则：1时、1日、1月、1年、至60岁、终身，约定存储格式为：H1、D1、M1、Y1、A60、L，用户自定义持续时间的产品/方案不用设置该字段
     */
    private String duration;
    /**
     * 保险专用字段，犹豫月，即收到电子保单后多少天内可全额退保，互联网渠道中该值一般取D0、D10、D15、D20、D30等，D0则表示不可退保
     */
    private String hesitationPeriod;
    /**
     * 保险专用字段，等待月，即电子保单生效后多少天内保险公司不承担赔偿责任，常见于重疾险、医疗险，等待月一般为D90、D180等
     */
    private String waitingPeriod;
    /**
     * 保险专用字段，宽限月，即保险公司对投保人未按时缴纳续月保费所给予的宽限时间，一般为60天，即D60
     */
    private String gracePeriod;
    /**
     * 佣金比例，两位小数
     */
    private BigDecimal commissionRatio;
    /**
     * 技术服务费比例，两位小数
     */
    private BigDecimal techServiceRatio;
    /**
     * 扩展字段
     */
    private String ext;
    /**
     * 版本（乐观锁）
     */
    private Long version;
    /**
     * 是否删除：0-不删除，1-删除
     */
    private Integer isDeleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 库存描述，（富）文本格式
     */
    private String stockDesc;
    /**
     * 附加字段
     */
    private String addition;
}