package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class GroupEntity {

    private Integer id;

    private String groupName;

    private String groupDesc;

    private List<KeyEntity> keyEntityList;
}
