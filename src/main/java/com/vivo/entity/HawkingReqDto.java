package com.vivo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HawkingReqDto implements Serializable {
    private static final long serialVersionUID = -6313963564480194320L;

    //业务编号，vivo保险则为core.vbaoxian-cib.com
    private String businessCode;
    //模块编号，保险则为insurance
    private String moduleCode;
    //实验编号列表
    private List<String> testCodeList;
    //请求参数
    private Map<String, String> reqParam;
}