package com.vivo.service;

import com.vivo.entity.Person;

import java.util.List;

public interface PersonService {
     public List<Person> getAllPerson(int page,int pageSize);
}
