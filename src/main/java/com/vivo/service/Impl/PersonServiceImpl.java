package com.vivo.service.Impl;

import com.github.pagehelper.PageHelper;
import com.vivo.dao.PersonMapper;
import com.vivo.entity.Person;
import com.vivo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired(required = false)
    PersonMapper personMapper;

    @Override
    @Transactional
    public List<Person> getAllPerson(int page,int pageSize)
    {
        PageHelper.startPage(page,pageSize);
//        int i=1/0;
        return personMapper.selectByExample(null);
    }
}
