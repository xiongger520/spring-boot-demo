package com.vivo.service.Impl;

import com.vivo.dao.CheckInLogMapper;
import com.vivo.dao.CheckInMapper;
import com.vivo.dto.CheckInReqDto;
import com.vivo.entity.CheckInEntity;
import com.vivo.entity.CheckInExample;
import com.vivo.entity.CheckInLog;
import com.vivo.service.CheckInService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;

@Service
@Slf4j
public class CheckInServiceImpl implements CheckInService {

    @Resource
    private CheckInMapper checkInMapper;

    @Resource
    private CheckInLogMapper checkInLogMapper;


    //根据vivo账户id查询签到进度
    @Override
    public Map<List<Integer>, Integer> queryCheckInProgress(CheckInReqDto checkInReqDto) {
        //入参校验
        if (paramPreCheck(checkInReqDto) == false) {
            return null;
        }
        CheckInExample checkInExample = new CheckInExample();
        checkInExample.createCriteria().andBizCodeEqualTo(checkInReqDto.getBizCode()).andSerialNoEqualTo(checkInReqDto.getSerialNo()).andIsDeletedEqualTo(0);
        List<CheckInEntity> checkInEntityList = checkInMapper.selectByExample(checkInExample);
        Map<List<Integer>, Integer> result = new HashMap<>();
        Date today = new Date();
        //没有签到记录
        if (CollectionUtils.isEmpty(checkInEntityList)) {
            result.put(generateProgressBar(0), 1);
        }
        //有签到记录
        else {
            CheckInEntity checkInEntity = checkInEntityList.get(0);
            Date lastDate = checkInEntity.getLastDate();
            Integer conDays = checkInEntity.getConDays();
            //今天已签到，不可再签到
            if (DateUtils.isSameDay(lastDate, today)) {
                result.put(generateProgressBar(conDays), conDays);
            }
            //今天未签到
            else {
                //昨天已签到，进度继续，可签到
                if (DateUtils.isSameDay(lastDate, DateUtils.addDays(today, -1))) {
                    result.put(generateProgressBar(conDays), conDays);
                }
                //昨天未签到，进度归零，可签到
                else {
                    result.put(generateProgressBar(0), 1);
                }
            }
        }
        return result;
    }

    //根据vivo账户id签到
    @Override
    public Map<List<Integer>, String> checkIn(CheckInReqDto checkInReqDto) {
        //入参校验
        if (paramPreCheck(checkInReqDto) == false) {
            return null;
        }
        String openId = checkInReqDto.getOpenId();
        String memberNo = checkInReqDto.getMemberNo();
        Integer bizType = checkInReqDto.getBizType();
        String bizCode = checkInReqDto.getBizCode();
        String serialNo = checkInReqDto.getSerialNo();
        Date today = new Date();
        CheckInExample checkInExample = new CheckInExample();
        checkInExample.createCriteria().andBizCodeEqualTo(bizCode).andSerialNoEqualTo(serialNo).andIsDeletedEqualTo(0);
        List<CheckInEntity> checkInEntityList = checkInMapper.selectByExample(checkInExample);
        Map<List<Integer>, String> result = new HashMap<>();
        //没有历史签到记录
        if (CollectionUtils.isEmpty(checkInEntityList)) {
            //新增两条记录
            CheckInEntity record = new CheckInEntity(null, openId, memberNo, bizType, bizCode, serialNo, today, 1, 1L, 0, null, null);
            checkInMapper.insertSelective(record);
            CheckInLog logRecord = new CheckInLog(null, openId, memberNo, bizType, bizCode, serialNo, new Date(), 1L, 0, null, null);
            checkInLogMapper.insertSelective(logRecord);
            //返回进度为1的签到进度条
            result.put(generateProgressBar(1), "奖励20元");
        }
        //有历史签到记录
        else {
            CheckInEntity checkInEntity = checkInEntityList.get(0);
            Date lastDate = checkInEntity.getLastDate();
            Integer conDays = checkInEntity.getConDays();
            //今天未签到
            if (!DateUtils.isSameDay(lastDate, today)) {
                //昨天已签到
                if (DateUtils.isSameDay(lastDate, DateUtils.addDays(today, -1))) {
                    if (conDays == 1 || conDays == 2 || conDays == 3 || conDays == 4 || conDays == 5) {
                        updateCheckInInfo(checkInEntity.getId(), checkInEntity.getOpenId(), checkInEntity.getMemberNo(), checkInEntity.getBizType(), checkInEntity.getBizCode(), checkInEntity.getSerialNo(), today, ++conDays, checkInEntity.getVersion());
                        result.put(generateProgressBar(conDays), "奖励20元");
                    } else if (conDays == 6) {
                        updateCheckInInfo(checkInEntity.getId(), checkInEntity.getOpenId(), checkInEntity.getMemberNo(), checkInEntity.getBizType(), checkInEntity.getBizCode(), checkInEntity.getSerialNo(), today, ++conDays, checkInEntity.getVersion());
                        result.put(generateProgressBar(conDays), "翻倍啦,奖励40元");
                    } else if (conDays == 7) {
                        updateCheckInInfo(checkInEntity.getId(), checkInEntity.getOpenId(), checkInEntity.getMemberNo(), checkInEntity.getBizType(), checkInEntity.getBizCode(), checkInEntity.getSerialNo(), today, 1, checkInEntity.getVersion());
                        result.put(generateProgressBar(1), "奖励20元");
                    } else {
                        updateCheckInInfo(checkInEntity.getId(), checkInEntity.getOpenId(), checkInEntity.getMemberNo(), checkInEntity.getBizType(), checkInEntity.getBizCode(), checkInEntity.getSerialNo(), today, 1, checkInEntity.getVersion());
                        result.put(generateProgressBar(conDays), "奖励20元");
                    }
                }
                //昨天没有签到
                else {
                    updateCheckInInfo(checkInEntity.getId(), checkInEntity.getOpenId(), checkInEntity.getMemberNo(), checkInEntity.getBizType(), checkInEntity.getBizCode(), checkInEntity.getSerialNo(), today, 1, checkInEntity.getVersion());
                    result.put(generateProgressBar(1), "奖励20元");
                }
            }
            //今天已经签到
            else {
                result.put(generateProgressBar(conDays), "今天您已经签到了!");
            }
        }
        return result;
    }

    public Boolean paramPreCheck(CheckInReqDto checkInReqDto) {
        //memberNo可为空，不做校验
        if (checkInReqDto == null) {
            log.error("入参为空");
            return false;
        } else if (StringUtils.isBlank(checkInReqDto.getOpenId())) {
            log.error("openId为空");
            return false;
        } else if (checkInReqDto.getBizType() == null) {
            log.error("bizType为空");
            return false;
        } else if (StringUtils.isBlank(checkInReqDto.getBizCode())) {
            log.error("bizCode为空");
            return false;
        } else if (StringUtils.isBlank(checkInReqDto.getSerialNo())) {
            log.error("serialNo为空");
            return false;
        } else return true;
    }

    //生成进度条
    public List<Integer> generateProgressBar(Integer conDays) {
        List<Integer> result = new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0, 0));
        if (conDays != null && conDays <= 7) {
            for (int i = 0; i < conDays; i++) {
                result.set(i, 1);
            }
        }
        return result;
    }

    //更新签到信息
    @Transactional(rollbackFor = Exception.class)
    public Integer updateCheckInInfo(Long id, String openId, String memberNo, Integer bizType, String bizCode, String serialNo, Date lastDate, Integer conDays, Long version) {
        CheckInEntity record = new CheckInEntity();
        record.setId(id);
        record.setVersion(version);
        record.setLastDate(lastDate);
        record.setConDays(conDays);
        //更新签到表数据，乐观锁
        checkInMapper.updateByPrimaryKeySelective(record);
        CheckInLog logRecord = new CheckInLog();
        logRecord.setOpenId(openId);
        logRecord.setMemberNo(memberNo);
        logRecord.setBizType(bizType);
        logRecord.setBizCode(bizCode);
        logRecord.setSerialNo(serialNo);
        logRecord.setCheckInTime(new Date());
        logRecord.setVersion(1L);
        logRecord.setIsDeleted(0);
        //插入签到记录表数据
        return checkInLogMapper.insertSelective(logRecord);
    }
}
