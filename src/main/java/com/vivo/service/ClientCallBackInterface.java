package com.vivo.service;

//客户端回调接口
public interface ClientCallBackInterface {
    //接收完成通知
    public void acceptCompletedNotice(String notice);
}
