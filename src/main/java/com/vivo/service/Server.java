package com.vivo.service;

public class Server {
    public void respond(ClientCallBackInterface clientCallBackInterface, String request) throws InterruptedException {
        System.out.println("客户端请求内容:" + request + ",服务端处理完毕!");
        Thread.sleep(5000);
        clientCallBackInterface.acceptCompletedNotice("200");
    }
}
