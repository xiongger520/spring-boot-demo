package com.vivo.service;

public class SevenDiscount implements PriceStrategy {
    @Override
    public Double getDiscountPrice(Double originalPrice) {
        return 0.7 * originalPrice;
    }
}
