package com.vivo.service;

public interface PriceStrategy {
    //根据原始价获取折后价
    public Double getDiscountPrice(Double originalPrice);
}
