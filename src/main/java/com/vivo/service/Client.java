package com.vivo.service;

import lombok.AllArgsConstructor;

//客户端先调用服务端，服务端然后再回调客户端，即通知客户端操作已完成,核心：把调用方（客户端）/回调接口的引用传递给被调用方（服务端）
@AllArgsConstructor
public class Client implements ClientCallBackInterface {

    public void request() throws InterruptedException {
        new Server().respond(this, "请求登陆");
    }

    @Override
    public void acceptCompletedNotice(String notice) {
        System.out.println("服务端返回的通知内容:" + notice);
    }
}
