package com.vivo.service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PriceContext {

    private PriceStrategy priceStrategy;

    public Double getDiscountPrice(Double originalPrice) {
        return priceStrategy.getDiscountPrice(originalPrice);
    }

}
