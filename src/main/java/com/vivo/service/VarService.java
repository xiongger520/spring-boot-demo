package com.vivo.service;

public interface VarService {

    public void atest(String productCode);

    public void btest(String productCode, String... args);

    //发送http请求  requestUrl为请求地址  requestMethod请求方式，值为"GET"或"POST"
    public String SendHttpRequest(String requestUrl, String requestMethod, String outputStr);
}
