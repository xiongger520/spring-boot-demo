package com.vivo.service;

public class SixDiscount implements PriceStrategy {
    @Override
    public Double getDiscountPrice(Double originalPrice) {
        return 0.6 * originalPrice;
    }
}
