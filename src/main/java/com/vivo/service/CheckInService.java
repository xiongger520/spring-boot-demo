package com.vivo.service;

import com.vivo.dto.CheckInReqDto;

import java.util.List;
import java.util.Map;

public interface CheckInService {

    //根据vivo账户id查询签到进度
    public Map<List<Integer>, Integer> queryCheckInProgress(CheckInReqDto checkInReqDto);

    //根据vivo账户id签到
    public Map<List<Integer>, String> checkIn(CheckInReqDto checkInReqDto);
}
