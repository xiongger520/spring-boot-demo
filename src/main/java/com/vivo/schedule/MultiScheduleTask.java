//package com.vivo.schedule;
//
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.scheduling.annotation.EnableAsync;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//
//@Component
//@EnableScheduling
//@EnableAsync
//public class MultiScheduleTask {
//    @Async
//    //间隔360秒
//    @Scheduled(fixedDelay = 360000)
//    public void first() {
//        System.out.println("第1个定时任务开始 : " + LocalDateTime.now().toLocalTime() + "线程 : " + Thread.currentThread().getName());
//    }
//
//    @Async
//    //间隔60秒
//    @Scheduled(fixedDelay = 60000)
//    public void second() {
//        System.out.println("第2个定时任务开始 : " + LocalDateTime.now().toLocalTime() + "线程 : " + Thread.currentThread().getName());
//    }
//}
