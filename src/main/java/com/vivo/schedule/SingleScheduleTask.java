//package com.vivo.schedule;
//
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//
//@Component
//@EnableScheduling
//public class SingleScheduleTask {
//
//    @Scheduled(cron = "0/360 * * * * ?")
//    //或直接指定时间间隔，例如：5秒
//    //@Scheduled(fixedRate=5000)
//    private void configureTasks() {
//        System.out.println("执行单个定时任务----->" + LocalDateTime.now());
//    }
//}