package com.vivo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//响应包装泛型类
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCenterRespDTO<T> {
    //结果编号
    private String code;
    //结果描述
    private String msg;
    //响应数据
    private T data;
}

