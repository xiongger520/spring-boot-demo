package com.vivo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

//股票类
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class StockDto implements Serializable {
    private static final long serialVersionUID = 6673293517719474089L;
    //股票名称
    private String stockName;
    //股票代码
    private String stockCode;
    //涨跌幅
    private Double change;
    //强势指数
    private Double avgPercent;
    //强势指数
    private String market;

    @Override
    public String toString() {
        return stockName + "," + stockCode + "," + String.format("%.2f%%,", change) + market + "," + String.format("%.2f%%", avgPercent);
    }
}
