package com.vivo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RiskControlTagDTO {
    @ApiModelProperty(value = "标签code")
    private Integer code;

    @ApiModelProperty("标签描述")
    private String desc;

    @ApiModelProperty("子标签")
    private List<RiskControlTagDTO> subRiskControlTagList;
}
