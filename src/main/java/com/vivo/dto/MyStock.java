package com.vivo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyStock {
    private String code;

    private String name;

    private Double change;

    private String market;

    private String nDays;

    private Double capital;

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MyStock) {
            MyStock myStock = (MyStock) obj;
            return myStock.code.equals(code) ? true : false;
        } else {
            return false;
        }
    }
}
