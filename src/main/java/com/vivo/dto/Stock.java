package com.vivo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stock implements Serializable {
    //股票代码
    private String code;
    //股票名称
    private String name;
    //涨跌幅%
    private Double change;
    //下一日涨跌幅%
    private Double nextChange;
    //所属行业
    private String market;
    //所属概念
    private String concepts;
    //价格
    private Double price;
    //市值
    private Double marketValue;
    //总股本
    private Double totalCapital;
    //价值比
    private Double ratio;
    //今年振幅%
    private Double yearAmplitude;
    //告警类型
    private String warnType;
    //备注
    private String remark;
    //是否持有
    private Boolean isHeld;

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Stock) {
            Stock stock = (Stock) obj;
            return stock.code.equals(code) ? true : false;
        } else {
            return false;
        }
    }
}
