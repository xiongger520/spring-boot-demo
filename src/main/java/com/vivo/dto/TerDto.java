package com.vivo.dto;

import java.io.Serializable;
import java.util.List;

//均线类
public class TerDto implements Serializable {

    private List<String> list;

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
}
