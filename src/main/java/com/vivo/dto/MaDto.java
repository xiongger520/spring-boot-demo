package com.vivo.dto;

import java.io.Serializable;

//均线类
public class MaDto implements Serializable {
    private static final long serialVersionUID = -3614297230861065695L;
    //均线名称
    private String maName;
    //均线值
    private Double maValue;

    public MaDto() {
    }

    public MaDto(String maName, Double maValue) {
        this.maName = maName;
        this.maValue = maValue;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getMaName() {
        return maName;
    }

    public void setMaName(String maName) {
        this.maName = maName;
    }

    public Double getMaValue() {
        return maValue;
    }

    public void setMaValue(Double maValue) {
        this.maValue = maValue;
    }

    @Override
    public String toString() {
        return "MaDto{" +
                "maName='" + maName + '\'' +
                ", maValue=" + maValue +
                '}';
    }
}
