package com.vivo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckInReqDto {
    //vivo账号id
    private String openId;
    //钱包账号id
    private String memberNo;
    //业务类型：1-自营贷款，2-引流业务，3-保险业务
    private Integer bizType;
    //业务编号
    private String bizCode;
    //业务流水号
    private String serialNo;
}

