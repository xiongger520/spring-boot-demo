package com.vivo.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

//K线类
public class KDto implements Serializable {
    private static final long serialVersionUID = 6673293517719474089L;
    //收盘时间
    private LocalDateTime day;
    //开盘价
    private String open;
    //最高价
    private String high;
    //最低价
    private String low;
    //收盘价
    private String close;
    //成交股数
    private String volume;

    public KDto() {
    }

    public KDto(LocalDateTime day, String open, String high, String low, String close, String volume) {
        this.day = day;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public LocalDateTime getDay() {
        return day;
    }

    public void setDay(LocalDateTime day) {
        this.day = day;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }
}
