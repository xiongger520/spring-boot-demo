package com.vivo.redis;

import com.vivo.entity.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class FindPerson {

    @Cacheable(cacheNames = RedisUtil.REDIS_KEY_PREFIX, key = "#id")
    public Person findPersonById(Integer id) {
        Person person = new Person(id, "潘志雄", null, 44);
        log.info("缓存没有命中----------->");
        return person;
    }

    @CacheEvict(cacheNames = RedisUtil.REDIS_KEY_PREFIX, key = "#id")
    public void deletePerson(Integer id) {
        log.info("删除{}缓存成功----------->", id);
    }
}
