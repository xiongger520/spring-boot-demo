package com.vivo.redis;

import com.vivo.entity.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class RedisController {

    // redis中缓存过月时间，单位:秒
    private static int EXPIRE_TIME = 360;

    @Autowired
    private RedisUtil redisUtil;

    @RequestMapping("/redisSet")
    public boolean redisSet(String key) {
        Person person = new Person(Integer.valueOf(key), "潘志雄", "男", 25);
        return redisUtil.set(redisUtil.REDIS_KEY_PREFIX + key, person, EXPIRE_TIME);
    }

    @RequestMapping("/redisGet")
    public Person redisGet(String key) {
        return (Person) redisUtil.get(redisUtil.REDIS_KEY_PREFIX + key);
    }

    @RequestMapping("/redisHSet")
    public boolean redisHSet(String key, String item) {
        //key=panzhixiong,item=1,2,3……
        Person person = new Person(Integer.valueOf(item), "潘志雄", "男", 25);
        return redisUtil.hset(redisUtil.REDIS_KEY_PREFIX + key, item, person, EXPIRE_TIME);
    }

    @RequestMapping("/redisHGet")
    public Person redisHGet(String key, String item) {
        //key=panzhixiong,item=1,2,3……
        return (Person) redisUtil.hget(redisUtil.REDIS_KEY_PREFIX + key, item);
    }
}