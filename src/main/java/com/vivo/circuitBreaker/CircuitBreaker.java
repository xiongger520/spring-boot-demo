package com.vivo.circuitBreaker;


/**
 * 熔断器
 */
public class CircuitBreaker {
    /**
     * 熔断器当前状态，初始是关闭态
     */
    public volatile State state;

    /**
     * 资源名，全局唯一
     */
    public String resourceName;

    /**
     * 熔断规则
     */
    public DegradeRule degradeRule;

    private CircuitBreaker() {
    }

    public CircuitBreaker(String resourceName, long statisticsSeconds, long degradeExcCnt, double degradeExcPct, long degradeSeconds, double detectReqPct, long normalExcCnt, double normalExcPct) {
        this.resourceName = resourceName;
        degradeRule = new DegradeRule(statisticsSeconds, degradeExcCnt, degradeExcPct, degradeSeconds, detectReqPct, normalExcCnt, normalExcPct);
        state = new CloseState(this);
    }

    /**
     * 获取当前状态
     */
    public StateEnum getStateEnum() {
        return state.getStateEnum();
    }

    /**
     * 设置状态
     */
    public void setState(State state) {
        //加锁
        synchronized (this) {
            StateEnum current = getStateEnum();
            StateEnum next = state.getStateEnum();
            if (current.equals(next)) {
                return;
            }
            // 更新状态
            this.state = state;
            System.out.println("熔断器状态转移：" + current.getName() + "->" + next.getName());
        }
    }

    /**
     * 尝试通过
     */
    public boolean tryPass() {
        return state.tryPass(this);
    }

    /**
     * 统计
     */
    public void statistic(boolean hasException) {
        state.statistic(this, hasException);
    }


    public static void main(String[] args) {
        CircuitBreaker cb = new CircuitBreaker("test",10, 1, 0.1, 10, 0.2, 5, 0.1);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    try {
                        Thread.sleep(1000);
                        if (cb.tryPass()) {
                            System.out.println(String.format("执行业务逻辑%d", i));
                            if (((i >= 0 && i < 20) || (i >= 60 && i < 80)) && i % 3 == 1) {
                                throw new NullPointerException();
                            }
                            cb.statistic(false);
                        } else {
                            System.out.println(String.format("降级逻辑%d", i));
                        }
                    } catch (Exception e) {
                        System.out.println("抛出异常,EEROR");
                        cb.statistic(true);
                    }
                }
            }
        };
        new Thread(runnable).start();
        new Thread(runnable).start();
        new Thread(runnable).start();
    }
}
