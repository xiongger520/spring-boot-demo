package com.vivo.circuitBreaker;

import lombok.extern.slf4j.Slf4j;

/**
 * 熔断器-关闭状态
 */
@Slf4j
public class CloseState implements State {
    /**
     * 进入此状态的时间戳
     */
    private long entryTimestamp = System.currentTimeMillis();

    /**
     * 总请求计数滑动窗口
     */
    private SlidingWindow totalSlidingWindow;

    /**
     * 异常计数滑动窗口
     */
    private SlidingWindow exceptionSlidingWindow;

    public CloseState(CircuitBreaker cb) {
        totalSlidingWindow = new SlidingWindow("total", cb.degradeRule.statisticsSeconds);
        exceptionSlidingWindow = new SlidingWindow("exception", cb.degradeRule.statisticsSeconds);
    }

    public StateEnum getStateEnum() {
        return StateEnum.CLOSE;
    }

    public void checkAndChangeState(CircuitBreaker cb) {
        long exceptionNum = exceptionSlidingWindow.getCount().longValue();
        if (exceptionNum >= cb.degradeRule.degradeExcCnt) {
            long totalNum = totalSlidingWindow.getCount().longValue();
            if (totalNum > 0) {
                double exceptionPercent = 1.0 * exceptionNum / totalNum;
                System.out.println(String.format("总个数:%d,异常数:%d,异常比例:%.2f%%", totalNum, exceptionNum, exceptionPercent * 100));
                if (exceptionPercent >= cb.degradeRule.degradeExcPct) {
                    cb.setState(new OpenState());
                }
            }
        }
    }

    public boolean tryPass(CircuitBreaker cb) {
        return true;
    }

    public void statistic(CircuitBreaker cb, boolean hasException) {
        totalSlidingWindow.increase();
        if (hasException) {
            exceptionSlidingWindow.increase();
        }
        checkAndChangeState(cb);
    }
}
