package com.vivo.circuitBreaker;

/**
 * 熔断状态
 */
public interface State {

    /**
     * 获取状态枚举
     */
    StateEnum getStateEnum();

    /**
     * 校验并改变状态
     */
    void checkAndChangeState(CircuitBreaker cb);

    /**
     * 尝试通过
     */
    boolean tryPass(CircuitBreaker cb);

    /**
     * 统计
     */
    void statistic(CircuitBreaker cb, boolean hasException);
}
