package com.vivo.circuitBreaker;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.vivo.circuitBreaker.CircuitBreaker;
import com.vivo.circuitBreaker.DegradeRule;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class CBService {

    private Map<String, CircuitBreaker> cbMap = new HashMap();

    private Map<String, DegradeRule> ruleMap = new HashMap();

    /**
     * 加载熔断规则
     */
    private void loadRule(String resourceName) {
        JSONObject jsonObject = JSON.parseObject("{\"test\":{\"statisticsSeconds\":30,\"degradeExcCnt\":1,\"degradeExcPct\":0.1,\"degradeSeconds\":30,\"detectReqPct\":0.2,\"normalExcCnt\":5,\"normalExcPct\":0.1}}")
                .getJSONObject(resourceName);
        if (null == jsonObject) {
            return;
        }
        DegradeRule degradeRule = new DegradeRule();
        degradeRule.setStatisticsSeconds(jsonObject.getLong("statisticsSeconds"));
        degradeRule.setDegradeExcCnt(jsonObject.getLong("degradeExcCnt"));
        degradeRule.setDegradeExcPct(jsonObject.getDouble("degradeExcPct"));
        degradeRule.setDegradeSeconds(jsonObject.getLong("degradeSeconds"));
        degradeRule.setDetectReqPct(jsonObject.getDouble("detectReqPct"));
        degradeRule.setNormalExcCnt(jsonObject.getLong("normalExcCnt"));
        degradeRule.setNormalExcPct(jsonObject.getDouble("normalExcPct"));
        ruleMap.put(resourceName, degradeRule);
    }

    @GetMapping("/tryPass")
    public Boolean tryPass(String resourceName, Boolean hasException) {
        if (StringUtils.isBlank(resourceName)) {
            return true;
        }
        loadRule(resourceName);
        DegradeRule degradeRule = ruleMap.get(resourceName);
        if (null == degradeRule) {
            return true;
        }
        CircuitBreaker cb = cbMap.get(resourceName);
        //没有熔断器则初始化，规则发生变更也重新初始化
        if (null == cb || !cb.degradeRule.getRuleString().equals(degradeRule.getRuleString())) {
            cb = new CircuitBreaker(resourceName, degradeRule.getStatisticsSeconds(), degradeRule.getDegradeExcCnt(),
                    degradeRule.getDegradeExcPct(), degradeRule.getDegradeSeconds(), degradeRule.getDetectReqPct(),
                    degradeRule.getNormalExcCnt(), degradeRule.getNormalExcPct());
            cbMap.put(resourceName, cb);
        }
        boolean rs = cb.tryPass();
        cb.statistic(hasException);
        System.out.println(String.format("结果:%s", rs));
        return rs;
    }
}