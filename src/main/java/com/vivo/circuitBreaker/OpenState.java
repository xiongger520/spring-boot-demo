package com.vivo.circuitBreaker;

/**
 * 熔断器-打开状态
 */
public class OpenState implements State {
    /**
     * 进入此状态的时间戳
     */
    private long entryTimestamp = System.currentTimeMillis();

    public StateEnum getStateEnum() {
        return StateEnum.OPEN;
    }

    public void checkAndChangeState(CircuitBreaker cb) {
        //超过熔断时间则变为半开状态
        if ((System.currentTimeMillis() - entryTimestamp) / 1000 >= cb.degradeRule.degradeSeconds) {
            cb.setState(new HalfOpenState());
        }
    }

    public boolean tryPass(CircuitBreaker cb) {
        checkAndChangeState(cb);
        return false;
    }

    public void statistic(CircuitBreaker cb, boolean hasException) {
        //熔断器打开状态下不做任何统计
    }
}
