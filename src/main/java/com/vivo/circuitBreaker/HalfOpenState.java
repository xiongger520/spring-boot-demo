package com.vivo.circuitBreaker;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 熔断器-半开状态
 */
public class HalfOpenState implements State {
    /**
     * 进入此状态的时间戳
     */
    private long entryTimestamp = System.currentTimeMillis();

    /**
     * 探测流量总请求计数器
     */
    private AtomicLong totalCount = new AtomicLong(0);

    /**
     * 探测流量异常计数器
     */
    private AtomicLong exceptionCount = new AtomicLong(0);


    public StateEnum getStateEnum() {
        return StateEnum.HALF_OPEN;
    }

    public void checkAndChangeState(CircuitBreaker cb) {
        //过了熔断时间
        if ((System.currentTimeMillis() - entryTimestamp) / 1000 >= cb.degradeRule.statisticsSeconds) {
            long totalNum = totalCount.longValue();
            long exceptionNum = exceptionCount.longValue();
            double exceptionPercent = totalNum > 0 ? 1.0 * exceptionNum / totalCount.longValue() : 0.0;
            System.out.println(String.format("总个数:%d,异常数:%d,异常比例:%.2f%%",totalNum, exceptionNum, exceptionPercent * 100));
            //已经恢复
            if (exceptionNum < cb.degradeRule.normalExcCnt && exceptionPercent < cb.degradeRule.normalExcPct) {
                cb.setState(new CloseState(cb));
            }
            //没有恢复
            else {
                cb.setState(new OpenState());
            }
        }
    }

    public boolean tryPass(CircuitBreaker cb) {
        checkAndChangeState(cb);
        //随机放量
        if (new Random().nextInt(100) <= cb.degradeRule.detectReqPct * 100) {
            System.out.println(String.format("探测流量PassPass"));
            return true;
        } else {
            System.out.println(String.format("其他流量Block"));
            return false;
        }
    }

    public void statistic(CircuitBreaker cb, boolean hasException) {
        totalCount.incrementAndGet();
        if (hasException) {
            exceptionCount.incrementAndGet();
        }
        checkAndChangeState(cb);
    }
}
