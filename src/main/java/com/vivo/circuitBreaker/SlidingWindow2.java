package com.vivo.circuitBreaker;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

public class SlidingWindow2 {

    /**
     * 统计时长（秒数）
     */
    private long statisticSeconds;

    /**
     * 每个小窗口统计时长（毫秒）
     */
    private long windowMillis = 1000;

    /**
     * 窗口列表
     */
    private Window[] windowArray;

    public SlidingWindow2(int seconds) {
        statisticSeconds = seconds;
        long currentTimeMillis = System.currentTimeMillis();
        windowArray = new Window[seconds];
        Arrays.fill(windowArray, new Window(currentTimeMillis, new AtomicLong(0)));
    }

    public synchronized long increase() {
        long now = System.currentTimeMillis();
        int currentIndex = (int) ((now / 1000) % statisticSeconds);
        System.out.println(String.format("currentIndex：%d", currentIndex));
        long sum = 0L;
        for (int i = 0; i < windowArray.length; i++) {
            Window window = windowArray[i];
            if ((now - window.getTime()) > statisticSeconds) {
                window.getCount().set(0);
                window.setTime(now);
            }
            if (currentIndex == i) {
                window.getCount().incrementAndGet();
            }
            sum += window.getCount().longValue();
        }
        System.out.println(String.format("统计耗时%dms", System.currentTimeMillis() - now));
        return sum;
    }

    private class Window {
        //时间戳
        private long time;
        //计数器
        private AtomicLong count;

        public Window(long time, AtomicLong count) {
            this.time = time;
            this.count = count;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public AtomicLong getCount() {
            return count;
        }

        public void setCount(AtomicLong count) {
            this.count = count;
        }
    }

    public static void main(String[] args) {
        SlidingWindow2 slidingWindow2 = new SlidingWindow2(10);
        for (int i = 0; i < 100000; i++) {
            System.out.println(String.format("计数：%d", slidingWindow2.increase()));
        }
    }
}

