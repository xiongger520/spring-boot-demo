package com.vivo.circuitBreaker;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

public class SlidingWindow {

    /**
     * 滑动窗口名称
     */
    private String name;
    /**
     * 时间戳队列
     */
    private ConcurrentLinkedQueue<Long> queue;
    /**
     * 计数器
     */
    private AtomicLong count;
    /**
     * 窗口总时长（单位：毫秒）
     */
    private long timeWindowMillis;

    public SlidingWindow(String name, long timeWindowSeconds) {
        this.name = name;
        this.queue = new ConcurrentLinkedQueue<>();
        this.count = new AtomicLong(0);
        this.timeWindowMillis = timeWindowSeconds * 1000;
    }

    public AtomicLong getCount() {
        return count;
    }

    public long increase() {
        long now = System.currentTimeMillis();
        synchronized (this) {
            while (!queue.isEmpty() && queue.peek() < now - timeWindowMillis) {
                queue.poll();
                count.decrementAndGet();
            }
            queue.add(now);
            count.incrementAndGet();
            long rs = count.longValue();
            System.out.println(String.format("%s窗口计数:%d,耗时%dms", name, rs, System.currentTimeMillis() - now));
            return rs;
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {
            SlidingWindow slidingWindow = new SlidingWindow("total", 1L);
            slidingWindow.increase();
        }
    }
}

