package com.vivo.circuitBreaker;


/**
 * 熔断规则
 */
public class DegradeRule {

    /**
     * 统计时长（单位：秒）
     */
    public long statisticsSeconds;

    /**
     * 触发熔断的最小异常数，异常数<该值，不触发熔断
     */
    public long degradeExcCnt;

    /**
     * 触发熔断的最小异常比例，异常比例>=该值，触发熔断
     */
    public double degradeExcPct;

    /**
     * 熔断持续时长（单位：秒）
     */
    public long degradeSeconds;

    /**
     * 探测请求比例
     */
    public double detectReqPct;

    /**
     * 恢复正常的最大异常数，探测流量异常数<该值，恢复正常
     */
    public long normalExcCnt;

    /**
     * 恢复正常的最大异常比例，探测流量异常比例<该值，恢复正常
     */
    public double normalExcPct;


    public DegradeRule() {
    }

    public DegradeRule(long statisticsSeconds, long degradeExcCnt, double degradeExcPct, long degradeSeconds, double detectReqPct, long normalExcCnt, double normalExcPct) {
        this.statisticsSeconds = statisticsSeconds;
        this.degradeExcCnt = degradeExcCnt;
        this.degradeExcPct = degradeExcPct;
        this.degradeSeconds = degradeSeconds;
        this.detectReqPct = detectReqPct;
        this.normalExcCnt = normalExcCnt;
        this.normalExcPct = normalExcPct;
    }

    public String getRuleString() {
        return String.format("%d,%d,%.2f,%d,%.2f,%d,%.2f", statisticsSeconds, degradeExcCnt, degradeExcPct, degradeSeconds, detectReqPct, normalExcCnt, normalExcPct);
    }

    public long getStatisticsSeconds() {
        return statisticsSeconds;
    }

    public void setStatisticsSeconds(long statisticsSeconds) {
        this.statisticsSeconds = statisticsSeconds;
    }

    public long getDegradeExcCnt() {
        return degradeExcCnt;
    }

    public void setDegradeExcCnt(long degradeExcCnt) {
        this.degradeExcCnt = degradeExcCnt;
    }

    public double getDegradeExcPct() {
        return degradeExcPct;
    }

    public void setDegradeExcPct(double degradeExcPct) {
        this.degradeExcPct = degradeExcPct;
    }

    public long getDegradeSeconds() {
        return degradeSeconds;
    }

    public void setDegradeSeconds(long degradeSeconds) {
        this.degradeSeconds = degradeSeconds;
    }

    public double getDetectReqPct() {
        return detectReqPct;
    }

    public void setDetectReqPct(double detectReqPct) {
        this.detectReqPct = detectReqPct;
    }

    public long getNormalExcCnt() {
        return normalExcCnt;
    }

    public void setNormalExcCnt(long normalExcCnt) {
        this.normalExcCnt = normalExcCnt;
    }

    public double getNormalExcPct() {
        return normalExcPct;
    }

    public void setNormalExcPct(double normalExcPct) {
        this.normalExcPct = normalExcPct;
    }
}
