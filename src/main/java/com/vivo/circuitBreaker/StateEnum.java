package com.vivo.circuitBreaker;

/**
 * 熔断状态枚举
 */
public enum StateEnum {

    CLOSE(0, "熔断器关闭"),
    OPEN(1, "熔断器打开"),
    HALF_OPEN(2, "熔断器半开");

    StateEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    private int code;

    private String name;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
