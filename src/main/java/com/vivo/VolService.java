package com.vivo;

import com.alibaba.fastjson.JSON;
import com.vivo.entity.KDto;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VolService {

    private CloseableHttpClient httpClient = HttpClients.createDefault();
    private Map<String, HttpGet> map = new HashMap<>();
    private Map<String, double[]> cache = new HashMap<>();
    private LocalTime nine31 = LocalTime.of(9, 31, 0, 0);

    //计算上一个交易日实时成交量
    public Double calPreviousVol(Integer timeIndex, String sinaCode) {
        //尝试从缓存中获取
        double[] array = cache.get(sinaCode);
        if (null != array) {
            return array[timeIndex];
        }
        Double result = 0.0;
        try {
            HttpGet httpGet = map.get(sinaCode);
            if (null == httpGet) {
                httpGet = new HttpGet(String.format("https://quotes.sina.cn/cn/api/jsonp_v2.php/var/CN_MarketDataService.getKLineData?symbol=%s&scale=1&ma=no&datalen=500", sinaCode));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(500).build());
                map.put(sinaCode, httpGet);
            }
            String raw = EntityUtils.toString(httpClient.execute(httpGet).getEntity());
            List<KDto> kDtoList = JSON.parseArray(raw.substring(53, raw.length() - 2), KDto.class);
            Integer size = kDtoList.size();
            //计算起始边界和结束边界
            Integer beginIdx = null;
            Integer endIdx = null;
            LocalDate localDate = kDtoList.get(size - 1).getDay().toLocalDate();
            Integer idx = size - 2;
            for (; idx >= 0; idx--) {
                LocalDate com = kDtoList.get(idx).getDay().toLocalDate();
                if (!localDate.isEqual(com)) {
                    if (null == endIdx) {
                        endIdx = idx;
                        localDate = com;
                    } else {
                        beginIdx = idx + 1;
                        break;
                    }
                }
            }
            array = new double[240];
            Double sum = 0.0;
            idx = 0;
            for (Integer i = beginIdx; i <= endIdx; i++, idx++) {
                sum = sum + Double.valueOf(kDtoList.get(i).getVolume());
                array[idx] = sum;
            }
            //用最后一个值填充剩余空值
            for (; idx < 240; idx++) {
                array[idx] = sum;
            }
            cache.put(sinaCode, array);
            result = array[timeIndex];
        } catch (Exception e) {
            System.out.println(sinaCode + "上一个交易日实时成交量计算失败!");
            e.printStackTrace();
        }
        return result;
    }
}
