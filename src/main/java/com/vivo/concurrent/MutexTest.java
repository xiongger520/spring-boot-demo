package com.vivo.concurrent;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;

public class MutexTest {
    public static void main(String[] args) {
        Map<String, BigDecimal> map = new HashMap<>();
        map.put("1", null);
        map.put("2", BigDecimal.ZERO);
        System.out.println(map.get("1"));
        System.out.println(map.size());
        Lock lock = new Mutex();
        new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();
                System.out.println(Thread.currentThread().getName() + " acquire lock");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                lock.unlock();
                System.out.println(Thread.currentThread().getName() + " release lock");
            }
        }, "Thread-1").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();
                System.out.println(Thread.currentThread().getName() + " acquire lock");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                lock.unlock();
                System.out.println(Thread.currentThread().getName() + " release lock");
            }
        }, "Thread-2").start();
    }
}
