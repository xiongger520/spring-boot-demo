package com.vivo.concurrent;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Router {

    public static CloseableHttpClient realHttpClient = HttpClients.createDefault();

    public static String getCookie() throws IOException {
        HttpPost httpPost = new HttpPost("http://192.168.1.1/cgi-bin/luci");
        httpPost.addHeader("Connection", "keep-alive");
        httpPost.addHeader("Cache-Control", "max-age=0");
        httpPost.addHeader("Upgrade-Insecure-Requests", "1");
        httpPost.addHeader("Origin", "http://192.168.1.1");
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36");
        httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        httpPost.addHeader("Referer", "http://192.168.1.1/cgi-bin/luci");
        httpPost.addHeader("Accept-Language", "zh-CN,zh;q=0.9");
        List<NameValuePair> nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("username", "useradmin"));
        nameValuePairList.add(new BasicNameValuePair("psd", "tmywz"));
        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
        httpPost.setEntity(urlEncodedFormEntity);
        httpPost.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
        CloseableHttpResponse response = realHttpClient.execute(httpPost);
        Header header = response.getFirstHeader("Set-Cookie");
        return header.getValue().split(";")[0].substring(8);
    }

    public static String geyToken(String cookie) throws IOException {
        HttpPost httpPost = new HttpPost("http://192.168.1.1/cgi-bin/luci/admin/device/pc");
        httpPost.addHeader("Connection", "keep-alive");
        httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36");
        httpPost.addHeader("Content-type", "application/x-www-form-urlencoded");
        httpPost.addHeader("Accept", "*/*");
        httpPost.addHeader("Origin", "http://192.168.1.1");
        httpPost.addHeader("Referer", "http://192.168.1.1/cgi-bin/luci/admin/device/pc");
        httpPost.addHeader("Accept-Language", "zh-CN,zh;q=0.9");
        httpPost.addHeader("Cookie", "sysauth=" + cookie);
        httpPost.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
        String rst = EntityUtils.toString(realHttpClient.execute(httpPost).getEntity());
        Pattern pattern = Pattern.compile("token: '\\w+'");
        Matcher matcher = pattern.matcher(rst);
        while (matcher.find()) {
            String find = matcher.group();
            return find.substring(8, find.length() - 1);
        }
        return null;
    }

    public static String setControl(String cookie, String token, String restrict, String mac) throws IOException {
        HttpPost httpPost = new HttpPost("http://192.168.1.1/cgi-bin/luci/admin/device/hostSettings");
        httpPost.addHeader("Connection", "keep-alive");
        httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36");
        httpPost.addHeader("Content-type", "application/x-www-form-urlencoded");
        httpPost.addHeader("Accept", "*/*");
        httpPost.addHeader("Origin", "http://192.168.1.1");
        httpPost.addHeader("Referer", "http://192.168.1.1/cgi-bin/luci/admin/device/pc");
        httpPost.addHeader("Accept-Language", "zh-CN,zh;q=0.9");
        httpPost.addHeader("Cookie", "sysauth=" + cookie);
        List<NameValuePair> nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", token));
        nameValuePairList.add(new BasicNameValuePair("type", "restrict"));
        nameValuePairList.add(new BasicNameValuePair("onOff", restrict));
        nameValuePairList.add(new BasicNameValuePair("mac", mac));
        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
        httpPost.setEntity(urlEncodedFormEntity);
        httpPost.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
        return EntityUtils.toString(realHttpClient.execute(httpPost).getEntity());
    }

    public static void sleep(Integer seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (Exception e) {
        }
    }


    public static void main(String[] args) throws Exception {
        //登陆
        String cookie = getCookie();
        String token = geyToken(cookie);
        //192.168.1.2
        //String rst = setControl(cookie, token, "0", "94D9B3488B7B");
        while (true) {
            try {
                //192.168.1.9
                Boolean result = "{\"setRes\":0}".equals(setControl(cookie, token, "1", "244BFE5A7123"));
                System.out.println(LocalTime.now() + "禁用" + (Boolean.TRUE.equals(result) ? "成功" : "失败"));
                sleep(10);
                Boolean result2 = "{\"setRes\":0}".equals(setControl(cookie, token, "0", "244BFE5A7123"));
                System.out.println(LocalTime.now() + "启用" + (Boolean.TRUE.equals(result2) ? "成功" : "失败"));
                sleep(10);
            } catch (Exception e) {
                System.out.println(LocalTime.now() + "开始重试……");
                sleep(1 * 60);
                cookie = getCookie();
                token = geyToken(cookie);
                System.out.println(LocalTime.now() + "重试成功……");
            }
        }
    }
}
