package com.vivo.concurrent;

import com.alibaba.fastjson.JSON;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Sync {

    private static Lock lock = new ReentrantLock();


    public static void main(String[] args) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet();
        httpGet.setURI(URI.create("http://dcfm.eastmoney.com/EM_MutiSvcExpandInterface/api/js/get?type=HSGT20_SC_LS_MX&token=894050c76af8597a853f5b408b759f5d&filter=(Market=001)"));
        String previousDay = JSON.parseArray(EntityUtils.toString(httpClient.execute(httpGet).getEntity())).getJSONObject(0).getString("HdDate");
        long t1 = LocalDate.parse(previousDay, DateTimeFormatter.ofPattern("yyyy-MM-dd")).atTime(23, 59, 59).toInstant(ZoneOffset.of("+8")).getEpochSecond();

        System.out.println(t1);
        //ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        //new Thread(new Runnable() {
        //    @Override
        //    public void run() {
        //        lock.lock();
        //        System.out.println(Thread.currentThread().getName() + "Hello World!");
        //        try {
        //            Thread.sleep(5000);
        //        } catch (InterruptedException e) {
        //            e.printStackTrace();
        //        }
        //        lock.unlock();
        //    }
        //}).start();
        //new Thread(new Runnable() {
        //    @Override
        //    public void run() {
        //        lock.lock();
        //        System.out.println(Thread.currentThread().getName() + "Hello World!");
        //        try {
        //            Thread.sleep(5000);
        //        } catch (InterruptedException e) {
        //            e.printStackTrace();
        //        }
        //        lock.unlock();
        //    }
        //}).start();
    }
}
