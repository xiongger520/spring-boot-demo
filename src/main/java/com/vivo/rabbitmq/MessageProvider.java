//package com.vivo.rabbitmq;
//
//import org.springframework.amqp.AmqpException;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.core.MessagePostProcessor;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
//@Component
//public class MessageProvider {
//
//    @Autowired
//    RabbitTemplate rabbitTemplate;
//
//    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//
//    public void sendMessage(String exchange, String routingkey, String msg) {
//        System.out.println(dtf.format(LocalDateTime.now()) + ",发送消息:" + msg);
//        rabbitTemplate.convertAndSend(exchange, routingkey, msg);
//    }
//
//    public void sendDelayMessage(String exchange, String routingkey, String msg) {
//        System.out.println(dtf.format(LocalDateTime.now()) + ",发送消息:" + msg);
//        this.rabbitTemplate.convertAndSend(exchange, routingkey, msg, new MessagePostProcessor() {
//            @Override
//            public Message postProcessMessage(Message message) throws AmqpException {
//                //延时30秒
//                message.getMessageProperties().setDelay(30 * 1000);
//                return message;
//            }
//        });
//    }

    //public void sendDelayMessage(Integer delaySeconds) {
    //    String text = "Hello World!";
    //    MessageProperties messageProperties = new MessageProperties();
    //    messageProperties.setExpiration(delaySeconds * 1000 + "");
    //    Message message = new Message(text.getBytes(), messageProperties);
    //    System.out.println("发送消息,时间:" + LocalDateTime.now() + "内容:" + message);
    //    rabbitTemplate.convertAndSend(RabbitMQConfig.DEAD_LETTER_EXCHANGE, RabbitMQConfig.DELAY_ROUTING_KEY, message);
    //}
    //
    //public void send(String msg, int delayTime) {
    //    System.out.println("msg=" + ",delayTime" + delayTime);
    //    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //    this.rabbitTemplate.convertAndSend(RabbitMQConfig.DEAD_LETTER_EXCHANGE, RabbitMQConfig.DELAY_ROUTING_KEY, msg, message -> {
    //        message.getMessageProperties().setExpiration(delayTime + "");
    //        System.out.println(sdf.format(new Date()) + " Delay sent.");
    //        return message;
    //    });
    //}
//}
