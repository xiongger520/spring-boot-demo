//package com.vivo.rabbitmq;
//
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
//@Component
//public class MessageConsumer {
//
//    //@RabbitListener(queues = RabbitMQConfig.IMMEDIATE_QUEUE)
//    //@RabbitHandler
//    //public void getDirectMessage(String message) {
//    //    System.out.println("接收消息,时间:" + LocalDateTime.now() + "内容:" + message);
//    //}
//
//    //@RabbitListener(queues = RabbitMQConfig.DELAY_QUEUE)
//    //@RabbitHandler
//    //public void getDelayMessage(String message) {
//    //    System.out.println("接收消息,时间:" + LocalDateTime.now() + "内容:" + message);
//    //}
//
//    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//
//    @RabbitListener(queues = RabbitMQConfig.QUEUE_1)
//    @RabbitHandler
//    public void get1(String msg) {
//        System.out.println(dtf.format(LocalDateTime.now()) + ",队列1收到消息:" + msg);
//    }
//
//    @RabbitListener(queues = RabbitMQConfig.QUEUE_2)
//    @RabbitHandler
//    public void get2(String msg) {
//        System.out.println(dtf.format(LocalDateTime.now()) + ",队列2收到消息:" + msg);
//    }
//}