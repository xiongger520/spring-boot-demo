//package com.vivo.rabbitmq;
//
//import org.springframework.amqp.core.*;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//public class RabbitMQConfig {
//    //rabbitmq有三种常用的工作模式
//    //direct模式对应的交换机类型为routing，routingkey和bindingkey需要完全匹配，类似于点对点通信
//    //publish/subscribe模式对应的交换机类型为fanout，routingkey无效，类似于广播
//    //topic模式对应的交换机类型为topic，routingkey和bindingkey模糊匹配，类似于多点间复杂通信
//    public static final String DIRECT_EXCHANGE = "demo.direct.exchange";//直达交换机
//    public static final String FANOUT_EXCHANGE = "demo.fanout.exchange";//扇出交换机
//    public static final String TOPIC_EXCHANGE = "demo.topic.exchange";//主题交换机
//
//    public static final String QUEUE_1 = "demo.queue.one";//队列1
//    public static final String QUEUE_2 = "demo.queue.two";//队列2
//
//    public static final String ROUTING_KEY_1 = "demo.routingkey.one";//routingkey1
//    public static final String ROUTING_KEY_2 = "demo.routingkey.two";//routingkey2
//    public static final String ROUTING_KEY = "demo.routingkey.*";//routingkey
//    //说明：对于topic模式，发送的routingkey带有*或#，而队列绑定的routingkey不带有*或#时，队列无法收到消息
//    //说明：对于topic模式，发送的routingkey不带有*或#，而队列绑定的routingkey带有*或#时，队列可以收到消息（常用）
//    //说明：对于topic模式，发送的routingkey带有*或#，队列绑定的routingkey带有*或#时，队列可以收到消息（常用）
//    //说明：对于topic模式，发送的routingkey不带有*或#，队列绑定的routingkey不带有*或#时，相当于direct模式
//
//    public static final String DELAY_EXCHANGE = "demo.delay.exchange";//延时交换机
//    public static final String DELAY_QUEUE = "demo.delay.queue";//延时队列
//    public static final String DELAY_ROUTING_KEY = "demo.delay.routingkey";//延时routingkey
//
//    //创建直达交换机
//    @Bean
//    public DirectExchange directExchange() {
//        return new DirectExchange(DIRECT_EXCHANGE, true, false);
//    }
//
//    //创建队列1
//    @Bean
//    public Queue queue1() {
//        return new Queue(QUEUE_1, true);
//    }
//
//    //创建队列2
//    @Bean
//    public Queue queue2() {
//        return new Queue(QUEUE_2, true);
//    }
//
//    //把队列1和直达交换机绑定到一起
//    @Bean
//    public Binding directBinding1() {
//        return BindingBuilder.bind(queue1()).to(directExchange()).with(ROUTING_KEY_1);
//    }
//
//    //把队列2和直达交换机绑定到一起
//    @Bean
//    public Binding directBinding2() {
//        return BindingBuilder.bind(queue2()).to(directExchange()).with(ROUTING_KEY_2);
//    }
//
//    //创建订阅交换机
//    @Bean
//    public FanoutExchange fanoutExchange() {
//        return new FanoutExchange(FANOUT_EXCHANGE, true, false);
//    }
//
//    //把队列1和扇出交换机绑定到一起
//    @Bean
//    public Binding fanoutBinding1() {
//        return BindingBuilder.bind(queue1()).to(fanoutExchange());
//
//    }
//
//    //把队列2和扇出交换机绑定到一起
//    @Bean
//    public Binding fanoutBinding2() {
//        return BindingBuilder.bind(queue2()).to(fanoutExchange());
//    }
//
//    //创建主题交换机
//    @Bean
//    public TopicExchange topicExchange() {
//        return new TopicExchange(TOPIC_EXCHANGE, true, false);
//    }
//
//    //把队列1和主题交换机绑定到一起
//    @Bean
//    public Binding topicBinding1() {
//        return BindingBuilder.bind(queue1()).to(topicExchange()).with(ROUTING_KEY_1);
//    }
//
//    //把队列2和主题交换机绑定到一起
//    @Bean
//    public Binding topicBinding2() {
//        return BindingBuilder.bind(queue2()).to(topicExchange()).with(ROUTING_KEY_2);
//    }
//
//    //创建延时直达交换机
//    @Bean
//    public CustomExchange delayExchange() {
//        Map<String, Object> args = new HashMap<String, Object>();
//        args.put("x-delayed-type", "direct");
//        return new CustomExchange(DELAY_EXCHANGE, "x-delayed-message", true, false, args);
//    }
//
//    //把队列1和延时交换机绑定到一起
//    @Bean
//    public Binding delayBinding() {
//        return BindingBuilder.bind(queue1()).to(delayExchange()).with(ROUTING_KEY_1).noargs();
//    }
//}