package com.vivo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.vivo.dto.MyStock;
import com.vivo.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import javax.imageio.ImageIO;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.*;
import java.text.Collator;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class WenCai {

    private static CloseableHttpClient realHttpClient = HttpClients.createDefault();

    private static List<MyStock> queryStockList(String yesterday, String today, String sentence) {
        List<MyStock> myStockList = new ArrayList<>();
        try {
            HttpPost httpPost = new HttpPost("http://ai.iwencai.com/urp/v7/landing/getDataList");
            httpPost.addHeader("Connection", "keep-alive");
            httpPost.addHeader("Accept", "application/json");
            httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36");
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            List<NameValuePair> nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("query", sentence));
            nameValuePairList.add(new BasicNameValuePair("urp_sort_index", "所属同花顺行业"));
            nameValuePairList.add(new BasicNameValuePair("urp_sort_way", "asc"));
            nameValuePairList.add(new BasicNameValuePair("perpage", "3000"));
            nameValuePairList.add(new BasicNameValuePair("page", "1"));
            nameValuePairList.add(new BasicNameValuePair("is_cache", "0"));
            nameValuePairList.add(new BasicNameValuePair("ret", "json_all"));
            nameValuePairList.add(new BasicNameValuePair("query_type", "stock"));
            nameValuePairList.add(new BasicNameValuePair("comp_id", "6116243"));
            nameValuePairList.add(new BasicNameValuePair("uuid", "18369"));
            UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
            httpPost.setEntity(urlEncodedFormEntity);
            httpPost.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
            CloseableHttpResponse response = realHttpClient.execute(httpPost);
            String result = EntityUtils.toString(response.getEntity());
            JSONArray jsonArray = JSON.parseObject(result).getJSONObject("answer").getJSONArray("components").getJSONObject(0).getJSONObject("data").getJSONArray("datas");
            Integer size = jsonArray.size();
            String key = LocalDate.parse(today, DateTimeFormatter.ofPattern("yyyy年M月d日")).format(DateTimeFormatter.ofPattern("yyyyMMdd"));
            for (Integer i = 0; i < size; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String stockName = jsonObject.get("股票简称").toString();
                String stockCode = jsonObject.get("股票代码").toString().substring(0, 6);
                Double change = Double.valueOf(jsonObject.getString(String.format("涨跌幅:前复权[%s]", key)));
                Double capital = Double.valueOf(jsonObject.getString(jsonObject.keySet().stream().filter(item -> item.contains("总市值")).findFirst().orElse(null)));
                String market = jsonObject.getString("所属同花顺行业");
                myStockList.add(new MyStock(stockCode, stockName, change, market, null, capital));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myStockList;
    }

    public static void main(String[] args) {
        String today = "2022年5月11日";
        String yesterday = "2022年5月10日";
        String drive = "F:\\01\\";
        File p = new File(drive);
        if (!p.exists()) {
            FileUtils.createDirectory(drive);
        }
        String sentence20 = String.format("%s最低价/20日线不大于1.02 开盘价、收盘价不小于20日线 20日线递增 主板 总市值大于100亿 %s涨跌幅>0%% 所属行业", yesterday, today);
        String sentence30 = String.format("%s最低价/30日线不大于1.02 开盘价、收盘价不小于30日线 30日线递增 主板 总市值大于100亿 %s涨跌幅>0%% 所属行业", yesterday, today);
        String sentence60 = String.format("%s最低价/60日线不大于1.02 开盘价、收盘价不小于60日线 60日线递增 主板 总市值大于100亿 %s涨跌幅>0%% 所属行业", yesterday, today);
        Boolean isGray = false;
        List<MyStock> myStockList = new ArrayList<>();
        myStockList.addAll(queryStockList(yesterday, today, sentence20));
        myStockList.addAll(queryStockList(yesterday, today, sentence30));
        myStockList.addAll(queryStockList(yesterday, today, sentence60));
        myStockList = myStockList.stream().distinct().sorted(Comparator.comparing(MyStock::getMarket, (x, y) -> {
            Comparator<Object> compare = Collator.getInstance(Locale.CHINA);
            return compare.compare(x, y);
        }).thenComparing(Comparator.comparing(MyStock::getChange).reversed())).collect(Collectors.toList());
        for (MyStock i : myStockList) {
            System.out.println(String.format("%s,%s,%s,%.2f%%", i.getCode(), i.getName(), i.getMarket(), i.getChange()));
        }
        String path = drive + "wencai";
        File f = new File(path);
        if (f.isDirectory() && f.exists()) {
            FileUtils.deleteDirectory(path);
        }
        FileUtils.createDirectory(path);
        try {
            Integer idx = 0;
            for (MyStock i : myStockList) {
                String eastCode = null;
                if (StringUtils.startsWith(i.getCode(), "6")) {
                    eastCode = "1." + i.getCode();
                } else {
                    eastCode = "0." + i.getCode();
                }
                HttpGet httpGet = new HttpGet(String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL", eastCode));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(500).build());
                File file = new File(String.format("%s\\%03d-%s-%s-%.0f亿.png", path, ++idx, i.getCode(), i.getName().replace("*", "星"), i.getCapital() / 1e8));
                if (isGray) {
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(EntityUtils.toByteArray(realHttpClient.execute(httpGet).getEntity()));
                    BufferedImage originalImage = ImageIO.read(byteArrayInputStream);
                    BufferedImage grayImage = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
                    ColorConvertOp colorConvertOp = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
                    colorConvertOp.filter(originalImage, grayImage);
                    ImageIO.write(grayImage, "png", file);
                } else {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(EntityUtils.toByteArray(realHttpClient.execute(httpGet).getEntity()));
                    fileOutputStream.close();
                }
            }
            BufferedWriter log = new BufferedWriter(new FileWriter(drive + "wencai.txt", false));
            for (MyStock i : myStockList) {
                String sinaCode = null;
                if (StringUtils.startsWith(i.getCode(), "6")) {
                    sinaCode = "SH" + i.getCode();
                } else {
                    sinaCode = "SZ" + i.getCode();
                }
                String line = String.format("%s,%s,%s\n", sinaCode, i.getName(), i.getMarket());
                log.write(line);
                log.flush();
                System.out.println(String.format("%s,%s,%s,%.2f%%", sinaCode, i.getName(), i.getMarket(), i.getChange()));
            }
            log.close();
            System.out.println(String.format("共计%d个", myStockList.size()));
        } catch (Exception e) {
        }
    }
}
