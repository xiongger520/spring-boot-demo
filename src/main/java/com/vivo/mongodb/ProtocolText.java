package com.vivo.mongodb;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class ProtocolText {
    //    自增id
    @Id
    private long id;
    //    文档id
    private String textCode;
    //    文档名称
    private String textName;
    //    文档分类
    private String textType;
    //    文档提供者id
    private String supplierCode;
    //    文档提供者名称
    private String supplierName;
    //    文档Base64字符串内容
    private String base64Content;

    public ProtocolText(long id, String textCode, String textName, String textType, String supplierCode, String supplierName, String base64Content) {
        this.id = id;
        this.textCode = textCode;
        this.textName = textName;
        this.textType = textType;
        this.supplierCode = supplierCode;
        this.supplierName = supplierName;
        this.base64Content = base64Content;
    }
}
