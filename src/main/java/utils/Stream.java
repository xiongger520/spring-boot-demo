package utils;

import com.vivo.VolService;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.concurrent.ExecutionException;

public class Stream {

    //lambda表达式一般语法
    //(Type1 param1, Type2 param2, ..., TypeN paramN) -> {
    //    statment1;
    //    statment2;
    //    //.............
    //    return statmentM;
    //}
    //单参数语法，当lambda表达式的参数个数只有一个，可以省略小括号
    //param1 -> {
    //    statment1;
    //    statment2;
    //    //.............
    //    return statmentM;
    //}
    //当lambda表达式只包含一条语句时，可以省略大括号、return和语句结尾的分号
    //public static void main(String[] args) {
    //    List<String> proNames = Arrays.asList(new String[]{"Ni", "Hao", "Lambda"});
    //    List<String> lowercaseNames1 = proNames.stream().map(name -> {
    //        return name.toLowerCase();
    //    }).collect(Collectors.toList());
    //    List<String> lowercaseNames2 = proNames.stream().map(name -> name.toLowerCase()).collect(Collectors.toList());
    //    //方法引用写法
    //    List<String> lowercaseNames3 = proNames.stream().map(String::toLowerCase).collect(Collectors.toList());
    //    //System.out.println(lowercaseNames1);
    //    //System.out.println(lowercaseNames2);
    //    //lowercaseNames3.forEach((x)-> {System.out.println(x);});
    //    //lowercaseNames3.forEach(System.out::print);
    //    List<Integer> nums = Lists.newArrayList(1, null, 3, 4, null, 6);
    //    //System.out.println(nums.stream().filter((num) -> {
    //    //    return num == null;
    //    //}).count());
    //    //System.out.println(nums.stream().filter(num -> num == null).count());
    //    //distinct用于去重复，limit用于截断
    //    List<Integer> ints = Lists.newArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    //    System.out.println(ints.stream().allMatch(item -> item < 100));
    //    Optional<Integer> optionalInteger = ints.stream().findAny();
    //    if (optionalInteger.isPresent()) {
    //        System.out.println(optionalInteger.get());
    //    }
    //    System.out.println(ints.stream().filter(x -> x > 3).findFirst().orElse(null));
    //    System.out.println(ints.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
    //    System.out.println(ints);
    //    Map<Integer, String> pairs = new TreeMap<>(Comparator.reverseOrder());
    //    pairs.put(2, "B");
    //    pairs.put(1, "A");
    //    pairs.put(3, "C");
    //
    //    List<MaDto> maDtoList = new ArrayList<>();
    //    String upMa = null;
    //    String downMa = null;
    //    maDtoList.add(new MaDto("MA5",1.0));
    //    maDtoList.add(new MaDto("MA10",2.0));
    //    maDtoList.add(new MaDto("MA20",7.0));
    //    maDtoList.add(new MaDto("MA30",4.0));
    //    maDtoList.add(new MaDto("MA60",4.0));
    //    maDtoList.add(new MaDto("MA120",6.0));
    //    maDtoList.add(new MaDto("realPrice",0.5));
    //    maDtoList = maDtoList.stream().sorted(Comparator.comparing(MaDto::getMaValue).reversed()).collect(Collectors.toList());
    //    System.out.println(maDtoList);
    //    for (MaDto maDto : maDtoList) {
    //        switch (maDto.getMaName()) {
    //            case "MA5": {
    //                //shareEntity.setMA5(maDto.getMaValue());
    //                break;
    //            }
    //            case "MA10": {
    //                //shareEntity.setMA10(maDto.getMaValue());
    //                break;
    //            }
    //            case "MA20": {
    //                //shareEntity.setMA20(maDto.getMaValue());
    //                break;
    //            }
    //            case "MA30": {
    //                //shareEntity.setMA30(maDto.getMaValue());
    //                break;
    //            }
    //            case "MA60": {
    //                //shareEntity.setMA60(maDto.getMaValue());
    //                break;
    //            }
    //            case "MA120": {
    //                //shareEntity.setMA120(maDto.getMaValue());
    //                break;
    //            }
    //            case "realPrice": {
    //                Integer idx = maDtoList.indexOf(maDto);
    //                if (idx - 1 >= 0) {
    //                    upMa = maDtoList.get(idx - 1).getMaName();
    //                }
    //                if (idx + 1 <= maDtoList.size() - 1) {
    //                    downMa = maDtoList.get(idx + 1).getMaName();
    //                }
    //                break;
    //            }
    //        }
    //    }
    //    System.out.println(upMa);
    //    System.out.println(downMa);
    //}

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        //List<String> list = Arrays.asList("one Two three Four five six one three Four".split(" "));
        //TerDto terDto = new TerDto();
        //terDto.setList(list);
        //System.out.println("List :" + terDto.getList());
        //list = list.stream().filter(item -> "Two".equals(item)).collect(Collectors.toList());
        //System.out.println(list);
        ////Collections.replaceAll(terDto.getList(), "one", "hundrea");
        ////System.out.println("replaceAll: " + terDto.getList());
        //PreCreditUserCodMqMessage preCreditUserCodMqMessage = new PreCreditUserCodMqMessage(
        //        "123",
        //        "456",
        //        "PL1111",
        //        new Date(),
        //        "buzhidao",
        //        "8976666"
        //);
        //log.info(JSON.toJSONString(preCreditUserCodMqMessage, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue));
        //String tmp="{\"rc\":0,\"rt\":10,\"svr\":182993738,\"lt\":1,\"full\":1,\"data\":{\"code\":\"600600\",\"trends\":[\"2021-04-23 09:30,96.30,96.300\",\"2021-04-23 09:31,97.80,96.706\",\"2021-04-23 09:32,97.71,97.084\",\"2021-04-23 09:33,97.30,97.216\",\"2021-04-23 09:34,97.78,97.256\",\"2021-04-23 09:35,97.71,97.302\",\"2021-04-23 09:36,98.00,97.394\",\"2021-04-23 09:37,97.98,97.503\",\"2021-04-23 09:38,97.03,97.507\",\"2021-04-23 09:39,96.39,97.441\",\"2021-04-23 09:40,96.13,97.380\",\"2021-04-23 09:41,95.78,97.309\",\"2021-04-23 09:42,95.50,97.255\",\"2021-04-23 09:43,95.95,97.189\",\"2021-04-23 09:44,96.64,97.157\",\"2021-04-23 09:45,97.10,97.144\",\"2021-04-23 09:46,96.88,97.138\",\"2021-04-23 09:47,96.94,97.130\",\"2021-04-23 09:48,96.81,97.123\",\"2021-04-23 09:49,96.80,97.120\",\"2021-04-23 09:50,96.08,97.108\",\"2021-04-23 09:51,95.97,97.070\",\"2021-04-23 09:52,95.93,97.049\",\"2021-04-23 09:53,95.85,97.017\",\"2021-04-23 09:54,95.58,96.989\",\"2021-04-23 09:55,95.76,96.953\",\"2021-04-23 09:56,95.38,96.905\",\"2021-04-23 09:57,95.00,96.821\",\"2021-04-23 09:58,94.84,96.786\",\"2021-04-23 09:59,94.53,96.742\",\"2021-04-23 10:00,94.27,96.678\",\"2021-04-23 10:01,94.52,96.603\",\"2021-04-23 10:02,94.59,96.548\",\"2021-04-23 10:03,94.88,96.506\",\"2021-04-23 10:04,95.21,96.477\",\"2021-04-23 10:05,95.60,96.453\",\"2021-04-23 10:06,95.51,96.434\",\"2021-04-23 10:07,95.02,96.417\",\"2021-04-23 10:08,94.56,96.387\",\"2021-04-23 10:09,94.55,96.366\",\"2021-04-23 10:10,94.64,96.347\",\"2021-04-23 10:11,94.44,96.320\",\"2021-04-23 10:12,94.58,96.301\",\"2021-04-23 10:13,94.70,96.281\",\"2021-04-23 10:14,94.92,96.263\",\"2021-04-23 10:15,94.80,96.252\",\"2021-04-23 10:16,94.97,96.243\",\"2021-04-23 10:17,95.45,96.233\",\"2021-04-23 10:18,95.85,96.227\",\"2021-04-23 10:19,96.00,96.224\",\"2021-04-23 10:20,95.41,96.223\",\"2021-04-23 10:21,95.17,96.218\",\"2021-04-23 10:22,95.29,96.208\",\"2021-04-23 10:23,95.00,96.203\",\"2021-04-23 10:24,95.17,96.193\",\"2021-04-23 10:25,94.95,96.188\",\"2021-04-23 10:26,94.71,96.179\",\"2021-04-23 10:27,94.60,96.167\",\"2021-04-23 10:28,94.36,96.154\",\"2021-04-23 10:29,94.34,96.142\",\"2021-04-23 10:30,94.27,96.113\",\"2021-04-23 10:31,94.41,96.093\",\"2021-04-23 10:32,94.30,96.084\",\"2021-04-23 10:33,94.32,96.075\",\"2021-04-23 10:34,94.46,96.069\",\"2021-04-23 10:35,94.50,96.060\",\"2021-04-23 10:36,94.36,96.051\",\"2021-04-23 10:37,94.24,96.039\",\"2021-04-23 10:38,94.06,96.022\",\"2021-04-23 10:39,93.85,95.984\",\"2021-04-23 10:40,93.70,95.960\",\"2021-04-23 10:41,93.85,95.946\",\"2021-04-23 10:42,93.77,95.932\",\"2021-04-23 10:43,93.87,95.923\",\"2021-04-23 10:44,93.80,95.913\",\"2021-04-23 10:45,93.79,95.897\",\"2021-04-23 10:46,93.59,95.883\",\"2021-04-23 10:47,93.44,95.862\",\"2021-04-23 10:48,93.30,95.828\",\"2021-04-23 10:49,93.50,95.807\",\"2021-04-23 10:50,93.80,95.800\",\"2021-04-23 10:51,93.55,95.789\",\"2021-04-23 10:52,93.51,95.779\",\"2021-04-23 10:53,93.22,95.763\",\"2021-04-23 10:54,93.06,95.748\",\"2021-04-23 10:55,93.00,95.709\",\"2021-04-23 10:56,92.85,95.698\",\"2021-04-23 10:57,92.60,95.673\",\"2021-04-23 10:58,92.59,95.642\",\"2021-04-23 10:59,92.59,95.625\",\"2021-04-23 11:00,92.69,95.612\",\"2021-04-23 11:01,92.71,95.595\",\"2021-04-23 11:02,92.92,95.587\",\"2021-04-23 11:03,93.00,95.573\",\"2021-04-23 11:04,93.05,95.557\",\"2021-04-23 11:05,92.98,95.543\",\"2021-04-23 11:06,92.81,95.533\",\"2021-04-23 11:07,92.91,95.522\",\"2021-04-23 11:08,92.86,95.507\",\"2021-04-23 11:09,92.86,95.499\",\"2021-04-23 11:10,92.92,95.489\",\"2021-04-23 11:11,92.97,95.472\",\"2021-04-23 11:12,92.94,95.457\",\"2021-04-23 11:13,92.84,95.441\",\"2021-04-23 11:14,92.84,95.433\",\"2021-04-23 11:15,92.75,95.424\",\"2021-04-23 11:16,92.67,95.415\",\"2021-04-23 11:17,92.60,95.405\",\"2021-04-23 11:18,92.55,95.398\",\"2021-04-23 11:19,92.53,95.377\",\"2021-04-23 11:20,92.52,95.371\",\"2021-04-23 11:21,92.51,95.363\",\"2021-04-23 11:22,92.50,95.352\",\"2021-04-23 11:23,92.46,95.338\",\"2021-04-23 11:24,92.41,95.330\",\"2021-04-23 11:25,92.30,95.320\",\"2021-04-23 11:26,92.04,95.308\",\"2021-04-23 11:27,92.16,95.285\",\"2021-04-23 11:28,92.20,95.275\",\"2021-04-23 11:29,92.19,95.268\",\"2021-04-23 11:30,92.32,95.260\",\"2021-04-23 13:01,92.36,95.247\",\"2021-04-23 13:02,92.57,95.233\",\"2021-04-23 13:03,92.52,95.224\",\"2021-04-23 13:04,92.69,95.217\",\"2021-04-23 13:05,92.55,95.211\",\"2021-04-23 13:06,92.51,95.206\",\"2021-04-23 13:07,92.31,95.198\",\"2021-04-23 13:08,92.27,95.190\",\"2021-04-23 13:09,92.30,95.177\",\"2021-04-23 13:10,92.23,95.165\",\"2021-04-23 13:11,92.45,95.109\",\"2021-04-23 13:12,92.60,95.089\",\"2021-04-23 13:13,92.38,95.078\",\"2021-04-23 13:14,92.38,95.074\",\"2021-04-23 13:15,92.50,95.062\",\"2021-04-23 13:16,92.52,95.057\",\"2021-04-23 13:17,92.57,95.044\",\"2021-04-23 13:18,92.60,95.036\",\"2021-04-23 13:19,92.51,95.029\",\"2021-04-23 13:20,92.50,95.025\",\"2021-04-23 13:21,92.48,95.023\",\"2021-04-23 13:22,92.36,95.017\",\"2021-04-23 13:23,92.26,95.012\",\"2021-04-23 13:24,92.23,95.008\",\"2021-04-23 13:25,92.24,95.002\",\"2021-04-23 13:26,92.18,94.998\",\"2021-04-23 13:27,92.12,94.992\",\"2021-04-23 13:28,92.10,94.982\",\"2021-04-23 13:29,92.08,94.975\",\"2021-04-23 13:30,92.09,94.966\",\"2021-04-23 13:31,92.09,94.957\",\"2021-04-23 13:32,92.23,94.950\",\"2021-04-23 13:33,92.25,94.947\",\"2021-04-23 13:34,92.30,94.942\",\"2021-04-23 13:35,92.39,94.937\",\"2021-04-23 13:36,92.60,94.931\",\"2021-04-23 13:37,92.57,94.927\",\"2021-04-23 13:38,92.61,94.923\",\"2021-04-23 13:39,92.76,94.917\",\"2021-04-23 13:40,93.05,94.910\",\"2021-04-23 13:41,93.29,94.904\",\"2021-04-23 13:42,93.23,94.901\",\"2021-04-23 13:43,93.39,94.896\",\"2021-04-23 13:44,93.37,94.893\",\"2021-04-23 13:45,93.50,94.888\",\"2021-04-23 13:46,93.78,94.884\",\"2021-04-23 13:47,93.92,94.881\",\"2021-04-23 13:48,93.91,94.878\",\"2021-04-23 13:49,93.69,94.869\",\"2021-04-23 13:50,93.60,94.864\",\"2021-04-23 13:51,93.47,94.859\",\"2021-04-23 13:52,93.38,94.856\",\"2021-04-23 13:53,93.35,94.849\",\"2021-04-23 13:54,93.25,94.843\",\"2021-04-23 13:55,93.20,94.841\",\"2021-04-23 13:56,93.04,94.838\",\"2021-04-23 13:57,92.98,94.831\",\"2021-04-23 13:58,92.71,94.827\",\"2021-04-23 13:59,92.58,94.820\",\"2021-04-23 14:00,92.72,94.810\",\"2021-04-23 14:01,93.00,94.800\",\"2021-04-23 14:02,92.97,94.798\",\"2021-04-23 14:03,92.76,94.794\",\"2021-04-23 14:04,92.76,94.787\",\"2021-04-23 14:05,92.76,94.783\",\"2021-04-23 14:06,92.73,94.778\",\"2021-04-23 14:07,92.60,94.772\",\"2021-04-23 14:08,92.39,94.764\",\"2021-04-23 14:09,92.24,94.753\",\"2021-04-23 14:10,92.17,94.734\",\"2021-04-23 14:11,92.08,94.718\",\"2021-04-23 14:12,92.09,94.705\",\"2021-04-23 14:13,92.09,94.696\",\"2021-04-23 14:14,92.20,94.691\",\"2021-04-23 14:15,92.14,94.683\",\"2021-04-23 14:16,92.13,94.675\",\"2021-04-23 14:17,92.15,94.674\",\"2021-04-23 14:18,92.15,94.665\",\"2021-04-23 14:19,92.12,94.656\",\"2021-04-23 14:20,92.05,94.644\",\"2021-04-23 14:21,92.00,94.617\",\"2021-04-23 14:22,92.20,94.605\",\"2021-04-23 14:23,92.18,94.599\",\"2021-04-23 14:24,92.19,94.595\",\"2021-04-23 14:25,92.22,94.588\",\"2021-04-23 14:26,92.31,94.585\",\"2021-04-23 14:27,92.26,94.579\",\"2021-04-23 14:28,92.18,94.572\",\"2021-04-23 14:29,92.02,94.567\",\"2021-04-23 14:30,92.01,94.555\",\"2021-04-23 14:31,92.14,94.539\",\"2021-04-23 14:32,92.19,94.536\",\"2021-04-23 14:33,92.18,94.534\",\"2021-04-23 14:34,92.10,94.528\",\"2021-04-23 14:35,92.00,94.520\",\"2021-04-23 14:36,92.01,94.513\",\"2021-04-23 14:37,91.92,94.501\",\"2021-04-23 14:38,91.86,94.490\",\"2021-04-23 14:39,91.79,94.481\",\"2021-04-23 14:40,92.15,94.461\",\"2021-04-23 14:41,92.30,94.457\",\"2021-04-23 14:42,92.48,94.451\",\"2021-04-23 14:43,92.49,94.447\",\"2021-04-23 14:44,92.47,94.444\",\"2021-04-23 14:45,92.45,94.441\",\"2021-04-23 14:46,92.44,94.439\",\"2021-04-23 14:47,92.46,94.437\",\"2021-04-23 14:48,92.69,94.430\",\"2021-04-23 14:49,92.61,94.427\",\"2021-04-23 14:50,92.57,94.425\",\"2021-04-23 14:51,92.55,94.421\",\"2021-04-23 14:52,92.57,94.418\",\"2021-04-23 14:53,92.57,94.412\",\"2021-04-23 14:54,92.57,94.405\",\"2021-04-23 14:55,92.71,94.395\",\"2021-04-23 14:56,92.73,94.389\",\"2021-04-23 14:57,92.60,94.383\",\"2021-04-23 14:58,92.60,94.383\",\"2021-04-23 14:59,92.60,94.383\",\"2021-04-23 15:00,92.60,94.375\"]}}";
        //JSONArray jsonArray = JSON.parseObject(tmp).getJSONObject("data").getJSONArray("trends");
        //Integer count = 0;
        //for (Object obj : jsonArray) {
        //    String[] arr = String.valueOf(obj).split(",");
        //    if (Double.valueOf(arr[1]) >= Double.valueOf(arr[2])) {
        //        count++;
        //    }
        //}
        //System.out.println(count * 1.0 / jsonArray.size() * 100 + "%");

        //CloseableHttpClient realHttpClient = HttpClients.createDefault();
        //String sinaCode = "sh600600";
        ////HttpGet httpGet = new HttpGet(String.format("http://image.sinajs.cn/newchart/min/n/%s.gif", sinaCode));
        //HttpGet httpGet = new HttpGet(String.format("http://image.sinajs.cn/newchart/daily/n/%s.gif", sinaCode));
        //httpGet.setConfig(RequestConfig.custom().setConnectTimeout(500).build());
        //File tmp = File.createTempFile("ShareFollow", ".gif");
        //tmp.deleteOnExit();
        //FileOutputStream fileOutputStream = new FileOutputStream(tmp);
        //fileOutputStream.write(EntityUtils.toByteArray(realHttpClient.execute(httpGet).getEntity()));
        //fileOutputStream.close();
        //Desktop.getDesktop().open(tmp);
        // AvgService avgService = new AvgService();
        //System.out.println(avgService.calAvgPercent("sh600600"));
        //FutureTask<Boolean> futureTask = new FutureTask<>(new Callable<Boolean>() {
        //    @Override
        //    public Boolean call() throws Exception {
        //        System.out.println("子线程1");
        //        Thread.sleep(5000);
        //        return null;
        //    }
        //});
        //
        //FutureTask<Boolean> futureTask2 = new FutureTask<>(new Callable<Boolean>() {
        //    @Override
        //    public Boolean call() throws Exception {
        //        System.out.println("子线程2");
        //        Thread.sleep(5000);
        //        return null;
        //    }
        //});
        //ExecutorService executorService = Executors.newSingleThreadExecutor();
        //executorService.submit(futureTask);
        //System.out.println("主线程");
        //System.out.println(futureTask.get());
        ////在析构方法中关闭线程池
        //executorService.shutdown();
        //Thread.sleep(5000);
        //executorService.submit(futureTask2);
        //LocalDateTime localDateTime=LocalDateTime.of();
        //localDateTime.;
        //LocalTime now = LocalTime.now();
        //LocalTime time930 = LocalTime.of(9, 30, 0, 0);
        //LocalTime time1130 = LocalTime.of(9, 30, 0, 0);
        //LocalTime time1300 = LocalTime.of(13, 00, 0, 0);
        //LocalTime time1500 = LocalTime.of(15, 00, 0, 0);
        //Long idx = 0L;
        //
        //if (now.isAfter(time1500)) {
        //    idx = 239L;
        //} else if (now.isAfter(time1300)) {
        //    idx = Duration.between(time930, time1130).getSeconds() / 60;
        //} else if (now.isAfter(time1130)) {
        //    idx = Duration.between(time930, time1130).getSeconds() / 60;
        //} else if (now.isAfter(time930)) {
        //    idx = Duration.between(time930, time1130).getSeconds() / 60;
        //}

        //System.out.println(time930.is(time1130));

        //AvgService avgService = new AvgService();
        //while (true) {
        //    System.out.println(avgService.calAvgPercent("1.600809"));
        //    Thread.sleep(1000);
        //}
        //MaService maService = new MaService();
        //System.out.println(maService.calMa("1.600519"));
        LocalTime nine30 = LocalTime.of(9, 30, 0, 0);
        LocalTime eleven30 = LocalTime.of(11, 30, 0, 0);
        LocalTime thirteen = LocalTime.of(13, 0, 0, 0);
        LocalTime fifteen = LocalTime.of(15, 0, 0, 0);
        VolService volService = new VolService();
        while (true) {
            Integer timeIndex = 0;
            LocalTime now = LocalTime.now();
            if (now.isBefore(nine30)) {
                timeIndex = 239;
            } else if (now.isBefore(eleven30)) {
                timeIndex = ((int) Duration.between(nine30, now).getSeconds() + 60) / 60 - 1;
            } else if (now.isBefore(thirteen)) {
                timeIndex = 119;
            } else if (now.isBefore(fifteen)) {
                timeIndex = ((int) Duration.between(nine30, now).getSeconds() + 60) / 60 - 91;
            } else {
                timeIndex = 239;
            }
            //long t1 = System.currentTimeMillis();
            System.out.println(volService.calPreviousVol(timeIndex, "sz002709"));
            //System.out.println("耗时:" + (System.currentTimeMillis() - t1));
            Thread.sleep(3000);
        }
    }
}
