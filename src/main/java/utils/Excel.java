package utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class Excel {

    public static void main(String[] args) throws IOException {
        //文件绝对路径
        String filePath = "D:\\123.xlsx";
        //表索引，从0开始
        Integer sheetNum = 1;
        //起始行数
        Integer startRow = 4 - 1;
        Integer endRow = 103 - 1;
        Integer zhu_nian_you = 'A' - 'A';
        Integer zhu_nian_wu = 'A' - 'A';
        Integer zhu_yue_you = 'A' - 'A';
        Integer zhu_yue_wu = 'A' - 'A';
        Integer xu_nian_you = 'A' - 'A';
        Integer xu_nian_wu = 'A' - 'A';
        Integer xu_yue_you = 'A' - 'A';
        Integer xu_yue_wu = 'A' - 'A';
        Integer fu_nian_you = 'A' - 'A';
        Integer fu_nian_wu = 'A' - 'A';
        Integer fu_yue_you = 'A' - 'A';
        Integer fu_yue_wu = 'A' - 'A';
        Integer zi_nian_you = 'A' - 'A';
        Integer zi_nian_wu = 'A' - 'A';
        Integer zi_yue_you = 'A' - 'A';
        Integer zi_yue_wu = 'A' - 'A';
        File file = new File(filePath);
        String fileName = file.getName();
        FileInputStream fileInputStream = new FileInputStream(file);
        Workbook workbook = null;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            workbook = new XSSFWorkbook(fileInputStream);
        } else {
            workbook = new HSSFWorkbook(fileInputStream);
        }
        Sheet sheet = workbook.getSheetAt(sheetNum);
        List<String> hasSocials = Arrays.asList("0", "1");
        List<String> pays = Arrays.asList("M12", "S");
        for (Integer age = startRow; age <= endRow; age++) {
            for (String hasSocial : hasSocials) {
                for (String pay : pays) {
                    if ("0".equals(hasSocial)) {
                        if ("M12".equals(pay)) {
                            System.out.println("0," + "M12," + sheet.getRow(age).getCell(1).getNumericCellValue());
                        }
                        if ("S".equals(pay)) {
                            System.out.println("0," + "S," + sheet.getRow(age).getCell(1).getNumericCellValue());
                        }
                    }
                    if ("1".equals(hasSocial)) {
                        if ("M12".equals(pay)) {
                            System.out.println("1," + "M12," + sheet.getRow(age).getCell(1).getNumericCellValue());
                        }
                        if ("S".equals(pay)) {
                            System.out.println("1," + "S," + sheet.getRow(age).getCell(1).getNumericCellValue());
                        }
                    }

                }
            }
        }

    }
}
